\chapter{Literature review} \label{Ch2:Literature}
\graphicspath{{Chapters/Literature/}}
\acf{P2PL} is a micro-financing platform, which is rising as an alternative to traditional financial intermedia such as banks. In a \ac{P2PL} system, lenders and borrowers are the two main roles, and \ac{P2PL} online platform connects lenders to borrowers directly. On the one side, the borrowers request loans from the P2PL online platform by providing their personal information. On the other side, the lenders decide to invest in a particular loan by looking through the information posted on the P2PL platform. In recent years, a great deal of research~\cite{Guo2016417, babaei2020multi, byanjankar2021data} has gone into developing recommendation systems to help lenders achieve high returns with low risk of defaults. In contrast, the work in~\cite{ceyhan2011dynamics} introduces a model to predict the likelihood of successful funding for borrowers. On the lender's side, the most critical problem is to avoid any investment on loans with a high likelihood of default. While on the borrower's side, successfully getting funded with the lowest interest rate payable is the only problem the borrowers need to consider. Hence, given the characteristics of the borrower, one needs to deal with the following three problems first to help both lenders and borrowers:
\begin{enumerate}
	\item Predict the chance of the loan defaulting.
	\item Predict the likelihood of the loan successfully being funded.
	\item Predict the interest rate of the loan.
\end{enumerate}
As a result, machine learning techniques are widely used in recent works on the \ac{P2PL} system. However, many machine learning algorithms are sensitive to the number of features (also called predictors) and the usefulness\footnote{Being relevant, irrelevant, noisy, or having co-nonlinearity.} of features while a plethora of features can also lead to long execution times. Hence, feature selection is an essential problem to study. This chapter first reviews several popular machine learning techniques for both classification and regression. Next, several feature selection algorithms, which are widely used in reducing irrelevant features, are introduced. The current state-of-the-art recommendation systems with optimisation models for lenders and borrowers are also reviewed in this chapter.

\section{Machine learning techniques}\label{Ch2:Machine}
A number of different machine learning techniques have been proposed to classify loans being considered for investment. The work in~\citep{malekipirbazari2015risk} explicitly identifies the borrower characteristics (the predictor variables) that most influence the two label classification problem of likelihood of borrowers defaulting on a loan. The work in~\citep{malekipirbazari2015risk} compared a number of classification approaches such as: \acf{LOGIT}~\citep{friedman2001elements}, \acf{SVM}~\citep{suykens1999least}, and \acf{RF}~\citep{breiman2001random}, with the conclusion that \ac{RF}
performs the best. This section briefly reviews five popular classification and regression algorithms, which are Linear regression~\cite{montgomery2012introduction}, \acf{LOGIT}~\cite{wiginton1980note}, \acf{RF}~\cite{feldman2005mortgage}, \acf{SVM}~\cite{suykens1999least} and \acf{k-NN}\cite{dudani1976distance}.

\subsection{Linear regression}\label{Ch2:Linear}
Linear regression is a linear model which assumes a linear relationship between the $N$ input variables $\mathbf{x} = \{x_{1}, x_{2}, \dots, x_{N}\}$ and the response variable $y$. In this thesis, linear regression is used to predict the interest rates payable of borrowers. In this case, the response variable $y$ is the interest rates payable by borrowers and the $N$ input variables are the features by borrowers. To illustrate, three features ( `IsBorrowerHomeowner', `DebtToIncomeRatio' and `Term') together with the response variable (the interest rates payable) are used as an example to build the linear regression model.  Then, the response variable $y$ can be computed from a linear combination of $x_{1}$, $x_{2}$ and $x_{3}$ as shown in equation (\ref{eq: linear regression}).
\begin{equation}\label{eq: linear regression}
	y = f(\mathbf{x}) = \beta_{0} + \sum_{i=1}^{N}\beta_{i}\times x_{i},
\end{equation}
\noindent
In equation (\ref{eq: linear regression}), $\beta_{0}$ is the intercept, $\beta_{i}$ is the coefficient corresponding to the feature $x_{i} \in \mathbf{x}$ and $N = 3$ is the number of features.  

To fit the linear regression line, the most popular method is the ordinary least square~\cite{mcelroy1967necessary}. This method computes the best-fitting line by minimising the sum of the squared residuals. More specifically, the ordinary least square method calculates the distance between the linear regression line and each data point, then takes the sum of squares of these distances.

\subsection{\acf{LOGIT}}
\label{Ch2: Logit}

\acf{LOGIT} is a regression model where the response variable only has two possible outcomes (hence is more often used as a classification algorithm), and it is very popular for analyzing \ac{P2PL} datasets~\cite{Guo2016417,malekipirbazari2015risk,serrano2015determinants}. In this thesis, \ac{LOGIT} is used to classify a given borrower as defaulting or non-defaulting. To illustrate, the response variable (`Loanstatus') and three features (`IsBorrowerHomeowner', `DebtToIncomeRatio' and `Term') are used as an example.  Given $\hat{p}$ is the probability of a borrower defaulting, the logistic regression function can be written as shown in equation (~\ref{eq:logit}).
	
\begin{equation}\label{eq:logit}
f(\mathbf{x}) = logit(\hat{p}) = \ln (\frac{\hat{p}}{1 - \hat{p}}) = \beta_{0} +
\sum\limits_{i = 1}^{N}\beta_{i} \times x_{i},
\end{equation}

\noindent
In equation (\ref{eq:logit}), $\beta_{0}$ is the intercept, $\beta_{i}$ is the coefficient corresponding to the feature $x_{i} \in \mathbf{x}$ and $N = 3$ is the number of features. The \textit{logit} function is defined as the logged odds:

\[\text{odds} = \frac{\hat{p}}{1 - \hat{p}} = \frac{\text{probability of
		a loan defaulting}}{\text{probability of a loan
		not-defaulting}}.\]

Hence, the following formulation is obtained:
\[y = \begin{cases}
0, & \text{if } f(\mathbf{x})> 0\\
1, & \text{otherwise}.
\end{cases} \]

Compared to linear regression, \ac{LOGIT} is more difficult to interpret. Because it is not a linear model and the interpretation of the weights ($\beta_{i}$) is multiplication. More specifically, the weights  ($\beta_{i}$) in \ac{LOGIT} do not influence the probability of a borrower defaulting, because the weighted sum is transfered by the logistic function to a probability. In addition, \ac{LOGIT} can only output a value from the set \{0, 1\}, hence it can not be used to predict the interest rates of borrowers.

\subsection{\acf{RF}}
\label{Ch2: Random-forest}

\acf{RF} is one of the most popular techniques for classification when there is an non-linear relationship between predictors and response variables. In fact, work in  Malekipirbazari and Aksakalli~\cite{malekipirbazari2015risk} proposes that \ac{RF} is the best technique for two label classification of likelihood of borrowers defaulting. This subsection first describes decision trees that constitute the \ac{RF} and then describes \ac{RF}s themselves.

\subsection*{Decision trees}
Decision trees~\citep{friedman2001elements} are supervised learning algorithms that can be used in both classification and regression. When building a decision tree, it is started from a root node followed by the sub-trees with interior nodes and end with the leaf nodes. Each interior node corresponds to a test of a descriptive feature, and the branches represent the process of splitting the dataset along the value of this feature. The split process will stop once the stopping criteria is reached, ending up with a leaf node.

\begin{figure}[h!]
	\centering
	\includegraphics[ width=1.0\textwidth]{decisiontree} 
	\caption{An example of a decision tree as applied to classify borrowers as defaulting or non-defaulting~\label{fig:tree}.}
\end{figure}

An example decision tree applied to the P2PL loan classification problem is illustrated in Figure~\ref{fig:tree}. In this example, three predictors are used to split the dataset: `Is Borrower Homeowner', `Debt To Income Ratio', and `Term'. By using the feature `Is Borrower Homeowner', the loans are first split into two parts, loans where the borrower is a home owner and loans where the borrower is not a home owner. For the loans for which borrower is a home owner, they are further split and classified into defaulting and non-defaulting by using the `Term' predictor. If the length of term is longer than 12 months, the loan may be classified as defaulting. Otherwise, the loan is classified as non-defaulting. For loans where the borrower is not a home owner, the feature `Debt To Income Ratio' (DTI) is used to split and classify, i.e. all loans with DTI greater than 0.3 are classified as defaulting and the other loans with DTI smaller or equal to 0.3 are classified as non-defaulting.

\subsection*{Random forest}
\acf{RF} have several advantages: they are easy to tune, they can handle categorical variables, and they run relatively efficiently for large datasets. In this thesis, a brief description of the main ideas of \ac{RF} is provided, the reader is referred to Breiman~\cite{breiman2001random} for a more in-depth discussion on \ac{RF}.

Given a training vector $\mathbf{x} = \{x_{1}, \ldots, x_{n}\}$ with a categorical response vector $\mathbf{Y} = \{y_{1}, \ldots y_{n}\}$, repeatedly choose random samples\footnote{These samples are bootstrap samples which have the same size as the training sample.} to replace the training dataset and then fit decision trees~\cite{friedman2001elements} to those $K$ samples with the following steps:

\begin{enumerate}
	\item For each sample, $k = 1,\ldots,K$, let the new training data	and response variable be $\mathbf{x}_{k}$ and $\mathbf{Y}_{k}$,	respectively.
	\item Train a decision tree $\hat{f}_{k}$ on each sample $k$.
	\item After training $K$ decision trees, the response for a new sample	(or new loan $x'$ in the \ac{P2PL} system) can then be predicted by averaging the predictions from each decision tree, using the formula below:
	\[
	\hat{f} = \frac{1}{K}\sum\limits_{k=1}^{K}\hat{f}_{k}(x')
	\]
\end{enumerate}

The above procedure describes the bootstrap aggregating technique for trees. However, \ac{RF}s use the random selection of predictors at each interior node to determine the split for each decision tree $\hat{f}_{k}$. For example, in Figure~\ref{fig:tree}, 3 predictors (features) are selected, then for each node of a decision tree $\hat{f}_{k}$, only one of these three predictors is allowed to be used for splitting and a new selection of 3 predictors is made for another node. For each decision tree $\hat{f}_{k}$, a depth, which represents the level to stop splitting the tree, needs to be carefully selected. Usually the deeper the tree, the more splits it has and the more information it captures. However, choosing a large number for the depth will definitely increase the execution time for the training phase.

\acf{RF} are resistant to over-fitting with growing number of decision trees and hence, one can build a large number of trees to reduce bias. However, the larger the size $K$, the longer the execution time. Within the process of splitting, \textit{Gini impurity}~\cite{raileanu2004theoretical} is used as the metric for measuring the homogeneity of the target variable. \textit{Gini impurity} is a measure that a randomly chosen element from the set would be misclassified. If two elements are randomly selected from a set, then the set is pure if they are of same class and probability of this is one. To compute \textit{Gini impurity} of a particular predictor $Z_{i}$ with $J$ classes, where a class indicates the possible values that the predictor can take, denote all possible classes as $C_{1}, \dots , C_{J}$, then the equation for computing the \textit{Gini impurity} can be described as shown in equation (\ref{eq:gini}):
\begin{equation}\label{eq:gini}
G(Z_{i})=1-\sum_{j=1}^{J}Pr(Z_{i} = C_{j})^{2}
\end{equation}
After \textit{Gini Impurities} are computed for each split predictor, the split is done with the predictor that has the smallest value of \textit{Gini Impurity}.

\subsection{\acf{SVM}}
\label{Ch2:SVM}

\acf{SVM} is another popular algorithm for classification, which can be used to capture linear or non-linear relationships between predictors and response variable. In addition, \ac{SVM} can also be used for regression which is also known as \acf{SVR}. Specifically, \ac{SVM} constructs one or more hyperplanes to best separate data corresponding to different values of the response variable. In two dimensional space, the hyperplane is a line dividing a plane in two parts where in each class lay on either side. Figure~\ref{fig:svm} provides an illustrative example of such separating (decision) surfaces for data from two classes. In Figure~\ref{fig:svm}, there are three hyperplanes ($H_{1}$, $H_{2}$ and $H_{3}$). In order to determine the hyperplanes which separates surfaces (or classifies data) from two classes the best, the \textit{margin} is used as the metric. In the example, the margin is the distance of the hyperplane to the closest class points. From the example in Figure~\ref{fig:svm}, the hyperplane $H_{1}$ does not separate the classes. The hyperplane $H_{2}$ separates the classes, but only with a small margin. The hyperplane $H_{3}$ separates data with the maximum margin, i.e. it is the optimal separating surface. Specifically, the response variable is determined by the optimal separating surface $H_{3} = f(\mathbf{x})$. The maximum margin separating surface $H_{3}$ is defined by using a kernel function $K(\mathbf{x},\mathbf{x}')$, which is the equivalent of projecting data to an infinite dimensional space using basis functions. 

\begin{figure}[tbh] \centering
	\includegraphics[width=4.3in]{svm}
	\caption{Hyperplanes of \ac{SVM} for separating surfaces for data from two classes in the linear case. The black dots and circles represent two different classes.~\label{fig:svm}. Reproduced from~\cite{WikipediaSVM}.}
\end{figure}

\ac{SVM}s also have several advantages. They are effective in high dimensional space with many features. They execute efficiently for large datasets, and can handle diverse problems with different kernel functions. However, \ac{SVM}s are weak in transparency of results. In other word, \ac{SVM}s cannot estimate the probability or score directly, because \ac{SVM}s always use hyperplanes to solve
classification problems.

\subsection{\acf{k-NN}}
\label{Ch2:k-NN}
\begin{figure}[h!] \centering
	\includegraphics[width=4.3in]{knn}
	\caption{Example for \ac{k-NN} classification~\label{fig:knn}. Blue squares and red triangles represents two different classes (e.g. defaulting and non-defaulting). Green dot is the test point (or new borrower) that needs to be classified. Reproduced from~\cite{Wikipediaknn}.}.
\end{figure}
\acf{k-NN} is another popular machine learning algorithm that is used in recent works~\cite{mezei2018credit, rui2012bayesian} on \ac{P2PL} systems. \ac{k-NN}  is a type of unsupervised learning or instance-based learning technique. Instead of building a certain internal model, \ac{k-NN}  simply stores instances of the training data. Specifically, \ac{k-NN} finds $k$ training samples nearest to the new point and predicts from these samples. In addition, \ac{k-NN} can be used for both classification and regression. In \ac{k-NN} classification, the new point is classified by a majority vote of the $k$ nearest neighbors, where the new point is assigned to the class which has the most points among these $k$ nearest neighbors. In \ac{k-NN} regression, the predicted values is computed by taking the average of the values of $k$ nearest neighbors. Figure~\ref{fig:knn} provides an illustrative example of the \ac{k-NN} classification. The green dot is the test point that needs to be classified as either blue square or red triangle. If $k=3$, then the test point should be classified as red triangle because there are two red triangles and only one blue square within distance $\epsilon$ from the green dot. In the case that $k=5$, the test point is classified as blue square since there are more blue squares (3) than red triangles (2).

\ac{k-NN} is easy to interpret and applicable for nonlinear data. However, \ac{k-NN} is sensitive to irrelevant features and the scale of the data, hence feature selection and data balancing are essential for \ac{k-NN}. In addition, because \ac{k-NN} has to store all training samples, it is computationally expensive compared to other techniques.

\section{Feature selection}\label{Ch2: Feature selection}
Feature selection, also known as attribute selection, is the process of choosing a smaller subset of the most relevant features for use in classification/regression. There are mainly three types of feature selection: 
\begin{enumerate}
	\item \textbf{Filter-based methods}: the filter-based methods investigate the relevance of the features measured with univariate statics such as information gain, chi-square test and correlation coefficient.
	\item \textbf{Wrapper-based methods}: the wrapper-based methods measure the importance of features with the classifier performance. For example, forward selection and backward selection.
	\item \textbf{Embedded methods}: the embedded methods are similar to the wrapper-based methods. However, the embedded methods apply the intrinsic model during learning. For example, LASSO regularization and RIDGE regression.   
\end{enumerate}
In this work, the wrapper-based methods like forward selection, backward selection, and recursive selection are discussed. 


The reasons for performing feature selection in \ac{P2PL} are: \textcircled{1} eliminate uninformative features that cause reduced accuracy of classification and regression. \textcircled{2} Reducing the execution time to train the model. \textcircled{3} Simplifying models to make them easier to interpret~\cite{james2013introduction}.

To achieve these goals, many feature selection techniques have been studied in the research literature. The recent work in ~\cite{tsai2014peer} uses a number of methods to investigate the most relevant features from \ac{P2PL} dataset.

\begin{enumerate}
	\item[1.] Evaluate the importance of a feature by measuring the information gain of the feature. The information gain of a feature can be formulated as shown in equation (\ref{eq:ig})
	\begin{equation}\label{eq:ig}
	\begin{aligned}
		&\text{Information gain}(\text{response variable, feature}) &=  &H(\text{response variable}) \\ &&-& H(\text{response variable }| \text{ feature}).
	\end{aligned}
	\end{equation}
	In equation (\ref{eq:ig}), $H( )$ is the entropy function which is defined in equation (\ref{eq:entro}) and 
	$H(\cdot|\cdot )$ is the conditional entropy function which is defined in equation (\ref{eq:entro_con})
	\begin{equation}\label{eq:entro}
		H(Y) = -\sum_{y\in Y} p(y)\log_{2}(p(y)),
	\end{equation}
	\begin{equation}\label{eq:entro_con}
	H(Y|X) = -\sum_{x\in X,y \in Y} p(x,y)\log(\frac{p(x,y)}{p(x)}).
\end{equation}
	In equations (\ref{eq:entro}) and (\ref{eq:entro_con}), $Y$ and $X$ are two random variables, and $p(y)$ is the fraction of the total data in a given class.
	\item[2.] Perform correlation-based feature subset selection~\cite{hall1998correlation} using genetic algorithm search \cite{goldberg1989genetic}. This technique evaluates the value of a subset of features by considering the individual predictive ability of each feature and minimize redundancy.
	\item[3.] Perform standard sequential forward feature selection. 
\end{enumerate}
The work in ~\cite{cui2016p2p} develops a new feature selection method that transforms all predictors and the response variable into graph-based features\footnote{The graph-based features are completed weighted graphs and the steady state random walk is used to  encapsulate the main characteristics.} thereby selecting the most relevant subset of the graph-based features. The work in~\cite{serrano2015determinants} shows the performance of 7 \ac{LOGIT} models, each with a different subset of features to determine the subset that fits the sample best, i.e. generate the highest total accuracy. However, those works only focus on the value of total accuracy, and total accuracy can not explain the fitness of the model when the dataset is unbalanced. For example, if 90\% of a sample dataset are non-defaulting borrowers, then one model can obtain total accuracy of 90\% by simply predicting all borrowers as non-defaulting.

Three feature selection algorithms are studied in this thesis: forward selection~\cite{friedman2001elements} , backward selection~\cite{marill1963effectiveness} and recursive selection~\cite{guyon2002gene}. The overview of the three algorithms is as follows:
\begin{itemize}
	\item \textbf{Forward selection}: Forward selection~\cite{friedman2001elements} is a well studied iterative method. In forward selection, it first starts with an empty feature set. Then, for each individual feature, runs the predictive model and computes the p-value associated with the t-test. In each iteration, a feature is added that has the lowest p-value. This process is repeated until no improvement of the total accuracy of the predictive model is obtained on adding a new feature. 
	\item \textbf{Backward selection}: Backward selection~\cite{friedman2001elements} is another prominent  algorithm to select a subset of features. In backward selection, it first starts with the full feature set, which includes all the features. Then, for each individual feature, the predictive model is run and the associated p-value is computed with the t-test. Next, the algorithm keeps on dropping the feature with the largest p-value. This process is repeated until no improvement in of the total accuracy of the predictive model is obtained.
	\item \textbf{Recursive selection}: Recursive selection~\cite{guyon2002gene} is a kind of backward selection. Similar to backward selection, it starts from the full feature set that contains all the features. However, at each iteration, it repeatedly builds models and removes the wost performing feature, i.e. the feature with the largest p-value in this predictive model. With the rest of the features, it creates a new predictive model and repeats the above process until all features are exhausted. Then, it selects the subset of features, which results in the highest total accuracy, as the best subset.
\end{itemize}

\section{Optimisation models for helping lenders obtain a prudent investment portfolio}\label{Ch2: Optimisation model}
In recent years, a great deal of research~\cite{Guo2016417,ren2019investment, babaei2020multi} has gone into developing recommendation systems to help lenders achieve a prudent investment portfolio on a P2PL system. In order to help lenders make profit in \ac{P2PL}, an optimisation model is a good option to use for achieving the desired investment portfolio.  The modern optimal portfolio construction theory of the well known return-risk trade-off Markowitz frontier~\cite{markowitz1991foundations} is the most popular model used in portfolio generation (e.g. stock investment). Let $w \in \mathbb{R}^{N}$ (output) be the vector of the proportion of investment split into $N$ loans and matrix $\Sigma \in \mathbb{R}^{N \times N}$ (input) be the covariance matrix between these $N$ loans. Let $\mu \in \mathbb{R}^{N}$ (input) be the vector of expected return rates. Then, the mean-variance optimisation problem can be formulated as shown in model~(\ref{model:mv}).
\begin{equation}\label{model:mv}
\begin{aligned} 
&\min        && - \mu^{T}w + \lambda w^{T} \Sigma w \\
&\text{s.t.}&& \mathbf{1}^{T}w = 1.\\
\end{aligned}
\end{equation}
\noindent

In model (\ref{model:mv}), $\lambda$ is a controlled variable varying from 0 to $+\infty$ that represents the lender's behavior on investment. Specifically, a risk-averse lender will take small value of $\lambda$, and a risk-seeking lender will take large value of $\lambda$.

However, unlike the stock market, most of the borrowers only borrow once on a \ac{P2PL} online platform. Hence, it is very difficult to specify the correlation between available loans on the \ac{P2PL} online platform. Thereby, the correlation between different loans is usually assumed to be 0 (i.e. no correlation) when studying \ac{P2PL} systems. As a result, the risk objective in model~\ref{model:mv} needs to be reconsidered. In this section, two current state-of-the-art models are reviewed.

\subsection{Instance-based optimisation model}\label{Ch2: Instance-based}
In a \ac{P2PL} online platform, most borrowers only borrow once. To capture this issue in an optimisation model, the work in~\cite{Guo2016417} introduces an instance-based model which uses similar loans (borrowers with similar characteristics) to predict the likelihood of defaulting of a new loan request. For a given new loan $i$, based on $K$ historical loans, each with the return rate $u_{k}$ for all $k\in\{1,2,\dots, K\}$, the predicted return rate $\mu_{i}$ of the new loan $i$ can be computed by equation (\ref{eq: return_instance}).
\begin{equation}\label{eq: return_instance}
\mu_{i} = \sum_{k=1}^{K}wt_{ik}u_{k},
\end{equation}
\noindent
The weighted variance $\sigma^{2}_{i}$ can be computed by equation (\ref{eq: risk_instance}).
\begin{equation}\label{eq: risk_instance}
\sigma^{2}_{i} = \sum_{k=1}^{K}wt_{ik}(u_{k}-\mu_{i}),
\end{equation}
\noindent
In equations (\ref{eq: return_instance}) and (\ref{eq: risk_instance}), $wt_{ik}$ represents the weight of the historical loan $k$ influencing the prediction of the new loan request $i$.

In particular, weight $wt_{ik}$ is computed by using kernel regression~\cite{nadaraya1965non} with kernel function as shown in equation (\ref{eq:kernel}).
\begin{equation}\label{eq:kernel}
Ker(x) = \frac{1}{\sqrt{2\pi}}e^{-\frac{1}{2}x^{2}}.
\end{equation}

Hence the equation to compute weight $wt_{ik}$ can be formulated as shown in equation (\ref{eq:weight}).
\begin{equation}\label{eq:weight}
wt_{ik} = \frac{Ker(\frac{|p_{i}-p_{k}|}{h})}{\sum_{k=1}^{K}Ker(\frac{|p_{i}-p_{k}|}{h})},
\end{equation}
\noindent
In equation (\ref{eq:weight}), $p_{i}$ and $p_{k}$ are the probabilities of default of new loan $i$ and historical loan $k$, respectively. The parameter $h$ is called the \textit{bandwidth}, which defines the proportion of local versus remote information used in the summation. To find the optimal bandwidth $h$ that best fits the kernel regression, the leave-one-out least-squares cross validation method~\cite{clark1975calibration} is selected. 

After predicting the return rate and variance of a new loan request, the instance-based model finds the investment portfolio by minimising the portfolio risk with a targeted expected return rate $R$. The optimisation model is formulated as shown in model (\ref{model:mib}).

\begin{equation}\label{model:mib}
\begin{aligned} 
&\min        && \sum_{i=1}^{N}w_{i}^{2}\sigma_{i}^{2} \\
&\text{s.t.}&& \mathbf{1}^{T}w = 1,\\
& && \sum_{i=1}^{N}w_{i}\mu_{i}=R.\\
\end{aligned}
\end{equation}

This instance-based model overcomes the lack of historical observations of the new loan request. However, it requires the lenders to give a fixed expected return rate to execute. In addition, the risk of the new loan is usually difficult to determine, thus one needs to find a method to accurately predict the risk first. Moreover, this instance-based model is computationally expensive when the training dataset is large.

\subsection{The return-default trade-off optimisation model}\label{Ch2: return-risk}
\begin{table}[b]
	\caption{Average return rate and probability of default for each loan grade}
	\label{tab:return default}
	\begin{center}
		\begin{tabular}{ccc} \toprule
			Grade & Average return rate  & Probability of default \\ \midrule
			AA & 0.0697 & 0.0466 \\ 
			A & 0.0984 & 0.112 \\ 
			B & 0.148  & 0.174 \\ 
			C & 0.194  & 0.220 \\ 
			D & 0.246 & 0.286 \\ 
			E & 0.296 & 0.346 \\ 
			HR & 0.309  & 0.382 \\ \bottomrule
		\end{tabular}
	\end{center}
\end{table}
Different from the instance-based model described in subsection~\ref{Ch2: Instance-based}, the work in~\cite{ren2019investment} uses a different method to overcome the lack of historical data for new borrowers. Instead of predicting the return rate and variance of each individual new loan request, they treat each grade of loans as a group and thereby compute the return rate and default probability of each grade group. For example, in Prosper, there are 7 different grades: AA, A, B, C, D, E, HR. The average return rate and probability of default for each individual grade are listed in Table~\ref{tab:return default}. The average return rate and probability of default are computed from the funded loan dataset. For each loan grade, the average return rate is computed by taking the mean value of the return rates of all loans in the grade. The probability of default for each loan grade is computed by measuring the percentage of defaulting borrowers in the grade. Then, the return rate and risk of any new loan request is defined by its loan grade with the values shown in Table~\ref{tab:return default}. For example, the return rate and probability of default of a new loan with AA grade are 0.0697 and 0.0466, respectively.

After evaluating the return rates and the probabilities of default of the new loans request, a return-default trade-off optimisation model can be formulated as shown in model (\ref{model:return risk}):
\begin{equation}\label{model:return risk}
\begin{aligned} 
&\min        && - \mu^{T}w + \lambda w^{T} \Sigma w\\ 
&\text{s.t.}&& \mathbf{1}^{T}w = 1, \\
&            && w \succeq 0,\\
&\text{and}  && w \in \mathbb{R}^{N}\\
\end{aligned}
\end{equation}
In model (\ref{model:return risk}), $N$ is the number of new loans available on the marketplace. Parameter $\lambda$ is a controlled variable from $0$ to $+\infty$, $\mu \in \mathbb{R}^{N}$, $\Sigma \in \mathbb{R}^{N\times N}$, and $\mathbf{1}$ is a $N$-degree vector of all ones. Specifically, $\mu$ is a column vector containing the return rate of loans available for investment. Matrix $\Sigma$ is the covariance matrix with the square of the probability of default of all loans on the diagonal and 0 in the rest. In particular, all loans are assumed as being mutually independent of each other. The column vector $w$ represents the proportion of the available amount to be invested, which is assumed to be 1 unit, to be invested into each individual loan.

This return-default trade-off optimisation model can provide a set of Pareto-optimal solutions~\cite{zitzler1999multiobjective}. To determine a preferred solution from the set of Pareto-optimal solutions, Sharpe ratio~\cite{sharpe1994sharpe} is used and can be written as shown in equation (\ref{eq:sharpe}).
\begin{equation}\label{eq:sharpe}
S = \frac{\mu_{port} - \mu_{rf}}{\sigma_{port}}
\end{equation}
In equation (\ref{eq:sharpe}), $\mu_{port}$ represents the portfolio return rate which can be computed by equation (\ref{eq:port_ret}). $\mu_{rf}$ stands for the benchmark rate, and $\sigma_{port}$ represents the corresponding portfolio probability of default which can be computed by equation (\ref{eq:port_risk}).

\begin{equation}\label{eq:port_ret}
\mu_{port} = \mu^{T}w 
\end{equation} 


\begin{equation}\label{eq:port_risk}
\sigma_{port}^{2} =  w^{T} \Sigma w
\end{equation} 

This return-default trade-off optimisation model can also overcome the lack of historical observations of the new loan request. In addition, it provides a set of portfolio solutions for the lender without expected return rate provided by  the lender. It also gives the lender a preferred solution by applying the Sharpe ratio. However, this model assumes new loans with the same grade have the same return rate and risk. Thus, only loan grade is used in this model and all other characteristics/features of a new borrower are not considered.


\section{Discussion}\label{Ch2: Discussion}

This chapter presented a survey of several popular machine learning techniques and feature selection algorithms. There are also other machine learning techniques used for classification and regression (e.g. Gaussian process~\cite{rasmussen2003gaussian} and neural networks~\cite{specht1991general}). The five machine learning techniques described in Section~\ref{Ch2:Machine} are the most popular techniques used in \ac{P2PL} systems. However, recent works~\cite{tsai2014peer,cui2016p2p,serrano2015determinants} only focus on the value of total accuracy, which can not be used as the only metric to explain the fitness\footnote{The ability to accurately classify the borrowers as defaulting or non-defaulting.} of the model when the dataset is unbalanced.

Based on the data obtained from the \ac{P2PL} online platform Prosper\footnote{www.prosper.com}, Chapter 3 explores the use of machine learning algorithms for predicting the likelihood of borrowers defaulting. Chapter 3 also compares several feature selection algorithms to find the best subset of features for predicting the likelihood of default. Such a feature selection process is necessary for reducing execution time and increasing predictive accuracy. As opposed to the current state-of-the-art approaches, the proposed method does not only consider the total accuracy but also takes account of the recall rates of both defaulting and non-defaulting borrowers to validate the fitness of a model. Moreover, Chapter 3 experimentally studies the impact of an unbalanced dataset on predicting the likelihood of the borrowers defaulting and finds the best way to balance the dataset. Finally, an investment portfolio for an individual lender is provided at the end of Chapter 3.

Low financial liquidity of \ac{P2PL} platforms~\cite{Roth20121115} is also a critical consideration. Specifically, there are not always enough loans available for investment on the \ac{P2PL} online platform. The current state-of-the-art models do not remedy this shortcoming and can only work under the assumption that there are enough loans of each grade on the \ac{P2PL} online platform available for investment at any given instant. To deal with this issue, Chapter 4 provides a new investment recommendation system for low-liquidity \ac{P2PL} online marketplaces. This new system not only finds an investment portfolio which results in the highest Sharpe ratio, but also predicts the number of days required to realise the investment portfolio.

For the purpose of finding a prudent investment portfolio for lenders in the \ac{P2PL} systems, many recommendation models have been proposed.  However, all these models are designed for a single lender and thereby not applicable for the case of multiple lenders. Since there are limits on investment for each individual loan, the lender who invests all funds available in a particular loan will wipe out this loan from the \ac{P2PL} online platform. In fact, many lenders choose a portfolio manager to help them invest rather than invest themselves. As a result, it is necessary to develop a recommendation system for multiple lenders. To address this problem, Chapter 5 proposes a novel recommendation system for multiple lenders for \ac{P2PL} investing. In particular, this recommendation system constructs a multi-lender integer linear programming formulation that achieves a set of Pareto-optimal portfolio solutions followed by a preferred solution with the highest Sharpe ratio.

Borrowers are also playing a key role in \ac{P2PL} systems. However, not too much research~\cite{althoff2014ask,herzenstein2011tell} has been done for helping borrowers get funded at lower interest rates. Chapter 6 presents a recommendation system that advises any new borrowers on applying for the right type of loans. Using this recommendation system, any new borrower can achieve lowered interest rates with a high chance of getting funded.