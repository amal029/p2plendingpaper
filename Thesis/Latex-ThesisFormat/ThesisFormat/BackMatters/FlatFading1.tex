\chapter{Description and data analysis of three labels}\label{AppendixB}

This appendix gives the description and analysis of the situation which considers the third label in `borrower status' --- prepayment.

In the \ac{P2PL} system, borrowers can complete the loan they requested before the maturity date. In practice, a lot of borrowers tend to complete the loans early, thereby resulting the prepaid loans. As a result the non-default loans should be divided into fully paid loans and prepaid loans. For example, in the funded loan dataset described in Section~\ref{Ch1: funded dataset}, there are total 18,572 completed loans that consist of 885 default loans, 2441 fully paid loans and 15,246 prepaid loans. Specifically, the prepaid loans accounts for up to 82\% of the total completed loans. The recent work~\cite{li2016prepayment} reveals that many borrowers prepay their loans before the date of maturity, and thereby reducing the total interest payable. As a result, lenders will lose some returns. However, instead of avoiding to invest in a prepaid loan, the lenders should invest in a prepaid loan. Because, lenders can reinvest their investment into a new loan to obtain more profit. 

\begin{table}[h!]
	\centering
	\caption{The resultant amount of money by investing in fully paid loan and prepaid loans}
	\label{tab: ap: profit}
			\begin{tabular}{lllll}
				\toprule
				       &Loan &  0 month & 6 months & 12 months \\ \midrule
				Lender 1    &  Loan A  & \$100 (invest)  &  --- & \$104 (obtain) \\ 
			Lender 2      &    Loan B  &  \$100 (invest) & \$102 (obtain) & --- \\ 
				       &   Loan C  & ---  &    \$102 (reinvest) & \$104.04 (obtain) \\ \bottomrule
			\end{tabular}
\end{table} 

For example, consider three loans that are matured in a year with 4\% return rate. Loan A is completed at the date of maturity, while Loan B and C are completed early with 6 months. Assume Loan A and B start at the same time, and Loan C starts 6 months after. Then, assume there are two lenders with \$100, Lender 1 invests in Loan A and Lender 2 first invest in Loan B and then Loan C. Then, the total amount of money that the two lenders obtain after one year is showed in Table~\ref{tab: ap: profit}. It can be observed that Lender 2 will earn more money than Lender 1. As a result, lenders should invest prepaid loans more, and thereby achieving more money. Therefore, to validate the quality of a loan, the annualised return rate (ARR) of a loan (the rate of return scaled to a one-year period) should be computed. Let $E$ is the total amount that the borrower paid back on the date of closing (the closing date might be before the original loan term specified, in case of prepayment),  $L$ stands for the original amount (the principal) borrowed, and $T$ is period in year. Then the \ac{ARR} can be formalised in equation~(\ref{eq:ap:arr}).

\begin{equation}\label{eq:ap:arr}
\text{ARR} = \frac{E}{L}^{1/T}-1.
\end{equation}

To accurately classify the loans into full payment, prepayment and default, \ac{MLOGIT} and \ac{RF} is used. Recall that the funded loan dataset set consists of 885 default loans, 2441 fully paid loans and 15,246 prepaid loans, which results a ratio of default:full payment:prepayment $\approx 1: 2.8 : 17$. To avoid the negative impact of the unbalanced dataset, 885 fully paid and prepaid loans together with the 885 default loans are randomly selected as the balanced sample dataset. Then, this balanced sample dataset is divided into a ratio of 80\% for training and 20\% for testing. By applying the recursive feature selection method, the result are shown in Tables~\ref{tab:ap:afrf},~\ref{tab:ap:5m} and~\ref{tab:ap:3labels}. It is clear that the recursive feature selection algorithm can improve the accuracy wile classifying. However, even the highest accuracy 65\% obtained by \ac{RF} is too bad to classify the three labels. Hence, it would be required to invent a new technique to improve the result.
\begin{table}[ht]
	\caption{The overall accuracy of three-label classification on
		test dataset with and without feature selection.}
	\label{tab:ap:afrf}
	\centering
	% \begin{scriptsize}
	\begin{tabular}{lll}
		\toprule
		Technique& All features & Recursive selection\\ \midrule
		\ac{MLOGIT} & 0.53 & 0.60\\
		\ac{RF} & 0.57 & 0.65 \\ \bottomrule

	\end{tabular}
	% \end{scriptsize}
\end{table}

\begin{table}[ht!]
	\caption{ Accuracy of five Monte Carlo CV results for three-label borrower status classification}
	\label{tab:ap:5m}
	\centering
		\begin{tabular}{lllllll}
			\toprule
			Technique & 1 & 2 & 3 & 4 & 5 & Average \\ \midrule
			\ac{MLOGIT} & 0.58 & 0.60 & 0.61 & 0.59 & 0.63 & 0.60\\
			\ac{RF} & 0.63 & 0.65 & 0.67 & 0.65 & 0.66 & 0.65\\ \bottomrule
		\end{tabular}
\end{table}

\begin{table}[ht!]
	\caption{The average accuracy scores for each of the three
		labels on test datasets. FP: Full payment, PP: Prepayment, D:
		Default.}
	\label{tab:ap:3labels}
	\centering
		% \begin{scriptsize}
		\begin{tabular}{llll}
			\toprule
			Technique & FP & PP & D\\ \midrule
			\ac{MLOGIT} & 0.78 & 0.41 & 0.64 \\ 
			\ac{RF} & 0.80 & 0.47 & 0.70 \\ \bottomrule
		\end{tabular}
\end{table}

Recall from Section~\ref{Ch5: Experiment}, the accuracy of two-label classification is about 0.79 (79\%). Hence, the borrowers are classified as defaulting and non-defaulting at first. Then, for those borrowers that are classified as non-defaulting, further classify them as full payment and prepayment by selecting new set of features with recursive feature selection algorithms. This technique is called as \ac{H3LC}. The results by applying this technique is shown in Table~\ref{tab:ap:h3lc}. It can be observed that the accuracy increases from 0.65 (65\%)  to 0.78 (78\%) by applying the \ac{H3LC} with \ac{RF} technique. 

\begin{table}[ht]
	\caption{The overall accuracy five Monte Carlo CV results of three-label classification.}
	\label{tab:ap:h3lc}
	\centering
	% \begin{scriptsize}
 \begin{tabular}{lllllll}
	\toprule
	Technique & 1 & 2 & 3 & 4 & 5 & Average \\ \midrule
	\ac{H3LC}\_\ac{RF} & 0.76 & 0.78 & 0.80 & 0.79 & 0.75 & \textbf{0.78}\\
	\ac{H3LC}\_\ac{LOGIT} & 0.67 & 0.65 & 0.64 & 0.68 & 0.70 & 0.67\\ \bottomrule
\end{tabular}
	% \end{scriptsize}
\end{table}

However, 0.78 (78\%) is still not good enough to classify the loans. In addition, this would also require to study how to predict the \ac{ARR} by using these results, thereby achieving the preferred investment portfolio. These directions are planned to address in the future.