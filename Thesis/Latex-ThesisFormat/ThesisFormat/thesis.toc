\thispagestyle {empty}
\contentsline {chapter}{\underline {Table of Contents}}{xiii}{chapter*.5}%
\contentsline {chapter}{List of figures}{xvii}{chapter*.10}%
\contentsline {chapter}{List of tables}{xxi}{chapter*.14}%
\contentsline {chapter}{\numberline {1}Introduction}{1}{chapter.1}%
\contentsline {section}{\numberline {1.1}Motivations}{2}{section.1.1}%
\contentsline {section}{\numberline {1.2}Data description}{3}{section.1.2}%
\contentsline {subsection}{\numberline {1.2.1}Funded loan dataset}{4}{subsection.1.2.1}%
\contentsline {subsection}{\numberline {1.2.2}Bidding loan dataset}{8}{subsection.1.2.2}%
\contentsline {section}{\numberline {1.3}Basic background knowledge of P2PL}{9}{section.1.3}%
\contentsline {subsection}{\numberline {1.3.1}Unbalanced dataset}{9}{subsection.1.3.1}%
\contentsline {subsection}{\numberline {1.3.2}Grade rating}{10}{subsection.1.3.2}%
\contentsline {subsection}{\numberline {1.3.3}Type of loans}{10}{subsection.1.3.3}%
\contentsline {section}{\numberline {1.4}Thesis contribution}{11}{section.1.4}%
\contentsline {section}{\numberline {1.5}Thesis organisation}{12}{section.1.5}%
\contentsline {chapter}{\numberline {2}Literature review}{15}{chapter.2}%
\contentsline {section}{\numberline {2.1}Machine learning techniques}{16}{section.2.1}%
\contentsline {subsection}{\numberline {2.1.1}Linear regression}{16}{subsection.2.1.1}%
\contentsline {subsection}{\numberline {2.1.2}\acf {LOGIT}}{17}{subsection.2.1.2}%
\contentsline {subsection}{\numberline {2.1.3}\acf {RF}}{17}{subsection.2.1.3}%
\contentsline {subsection}{\numberline {2.1.4}\acf {SVM}}{20}{subsection.2.1.4}%
\contentsline {subsection}{\numberline {2.1.5}\acf {k-NN}}{21}{subsection.2.1.5}%
\contentsline {section}{\numberline {2.2}Feature selection}{23}{section.2.2}%
\contentsline {section}{\numberline {2.3}Optimisation models for helping lenders obtain a prudent investment portfolio}{25}{section.2.3}%
\contentsline {subsection}{\numberline {2.3.1}Instance-based optimisation model}{26}{subsection.2.3.1}%
\contentsline {subsection}{\numberline {2.3.2}The return-default trade-off optimisation model}{27}{subsection.2.3.2}%
\contentsline {section}{\numberline {2.4}Discussion}{29}{section.2.4}%
\contentsline {chapter}{\numberline {3}Feature selection and likelihood of default prediction}{31}{chapter.3}%
\contentsline {section}{\numberline {3.1}Introduction}{31}{section.3.1}%
\contentsline {section}{\numberline {3.2}Data distribution}{33}{section.3.2}%
\contentsline {section}{\numberline {3.3}Encoding of categorical features}{36}{section.3.3}%
\contentsline {subsection}{\numberline {3.3.1}The one-hot encoding technique}{37}{subsection.3.3.1}%
\contentsline {subsection}{\numberline {3.3.2}The ordinal encoding technique}{39}{subsection.3.3.2}%
\contentsline {subsection}{\numberline {3.3.3}Likelihood encoding}{39}{subsection.3.3.3}%
\contentsline {subsection}{\numberline {3.3.4}Encode the categorical features by applying suitable encoding techniques}{40}{subsection.3.3.4}%
\contentsline {section}{\numberline {3.4}Balancing the unbalanced dataset}{41}{section.3.4}%
\contentsline {subsection}{\numberline {3.4.1}Experimental setup}{42}{subsection.3.4.1}%
\contentsline {subsection}{\numberline {3.4.2}Results of metrics on different models}{43}{subsection.3.4.2}%
\contentsline {subsection}{\numberline {3.4.3}Identifying the best balancing ratio}{44}{subsection.3.4.3}%
\contentsline {section}{\numberline {3.5}Feature selection}{48}{section.3.5}%
\contentsline {subsection}{\numberline {3.5.1}Experimental setup}{48}{subsection.3.5.1}%
\contentsline {subsection}{\numberline {3.5.2}Experimental results}{49}{subsection.3.5.2}%
\contentsline {subsection}{\numberline {3.5.3}Feature set comparison}{52}{subsection.3.5.3}%
\contentsline {section}{\numberline {3.6}Recommending the investment portfolio for an individual lender}{53}{section.3.6}%
\contentsline {subsection}{\numberline {3.6.1}Experimental setup}{54}{subsection.3.6.1}%
\contentsline {subsection}{\numberline {3.6.2}A numerical example}{54}{subsection.3.6.2}%
\contentsline {section}{\numberline {3.7}Discussion}{57}{section.3.7}%
\contentsline {chapter}{\numberline {4}Recommendation system for low-liquidity P2PL system}{59}{chapter.4}%
\contentsline {section}{\numberline {4.1}Introduction}{59}{section.4.1}%
\contentsline {section}{\numberline {4.2}The overall proposed methodology and data analysis}{61}{section.4.2}%
\contentsline {subsection}{\numberline {4.2.1}The overall proposed methodology}{61}{subsection.4.2.1}%
\contentsline {subsection}{\numberline {4.2.2}Data analysis}{62}{subsection.4.2.2}%
\contentsline {section}{\numberline {4.3}Probability-based prediction model}{64}{section.4.3}%
\contentsline {section}{\numberline {4.4}Return-risk trade-off optimisation and bi-objective optimisation}{73}{section.4.4}%
\contentsline {subsection}{\numberline {4.4.1}Return-risk trade-off optimisation}{74}{subsection.4.4.1}%
\contentsline {subsection}{\numberline {4.4.2}Appearance probability and waiting days optimisation}{76}{subsection.4.4.2}%
\contentsline {section}{\numberline {4.5}Experimental results}{79}{section.4.5}%
\contentsline {subsection}{\numberline {4.5.1}Investing by using the current state-of-the-art model}{79}{subsection.4.5.1}%
\contentsline {subsection}{\numberline {4.5.2}Investing by applying the proposed model}{80}{subsection.4.5.2}%
\contentsline {subsection}{\numberline {4.5.3}Comparison with historical data}{82}{subsection.4.5.3}%
\contentsline {section}{\numberline {4.6}Discussion}{83}{section.4.6}%
\contentsline {chapter}{\numberline {5}Recommendation system for multiple lenders}{85}{chapter.5}%
\contentsline {section}{\numberline {5.1}Introduction}{86}{section.5.1}%
\contentsline {section}{\numberline {5.2}Modern portfolio models}{87}{section.5.2}%
\contentsline {section}{\numberline {5.3}Data analysis and predictive model selection}{90}{section.5.3}%
\contentsline {subsection}{\numberline {5.3.1}Data analysis}{90}{subsection.5.3.1}%
\contentsline {subsection}{\numberline {5.3.2}Predictive model selection}{90}{subsection.5.3.2}%
\contentsline {section}{\numberline {5.4}Weighted Multi-lender portfolio integer optimisation problems}{91}{section.5.4}%
\contentsline {subsection}{\numberline {5.4.1}The generic weighted optimisation model}{92}{subsection.5.4.1}%
\contentsline {subsection}{\numberline {5.4.2}Portfolio risk linearisation}{94}{subsection.5.4.2}%
\contentsline {subsection}{\numberline {5.4.3}Individual and global constraints}{95}{subsection.5.4.3}%
\contentsline {subsection}{\numberline {5.4.4}Weighted multi-lender portfolio integer optimisation model}{97}{subsection.5.4.4}%
\contentsline {subsection}{\numberline {5.4.5}Metric to identify a preferred investment portfolio}{99}{subsection.5.4.5}%
\contentsline {section}{\numberline {5.5}Comparison of building an investment portfolio for multiple lenders independently vis-a-vis simultaneously}{100}{section.5.5}%
\contentsline {subsection}{\numberline {5.5.1}Model to build an investment portfolio for multiple lenders independently}{101}{subsection.5.5.1}%
\contentsline {subsection}{\numberline {5.5.2}A numerical example of finding investment portfolios for multiple lenders independently and simultaneously}{103}{subsection.5.5.2}%
\contentsline {section}{\numberline {5.6}Experimental Results}{105}{section.5.6}%
\contentsline {subsection}{\numberline {5.6.1}Predictive models comparison}{105}{subsection.5.6.1}%
\contentsline {subsection}{\numberline {5.6.2}Efficacy of the proposed technique}{106}{subsection.5.6.2}%
\contentsline {section}{\numberline {5.7}Discussion}{120}{section.5.7}%
\contentsline {chapter}{\numberline {6}Recommendation system for lower interest borrowing}{123}{chapter.6}%
\contentsline {section}{\numberline {6.1}Introduction}{123}{section.6.1}%
\contentsline {section}{\numberline {6.2}Problem statement, objectives and the overall proposed methodology}{125}{section.6.2}%
\contentsline {subsection}{\numberline {6.2.1}The problem statement and objectives}{125}{subsection.6.2.1}%
\contentsline {subsection}{\numberline {6.2.2}The overall proposed methodology}{126}{subsection.6.2.2}%
\contentsline {section}{\numberline {6.3}Data analysis}{128}{section.6.3}%
\contentsline {section}{\numberline {6.4}Methodology}{130}{section.6.4}%
\contentsline {subsection}{\numberline {6.4.1}Feature encoding and sentiment analysis}{130}{subsection.6.4.1}%
\contentsline {subsection}{\numberline {6.4.2}Machine learning models for interest rate prediction and likelihood of getting funded}{133}{subsection.6.4.2}%
\contentsline {subsection}{\numberline {6.4.3}Decision process to recommend the type of loan application}{134}{subsection.6.4.3}%
\contentsline {section}{\numberline {6.5}Experimental results}{135}{section.6.5}%
\contentsline {subsection}{\numberline {6.5.1}Experimental setup}{136}{subsection.6.5.1}%
\contentsline {subsection}{\numberline {6.5.2}Interest rate payable prediction for traditional and bidding loans}{137}{subsection.6.5.2}%
\contentsline {subsection}{\numberline {6.5.3}Predicting the success rate of funding of bidding loans}{140}{subsection.6.5.3}%
\contentsline {subsection}{\numberline {6.5.4}Impact of sentiment score}{143}{subsection.6.5.4}%
\contentsline {subsection}{\numberline {6.5.5}Efficacy of the overall recommendation engine}{145}{subsection.6.5.5}%
\contentsline {section}{\numberline {6.6}Discussion}{146}{section.6.6}%
\contentsline {chapter}{\numberline {7}Conclusions and future work}{149}{chapter.7}%
\contentsline {section}{\numberline {7.1}Integration and validity}{149}{section.7.1}%
\contentsline {section}{\numberline {7.2}Conclusions}{152}{section.7.2}%
\contentsline {section}{\numberline {7.3}Research direction}{154}{section.7.3}%
\contentsline {chapter}{References}{157}{chapter*.133}%
\contentsline {chapter}{Appendix \numberline {A}}{165}{appendix.a.A}%
\contentsline {chapter}{Appendix \numberline {B}Description and data analysis of three labels}{167}{appendix.a.B}%
