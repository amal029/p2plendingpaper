\section{Classification methods}
\label{sec:class-meth}

In this paper we propose a new technique for classifying loans into
three labels: default, full payment, and prepayment, called \acf{H3LC}.
The proposed technique can be considered to be a meta-classification
technique, which itself uses \acf{LOGIT}~\citep{friedman2001elements}
and \acf{RF}~\citep{breiman2001random} for classification. Recent work
in Malekipirbazari and Aksakalli~\cite{malekipirbazari2015risk} compares
different classification techniques for classifying loans on \ac{P2PL}
platforms into two labels: default and non-default and suggests that
\ac{RF} is the best approach. Work in Guo et al.~\cite{Guo2016417} on
the other hand proposes to use \ac{LOGIT} for two label classification
of loans and finally, work in Li et al.~\cite{li2016prepayment} proposes
the use of \acf{MLOGIT}~\citep{friedman2001elements} for three-label
classification. In this section, for the sake of completeness, we give a
description of different classification techniques followed by a
thorough experimental comparison of these techniques with the proposed
\ac{H3LC} technique in Section~\ref{sec:exper-eval}.


\subsection{\acf{LOGIT}}
\label{sec:acflogit}

\acf{LOGIT}~\cite{wiginton1980note} is one of the most popular
classification method used in \ac{P2PL} analysis. Let $\hat{f}$ be a
linear function of the eight predictor variables (c.f.
Section~\ref{sec:feature-selection}) represented by vector $\mathbf{X}$.
The linear function $\hat{f}(\mathbf{X})$ is then represented below.

\[
  \hat{f}(\mathbf{X}) = \beta_{0} + \sum\limits_{i = 1}^{N}\beta_{i}
  \times x_{i}
\]

\noindent
where $\beta_{0}$ is the intercept and $x_{i}$ for $i = 1 \ldots n$
represents the predictor variables in $\mathbf{X}$. Then, the logistic
regression function can be written as:

\[
  logit(\hat{p}) = \ln (\frac{\hat{p}}{1 - \hat{p}}) =
  \hat{f}(\mathbf{X}) = \beta_{0} + \sum\limits_{i = 1}^{n}\beta_{i}
  \times x_{i}
\]

\noindent
where $\hat{p}$ is the probability of a loan defaulting. The $logit$
function predicts the \textit{odds} of a loan defaulting using a linear
combination of predictors.

\subsection{\acf{MLOGIT}}
\label{sec:acfmlogit}

\acf{MLOGIT}~\cite{friedman2001elements} is very similar to \ac{LOGIT},
but it is used in multi-label classification problems, i.e., in cases
when the response variable is categorical but not binary. \ac{MLOGIT} is
useful in classifying loan status as: default (0), full payment at
maturity (1) and prepayment (2). We define three linear combinations of
the predictor variables as shown below:

\begin{align*}
  \hat{f}_{0}(\mathbf{X}) = \beta_{0, 0} + \sum\limits_{i = 1}^{n}\beta_{0,i} \times x_{0,i},\\
  \hat{f}_{1}(\mathbf{X}) = \beta_{1, 0} + \sum\limits_{i = 1}^{n}\beta_{1,i} \times x_{1,i},\\
  \hat{f}_{2}(\mathbf{X}) = \beta_{2,0} + \sum\limits_{i = 1}^{n}\beta_{2,i} \times x_{2,i},
\end{align*}

Under the \ac{MLOGIT} regression model, the probability that a loan
belongs to default, full payment at maturity and prepayment can be
written as:

\begin{align*}
  \hat{p}_{dft} = \frac{e^{\hat{f}_{0}}}{e^{\hat{f}_{0}}+e^{\hat{f}_{1}}+e^{\hat{f}_{2}}},\\
   \hat{p}_{full} = \frac{e^{\hat{f}_{1}}}{e^{\hat{f}_{0}}+e^{\hat{f}_{1}}+e^{\hat{f}_{2}}},\\
   \hat{p}_{pre} = \frac{e^{\hat{f}_{2}}}{e^{\hat{f}_{0}}+e^{\hat{f}_{1}}+e^{\hat{f}_{2}}},
\end{align*}

\noindent
where $\hat{p}_{dft}$ is the probability of default, $\hat{p}_{pre}$
is the probability of prepayment, and $\hat{p}_{full}$ is the
probability of full payment.

In addition, these three probabilities satisfy the following criteria:
\begin{align*}
\hat{p}_{dft}+\hat{p}_{full}+\hat{p}_{pre} = 1,\\
0\leq \hat{p}_{dft}\leq 1,\\
0\leq \hat{p}_{full}\leq 1,\\
0\leq \hat{p}_{pre}\leq 1.  
\end{align*}

 This means that the sum of these three probabilities must
  equal to one, and each probability cannot be lower than zero or higher
  than one.

\subsection{\acf{RF}}
\label{sec:random-forest}

\acf{RF} is one of the most popular techniques for classification when
there is an non-linear relationship between predictors and response
variables. In fact, work in  Malekipirbazari and Aksakalli~\cite{malekipirbazari2015risk} proposes that
\ac{RF} is the best technique for two label classification of loans. We
can (and will) use \ac{RF} for three label classification in the
proposed \ac{H3LC} technique. \acp{RF} have several advantages: they are
easy to tune, they are easy to interpret; they can handle nominal data,
and they run relatively efficiently for large datasets. In this paper we
cannot cover all aspects of \acp{RF} and hence, we only give a brief
description of the main ideas. The reader is referred
to Breiman~\cite{breiman2001random} for a more in-depth discussion on \acp{RF}.

Given a training vector $\mathbf{X} = x_{1}, \ldots, x_{n}$ with a
categorical response vector $\mathbf{Y} = y_{1}, \ldots y_{n}$,
repeatedly choose random samples to replace the training set and then
fit decision trees~\citep{friedman2001elements} to those $N$ samples with
the following steps:

\begin{enumerate}
	\item For each sample, $k = 1,\ldots,N$, let the new training data
	and response variable be $\mathbf{X}_{k}$ and $\mathbf{Y}_{k}$,
	respectively.
	\item Train a decision tree $\hat{f}_{k}$ on each sample $k$.
	\item After training $N$ decision trees, the response for a new sample
	(or new loan $x'$ in our case) can then be predicted by averaging the
	predictions from each decision tree we trained, using the formula
	below:
	
	\[
	\hat{f} = \frac{1}{N}\sum\limits_{k=1}^{N}\hat{f}_{k}(x')
	\]
	
\end{enumerate}

\acp{RF} are resistant to over-fitting with growing number of decision
trees and hence, we can build a large number of trees to reduce bias. In
our case we set $k=100$ to build 100 decision trees. Furthermore, each
decision tree is expanded to its full depth, i.e., until the leaves are
\textit{pure}. Impurity of a node is measured using the \textit{gini}
impurity index~\citep{breiman2001random}.



\subsection{Proposed \acf{H3LC}}
\label{sec:acfh3lc}

\acf{H3LC} is a meta-classification technique that we propose in this
paper. We want to classify loans into three classes: default, fully
payed, and prepayed. Classification methods such as \ac{MLOGIT} (c.f.
Section~\ref{sec:acfmlogit}) and \ac{RF} (c.f.
Section~\ref{sec:random-forest}), can be used for three label
classification. All these methods consider the response variable as
flat, i.e., prediction models are built using all three labels as
possible responses at once, which can lead to increased
miss-classification rates. Especially if the frequency distributions of
responses vis-\'a-vis predictors are similar. For example, consider the
debt-to-income-ratio predictor used to compute the probability of loan
default. The likelihood of prepayment, full payment, and default
considering \textit{just} debt-to-income-ratio maybe 59\%, 27\% and
14\%, respectively. However, we know that full payment and prepayment
both account for non-defaulting loans. Hence, if we were to consider
prepayment and full payment as a single label: non-default, then two
label classification would result in likelihood of non-default and
default of 86\% and 14\%, respectively. Same can be said about other
predictors.

Predictors and the response variable for two-label classification are
much more strongly correlated, compared to three label classification
for the same response variable. The stronger the correlation, the
greater the accuracy of classification. Moreover, misclassifying
defaulting loans as non-default \textit{incorrectly} reduces the risk of
an investment, while misclassifying prepaid as fully paid (or
vice-versa) only reduces (respectively increases) the \ac{ARR}, but
there is no chance of the investment defaulting. Given these
observations, we propose a hierarchical classification technique:

\begin{enumerate}
\item \textit{Step-A}: Build a \textit{two-label} classification model
  that classifies the loan status (response variable) as default or
  non-default.
\item \textit{Step-B}: Given the set of loans, which are classified as
  non-default from Step-A, perform a \textit{two-label} classification,
  considering the loan status as prepay and fully paid.
\end{enumerate}

Any two-label classification technique, e.g., \ac{LOGIT} or \ac{RF}, can
be used for Steps A and B, hence we call the proposed \ac{H3LC}
technique a meta-classification technique.




%%% Local Variables:
%%% mode: latex
%%% TeX-master: "manualscript"
%%% End:
