\section{Introduction}
\label{sec:introduction}

\acf{P2PL} is a form of micro financing~\citep{morduch1999microfinance,
  conlin1999249}, which allows securing personal and small business
loans from non-traditional sources. In a \ac{P2PL} system, borrowers are
funded by one or more individual lenders rather than institutions.
Usually, the borrowers securing loans via \ac{P2PL} systems have low
credit ratings, or very little assets~\citep{funk2015online}. Moreover,
the loans are unsecured, i.e., defaulting on a loan, by a borrower, will
lead to a complete wipe out of the lenders' investment. Yet, \ac{P2PL}
is a growing industry, because lenders can achieve higher return on
their investments via higher interest rates and borrowers, who would not
otherwise be funded by banks, are able to achieve
financing~\citep{Roth20121115}.

Online \ac{P2PL} platforms connect lenders to borrowers. The very first
\ac{P2PL} platform was established in 2005 in the United Kingdom
(ZOPA\footnote{www.zopa.com}) and such platforms have mushroomed in the
past 10 years. Currently there are many \ac{P2PL} online platforms in a
wide range of countries. For instance, Prosper\footnote{www.prosper.com}
and Lending Club\footnote{www.lendingclub.com} in the United States,
Harmoney\footnote{www.harmoney.co.nz} in New Zealand, and Rate
Setter\footnote{www.ratesetter.com} in Australia. The exponential growth
in the \ac{P2PL} industry can be judged by the fact that one of the
smaller \ac{P2PL} platforms, Lending Club, had loan transactions worth
7.6 Billion USD in January 2014, which had grown to 20 Billion USD by
June 2016~\citep{LCW}.

The major drawback of the \ac{P2PL} system is the chance of borrower
default thereby wiping out all lender investment. Hence, accurately
predicting the possibility of a given loan defaulting is essential. In
order to allow investors to make an informed decision, lending platforms
provide a number of borrower characteristics, including a so called
\textit{risk rating}. This risk rating is usually categorized from A to
F, with A indicating the lowest chance of default, and F indicating the
highest. These risk ratings are calculated based on borrower
characteristics such as: borrower creditworthiness, debt to income
ratio, past delinquencies, etc. The interest rates vary proportionally
with the risk ratings. The lenders are expected to invest prudently
according to their risk aptitude in order to maximize their returns.
However, these risk ratings are insufficient in most cases, because:
\textcircled{1} The risk rating does not give any indication as to how
much a lender should be investing in a given loan. \textcircled{2} Many
loans with the same risk ratings vary in other characteristics, which
should be carefully investigated. \textcircled{3} Individual risk
ratings do not give an \textit{overall} perspective, i.e., a frontier
that simultaneously compares risk with return of multiple loans that
would help in making prudent investment decisions.

There is a growing interest in providing tools that allow lenders to
build loan portfolios for maximizing their investments. Most recent work
of Guo et al.~\cite{Guo2016417} provides an instance based (also
referred to as supervised) machine learning technique that allows
predicting the return rate and probability of default for each loan.
However, this work considers that all non-defaulting loans will
\textit{only} mature at the end of the loan term, which varies between
12 to 60 months. The data~\citep{li2016prepayment} on the other hand
indicates that many borrowers \textit{prepay} their loans long
\textit{before} the date of maturity, in order to reduce the total
interest payable since there are no penalties associated with early
repayment. Hence, the risk vs. return portfolio should be based on the
\acf{ARR} -- the rate of return scaled to a one year period. Moreover,
classifying loans as just defaults or non-defaults leads to
\textit{overestimating} the \ac{ARR}, if most borrowers prepay and
\textit{underestimating}, if most borrowers pay at the date of maturity.
In fact, our experimental results, on real datasets, show that the
current state-of-the-art techniques grossly overestimate the \ac{ARR}.
Gross overestimates and underestimates can lead to wrong investment
decisions and hence, we need new techniques, which result in the most
accurate \ac{ARR} and associated risk predictions.

Finally, the \ac{P2PL} dataset used for loan classification is
unbalanced. Since there are few defaulters in general, compared to full
payments and prepayments. The current state-of-the-art loan
classification techniques miss-classify many defaulting loans as either
full payments or prepayments~\cite{malekipirbazari2015risk}. Overcoming
the unbalanced dataset problem, although straightforward, is essential
in correctly classifying loans. However, it remains unclear if current
state-of-the-art techniques address this problem.

\begin{figure}[t]
  \centering
  \includegraphics[scale=0.5]{overall}
  % \scalebox{0.6}{\input{./figures/overall.latex}}
  \caption{Overall proposed methodology}
  \label{fig:overall}
\end{figure}

The \textbf{main contribution} of this paper is building a risk vs.
return portfolio~\citep{markowitz1991foundations, boyd2004convex} in
order to invest in the best performing loans (those with the highest and
most accurate \ac{ARR}) with the minimum risk (those that are least
likely to default). The proposed methodology is shown in
Figure~\ref{fig:overall}. We start with a set of historical loan data.
In step-1 we balance the unbalanced dataset to include approximately
equal number of default, full payment, and prepayment loans. This helps
us more accurately classify the defaulting loans. Next, in step-2, we
select the most prominent features that help classify the loans as:
possible default, possible full payment, or possible prepayment. Our
\textit{first key technical contribution} is a hierarchical
classification technique (shown in step-3), which first classifies the
loans as possible default or non-default and then further classifies the
non-defaulting loans as possible full payments and prepayments. The
resultant three dimensional vector, where each constituent gives the
probability of default, full payment, and prepayment, respectively, is
then used to compute the likely return (reward) and variance (risk) in
the next step. Our \textit{second key technical contribution}, in
step-4, is the introduction of an exponential weighted average function
to estimate the \ac{ARR} and the associated risk of investing in a loan
-- its variance. The exponential weighted technique has two key
advantages: (1) the algorithmic complexity of constructing the
prediction model is $O(1)$, compared to the $O(N^{2})$ complexity of the
current state-of-the-art kernel regression
techniques~\citep{Guo2016417}, where $N$ is the number of training
samples. Hence, the proposed technique scales to large datasets (c.f.
Section~\ref{sec:instance-based-model}.) (2) The proposed technique more
accurately predicts the \ac{ARR} and variance (c.f.
Section~\ref{sec:comp-accur-pred}) compared to the current
state-of-the-art. Finally, we build a return-risk (Markowitz)
frontier~\citep{markowitz1991foundations} for loan selection. We show
that the proposed methodology always results in the \textit{most}
accurate \ac{ARR} for the same loan portfolio compared to any of the
current techniques in research literature.


% The proposed methodology considers \ac{ARR} and is based on classifying
% loans into three labels: default, full payment, and prepayment.

We start with qualitatively comparing the proposed methodology with the
current technique sin Section~\ref{sec:related-works}.
Section~\ref{sec:data-description} then describes different features of
historical loan data from popular lending platforms along with the
relevant feature selection technique. Section~\ref{sec:class-meth}
describes the various classification techniques used and compared in
this work. Section~\ref{sec:instance-based-model} presents the instance
based weighted return and variance computations.
Section~\ref{sec:portf-optim-model} describes the mean-variance
portfolio generation. A thorough experimental evaluation and
quantitative comparison with current techniques is presented in
Section~\ref{sec:exper-eval}. Finally, we finish by discussing the
merits and limitations of the proposed technique in
Section~\ref{sec:conclusion}.


%%% Local Variables:
%%% mode: latex
%%% TeX-master: "manualscript"
%%% End:
