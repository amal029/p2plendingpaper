\section{Experimental evaluation and discussion}
\label{sec:exper-eval}
In this section we perform a thorough comparison of the various
techniques described from Sections~\ref{sec:class-meth}
to~\ref{sec:portf-optim-model}. We divide this section into five
subsections, following the steps presented in Figure~\ref{fig:overall},
as follows:

\begin{itemize}
\item We describe the experimental setup in
  Section~\ref{sec:experimental-setup}.
\item Section~\ref{sec:bal} describes the effects of using unbalanced
dataset and how we overcome this problem.
\item Section~\ref{sec:comp-accur-class} then compares the accuracy of
  various classification techniques presented in
  Section~\ref{sec:class-meth} and highlights how recursive feature
  selection improves the accuracy of prediction.
\item Section~\ref{sec:comp-accur-pred} compares the accuracy of
  predicting the \ac{ARR} and standard deviation (risk) between the
  \underline{best} classification techniques from
  Section~\ref{sec:comp-accur-class} with the most significant selected
  features.
\item Finally, in Section~\ref{sec:mean-vari-front}, we present the
  Markowitz frontier generation and Sharpe ratio computation for the
  \underline{best} technique from Section~\ref{sec:comp-accur-pred}, and
  compare the portfolio \ac{ARR} and standard deviation (risk) between
  them.
\end{itemize}

\subsection{Experimental setup}
\label{sec:experimental-setup}

All our experiments are performed on the Prosper dataset (c.f.
Section~\ref{sec:data-description}). First we randomly sample equal
numbers of defaulting, prepayment, and full payment loans from the
historical dataset to create a balanced dataset. Next, we work on a
85:15 ratio of training and testing sample sets from the balanced
dataset. We use the predictor variables (features) selected using
recursive selection technique for training our classification models. In
order to avoid over-fitting and under-fitting of the classification
models, we apply 5-fold Monte Carlo cross-validation (CV) on the
dataset. Specifically, out of the balanced dataset we randomly sample
five \textit{pairs} of datasets each with a training:testing split of
85:15 ratio for training and testing, respectively. The work in Li et
al.~\cite{li2016prepayment} proposes the \ac{MLOGIT} three-label
classification technique for classifying loans, the work
in~\cite{malekipirbazari2015risk} shows that \ac{RF} is the best
classification technique for \ac{P2PL} loans. Hence, for experiments we
always compare the proposed \ac{H3LC} variants with \ac{RF} and
\ac{MLOGIT}. Where necessary, we also give the execution time for the
various prediction models. The models were built using the
Scikit-learn~\citep{pedregosa2011scikit} Python library. All experiments
were executed on Intel Core i7-4720HQ CPU, running at 2.6GHZ with 8.00
GB of RAM with Windows 10.


% Since unbalanced data will effect the result dramatically, which is
% described in Section~\ref{sec:bal}. Therefore, we randomly select 3,000
% defaulted loans, 3,000 prepaid loans and 3,000 fully paid loans together
% as the sample dataset. From the dataset and divide these into a ratio of
% 85:5, indicating the 8500 loans, which will be used to train various
% prediction models and the remaining 500 loans used for testing. 

\subsection{Balancing the unbalanced dataset}
\label{sec:bal}

In this section, we use three-label loan classification to show the
negative effects of unbalanced dataset and the need for balancing this
dataset before using any classification technique.

Recall that we have 33,951 historical loans in the dataset
(Section~\ref{sec:data-description}). Amongst these, there are 3,075
defaulting loans, 7,476 fully paid loans, and the rest are pre-payed
loans, resulting in an unbalanced dataset with a ratio of
\mbox{default:full payment:prepayment} $\approx 1:2.4:7.6$. We can split this
dataset for training and testing in a ratio of 85:15 and apply the
\ac{MLOGIT}, \ac{RF}, and the proposed \ac{H3LC} techniques for
classification.

The accuracy of classification, with the features selected using
recursive selection technique, for each of the classification algorithm,
is shown in Table~\ref{tab:unbal}. It can be observed from
Table~\ref{tab:unbal} that all the four techniques have extremely low
accuracy when predicting defaulting loans. Notice, that the overall
accuracy of the proposed \ac{H3LC} classification technique, with
\ac{RF}, being used in both its steps, is very high at 92\%. However,
this overall accuracy score is misleading, because of the very large
number of misclassified defaulting loans. The low accuracy of predicting
defaulting loans can mislead lenders towards making dangerously
incorrect investment decisions. Same can be said about other
classification algorithms.

\begin{table}[ht]
  \caption{Results of accuracy for three-label loan status classification
    (in \%) with unbalanced dataset. FP: Full payment, PP: Prepayment, D:
    Default, O: Overall accuracy.}
  \label{tab:unbal}
  \centering
  % \begin{scriptsize}
    \begin{tabular}{|c|c|c|c|c|}
      \hline
      \diagbox{Technique}{Loan status} & FP & PP & D & O\\
      \hline
      \hline
      \ac{MLOGIT} & 0 & 99.3 & 0 & 67.5\\ 
      \hline
      \ac{RF} & 4.45 & 94.66 & 9.6 & 67.6\\
      \hline
      \ac{H3LC}\_\ac{RF} & 99 & 98.5 & 2.8 & 92 \\
      \hline
      \ac{H3LC}\_\ac{LOGIT} & 0 & 99.5 & 4.8 & 71.6\\
      \hline
    \end{tabular}
  % \end{scriptsize}
\end{table}

To overcome the problem of unbalanced dataset, we select 3,000 defaulted
loans, 3,000 fully paid loans and 3,000 pre-paid loans together as our
new \textit{balanced} dataset. We again simply spilt this new dataset in
a ratio of 85\% to 15\% for training and testing the four classification
algorithms with features selected using recursive selection. The results
are shown in Table~\ref{tab:bal}. From Table~\ref{tab:bal}, we can
observe that the accuracy of predicting defaulting loans is increased
dramatically for all the four classification algorithms. In addition,
the accuracy of predicting fully paid loans is also increased a lot for
\ac{MLOGIT}, \ac{RF} and H3LC\_LOGIT\@.

Although, overall accuracy has slightly decreased, the results obtained
by using a balanced dataset are still more reliable than that using
unbalanced dataset. As a result, in all future experiments, of this
paper, we will use the balanced dataset for training and testing.

\begin{table}[ht]
  \caption{Results of accuracy for three-label loan status classification
    (in \%) with balanced dataset. FP: Full payment, PP: Prepayment, D:
    Default, O: Overall accuracy.}
  \label{tab:bal}
  \centering
  % \begin{scriptsize}
    \begin{tabular}{|c|c|c|c|c|}
      \hline
      \diagbox{Technique}{Loan status} & FP & PP & D & O\\
      \hline
      \hline
      \ac{MLOGIT} & 75 & 45 & 66 & 62\\ 
      \hline
      \ac{RF} & 82 & 50 & 72 & 67\\
      \hline
      \ac{H3LC}\_\ac{RF} & 86 & 75 & 82 & 80 \\
      \hline
      \ac{H3LC}\_\ac{LOGIT} & 74 & 50 & 79 & 68 \\
      \hline
    \end{tabular}
  % \end{scriptsize}
\end{table}

\subsection{Feature selection and comparison of accuracy of
  classification}
\label{sec:comp-accur-class}

This section is split into two parts.
Section~\ref{sec:feature-selection-1} first presents the results for
recursive feature selection algorithm and describes the observed
improvements. Next in Section~\ref{sec:comparison} we compare the
different classification algorithms using the features selected by the
recursive feature selection algorithm to discover the most accurate
algorithm. All experiments were carried out on a balanced dataset.

\subsubsection{Feature selection}
\label{sec:feature-selection-1}

In this section we show the improvement in predicting the loan status
response variable after using the recursive feature selection technique
vs. no feature selection. The list of selected features for each of the
classification algorithms, and the comparison with forward and backward
feature selection techniques is provided in~\cite{Appendix}.

% In this section, we compare the overall accuracy of selecting all the 38
% features and those selected by recursive feature selection for the
% \ac{MLOGIT}, \ac{RF}, and the proposed \ac{H3LC} techniques.

\begin{table}[ht]
  \caption{The overall accuracy of three-label classification (in \%) on
    test dataset with and without feature selection.}
  \label{tab:afrf}
  \centering
  % \begin{scriptsize}
  \begin{tabular}{|c|c|c|}
    \hline
    \diagbox{Technique}{Feature selected} & All features & Recursive selection\\
    \hline
    \hline
    \ac{MLOGIT} & 53 & 62\\ 
    \hline
    \ac{RF} & 57 & 67 \\
    \hline
    \ac{H3LC}\_\ac{RF} & 69 & 80 \\
    \hline
    \ac{H3LC}\_\ac{LOGIT} & 59 & 68\\
    \hline
  \end{tabular}
  % \end{scriptsize}
\end{table}

To compare, we use the balanced dataset as the sample dataset, and split
the sample dataset in a ratio of 85\% to 15\% for training and testing
the four classification algorithms with all features and features
selected using recursive selection. The results are shown in
Table~\ref{tab:afrf}. From Table~\ref{tab:afrf}, we can observe that the
overall accuracy is increased by \textasciitilde10\% for all the four
classification algorithms when using features selected by applying
recursive selection.

\subsubsection{Comparison of classification algorithms}
\label{sec:comparison}

In this section we compare the accuracy of three-label classification
for the \ac{MLOGIT}, \ac{RF}, and the proposed \ac{H3LC} techniques.


\begin{table}[ht!]
  \caption{Five Monte Carlo CV results for three-label loan status classification
    (in \%) on test datasets. FP: Full payment, PP: Prepayment, D:
    Default.}
  \label{fig:acccomp}
  \centering
  \subfigure[Prosper test dataset classification accuracy results\label{fig:acccompp}]
  {
    % \begin{scriptsize}
      \begin{tabular}{|c|c|c|c|c|c|c|}
        \hline
        \diagbox{Technique}{CV sets} & 1 & 2 & 3 & 4 & 5 & Average \\
        \hline
        \hline
        \ac{MLOGIT} & 58 & 60 & 61 & 59 & 63 & 60.2\\
        \hline
        \ac{RF} & 63 & 65 & 67 & 65 & 66 & 65.2\\
        \hline
        \ac{H3LC}\_\ac{RF} & 76 & 78 & 80 & 79 & 75 & \textbf{77.6}\\
        \hline
        \ac{H3LC}\_\ac{LOGIT} & 67 & 65 & 64 & 68 & 70 & 66.8\\
        \hline
      \end{tabular}
    % \end{scriptsize}
  }
  
  \subfigure[The average accuracy scores for each of the three
  labels for Prosper dataset\label{fig:acclabp}]
  {
    % \begin{scriptsize}
      \begin{tabular}{|c|c|c|c|}
        \hline
        \diagbox{Technique}{Loan status} & FP & PP & D\\
        \hline
        \hline
        \ac{MLOGIT} & 78 & 41 & 64 \\ 
        \hline
        \ac{RF} & 80 & 47 & 70 \\
        \hline
        \ac{H3LC}\_\ac{RF} & 88 & 70 & 79 \\
        \hline
        \ac{H3LC}\_\ac{LOGIT} & 70 & 52 & 78 \\
        \hline
      \end{tabular}
    % \end{scriptsize}
  } 
\end{table}

\begin{table}[ht!]
  \caption{Five Monte Carlo CV results for two-label loan status classification
    (in \%) on test datasets.}
  \label{fig:acccomp2}
  \centering
  \subfigure[Prosper test dataset classification accuracy results\label{fig:acccompp2}]
  {
    % \begin{scriptsize}
      \begin{tabular}{|c|c|c|c|c|c|c|}
        \hline
        \diagbox{Technique}{CV sets} & 1 & 2 & 3 & 4 & 5 & Average \\
        \hline
        \hline
        \ac{LOGIT} & 78 & 79 & 73 & 75 & 77 & 76.4\\
        \hline
        \ac{RF} & 80 & 81 & 78 & 79 & 80 & 79.6\\
        \hline
      \end{tabular}
    % \end{scriptsize}
  }
  
  \subfigure[Average accuracy scores (in \%) for two-label classification of
  loan status for the test Prosper dataset. \label{fig:twolabelaccdndp}]
  {
    % \begin{scriptsize}
      \begin{tabular}{|c|c|c|}
        \hline
        \diagbox{Technique}{Loan status} & Default & Non default \\
        \hline
        \hline
        \ac{LOGIT} & 78 & 76 \\
        \hline
        \ac{RF} & 79 & 81 \\
        \hline
      \end{tabular}
    % \end{scriptsize}
  } 
\end{table}


The results for the 5-fold Monte Carlo CV on the test dataset from
Prosper is shown in Table~\ref{fig:acccomp}. From
Table~\ref{fig:acccompp}, it is clear that the proposed \ac{H3LC}
meta-classification technique outperforms all the other techniques based
on three-label classification. In fact, \ac{H3LC} with \ac{RF}
classification (termed \ac{H3LC}\_\ac{RF}) being used for step-A and
step-B (c.f. Section~\ref{sec:acfh3lc}) significantly outperforms all
other techniques.

The reasons for these results becomes clear from
Table~\ref{fig:acclabp}, which show the \textit{average} accuracy scores
for each of the three possible classes for the loan status response
variable. Correctly classifying any given loan as any of the three
labels is highest of \ac{H3LC}\_\ac{RF}. We would also like to point out
that the execution time for \ac{H3LC}\_\ac{RF} and \ac{RF} techniques is
$25 \times$ larger (approximately 10 seconds) than the execution time for
\ac{H3LC}\_\ac{LOGIT} and \ac{MLOGIT} techniques (approximately 0.4
seconds).

Finally, we present the two-label classification scores, in
Table~\ref{fig:acccomp2}, for the 5 test datasets. From
Table~\ref{fig:acccompp2}, it can be observed that the \ac{RF} technique
has the highest accuracy based on 2-label classification followed by the
\ac{LOGIT} technique. The reasons for these results can be revealed from
Table~\ref{fig:twolabelaccdndp}. Guo et al.~\cite{Guo2016417} propose to
use \ac{LOGIT} based two label classification for predicting the
\ac{ARR} and standard deviation in step-2 (Figure~\ref{fig:overall}) of
the overall methodology. The results from
Table~\ref{fig:twolabelaccdndp} will be used for comparison in
Section~\ref{sec:comp-accur-pred}.

\subsection{Comparison of accuracy of predicting \ac{ARR} and standard
  deviation}
\label{sec:comp-accur-pred}

In this section we compare the accuracy of predicting the \ac{ARR} and
the standard deviation (risk) of the loans being considered for
investment using the \ac{RBF} technique prescribed by Guo et
al.~\cite{Guo2016417} and presented in
Section~\ref{sec:acfrbf-comp-simil} and the proposed \ac{EDM} technique
presented in Section~\ref{sec:acfedm-comp-simil}.

The probability of default required for the \ac{RBF} technique is
computed using \ac{LOGIT} and \ac{RF} as shown in
Table~\ref{fig:twolabelaccdndp} and the three-vector indicating the
probability of full payment, prepayment, and default, required for
\ac{EDM}, is computed using the \ac{H3LC}\_\ac{LOGIT} and
\ac{H3LC}\_\ac{RF} as shown in Table~\ref{fig:acccomp}. As before, we
give the results for the 5 Monte Carlo CV on the test datasets.


\begin{table}[h!]
  \caption{Root mean square error between the predicted \ac{ARR} and the real
    \ac{ARR} with five Monte Carlo CV.}
  \label{fig:arrer}
  \centering
  % \resizebox{\textwidth}{!}{
    % \begin{scriptsize}
      \begin{tabular}{|l|l|l|l|l|l|l|}
        \hline
        \diagbox{Technique}{CV sets}                       & 1     & 2     & 3     & 4     & 5     & Average \\ \hline
        LOGIT\_RBF                 & 0.26 & 0.24 & 0.20 & 0.26 & 0.23 & 0.238  \\ \hline
        RF\_RBF                    & 0.26 & 0.23 & 0.26 & 0.27 & 0.21 & 0.246  \\ \hline
        H3LC\_LOGIT\_EDM & 0.25 & 0.20 & 0.18 & 0.26 & 0.23 & 0.224  \\ \hline
        H3LC\_RF\_EDM    & 0.25 & 0.20 & 0.17 & 0.26 & 0.23 & \textbf{0.222}  \\ \hline
      \end{tabular}
    % \end{scriptsize}
  % }

\end{table}

\begin{table}[tb]
  \caption{Root mean square error between the predicted standard deviation and the real standard deviation with five Monte Carlo CV.}
  \label{fig:stddeve}
  \centering
  % \resizebox{\textwidth}{!}{
    % \begin{scriptsize}
      \begin{tabular}{|l|l|l|l|l|l|l|}
        \hline
        \diagbox{Technique}{CV sets}                          & 1     & 2     & 3     & 4     & 5     & Average \\ \hline
        LOGIT\_RBF                 & 0.19 & 0.18 & 0.13 & 0.19 & 0.15 & 0.168  \\ \hline
        RF\_RBF                    & 0.22 & 0.19 & 0.20 & 0.22 & 0.16 & 0.198  \\ \hline
        H3LC\_LOGIT\_EDM & 0.19 & 0.14 & 0.12 & 0.19 & 0.17 & \textbf{0.162}  \\ \hline
        H3LC\_RF\_EDM    & 0.18 & 0.15 & 0.12 & 0.20 & 0.17 & 0.164  \\ \hline
      \end{tabular}
    % \end{scriptsize}
  % }
  

\end{table}

Table~\ref{fig:arrer} shows the root mean square error between the
\textit{predicted} \ac{ARR} (c.f. Equation~(\ref{eq:2})) and the real
\ac{ARR}. We can observe that the two-label classification techniques
along with \ac{RBF} (as proposed by Guo et al.~\cite{Guo2016417}) result
in a higher root mean square error compared to three-label
classification (\ac{H3LC} technique) with the proposed \ac{EDM}
technique for the same underlying classification method. In particular,
the lowest root mean square error is achieved by using H3LC\_RF\_EDM
with the Euclidean distance.

The comparison between the root mean square error of standard deviation
(risk) of loans for the two label classification with \ac{RBF} as
proposed by Guo et al.~\cite{Guo2016417} and the proposed three label
classification with the \ac{EDM} technique is shown in
Table~\ref{fig:stddeve}. On average, the lowest value of root mean
square error of standard deviation is achieved by using
H3LC\_LOGIT\_EDM. % In fact, with the same underlying classification
% method, our proposed techniques with both Euclidean distance show better
% result than the two-label classification technique with \ac{RBF}. 
Hence, we can state that our proposed technique outperforms the current
state-of-the-art technique (as proposed by Guo et al.
~\cite{Guo2016417}) when predicting the \ac{ARR} and standard deviation
of loans.

\begin{figure}[t]
  \centering
  \includegraphics[width=3.1in]{exec_t}
  \caption{Execution time comparison of \ac{RBF} and \ac{EDM}
    techniques~\label{fig:exect}}
\end{figure}

Finally, the \ac{RBF} technique is orders of magnitude slower compared
to the proposed \ac{EDM} technique, because one needs to perform leave
one out cross validation in order to tune the bandwidth (c.f.
Equation~(\ref{eq:4})), while \ac{EDM} does not require any tuning at
all. Figure~\ref{fig:exect} shows the execution time comparison between
the \ac{EDM} and \ac{RBF} techniques with growing size of training
dataset. It can be observed that, when the number of training samples
increases, the execution time of \ac{RBF} grows quadratically. However,
the execution time of the proposed \ac{EDM} technique remains constant.
It can also be observed from Figure~\ref{fig:exect} that the algorithmic
complexity for \ac{EDM} is $O(1)$, and for \ac{RBF} is $O(N^{2})$.


\subsection{Mean-variance frontier generation and portfolio selection}
\label{sec:mean-vari-front}

\begin{table}[h!]
  \caption{Error (difference) between the predicted portfolio \ac{ARR}
    and the real portfolio \ac{ARR} with five Monte Carlo CV }
  \label{fig:port_ARR}
  \centering
  % \resizebox{\textwidth}{!}{
    % \begin{scriptsize}
      \begin{tabular}{|l|l|l|l|l|l|l|}
        \hline
        \diagbox{Technique}{CV sets}                        & 1     & 2     & 3     & 4     & 5     & Average \\ \hline
        LOGIT\_RBF                 & -0.037 & -0.042 & -0.043 & -0.046 & -0.010 & -0.0356  \\ \hline
        RF\_RBF                    & 0.066 & 0.084 & 0.080 & 0.048 & 0.054 & 0.0664  \\ \hline
        H3LC\_LOGIT\_EDM & -0.016 & -0.023 & -0.018 & -0.022 & -0.007 & \textbf{-0.0172}  \\ \hline
        H3LC\_RF\_EDM   & 0.032 & 0.039 & 0.030 & 0.033 & 0.014 & 0.0296  \\ \hline
      \end{tabular}
    % \end{scriptsize}
  % }
\end{table}

\begin{table}[h!]
  \caption{Error (difference) between the predicted portfolio standard
    deviation and the real portfolio standard deviation with five Monte
    Carlo CV. }
  \label{fig:port_std}
  \centering
  % \resizebox{\textwidth}{!}{
    % \begin{scriptsize}
      \begin{tabular}{|l|l|l|l|l|l|l|}
        \hline
        \diagbox{Technique}{CV sets}                        & 1     & 2     & 3     & 4     & 5     & Average \\ \hline
        LOGIT\_RBF                 & -0.034 & -0.026 & -0.029 & -0.022 & -0.024 & -0.027   \\ \hline
        RF\_RBF                    & -0.060 & -0.061 & -0.19  & -0.088 & -0.049 & -0.0896  \\ \hline
      	H3LC\_LOGIT\_EDM & -0.031 & -0.023 & -0.018 & -0.034 & -0.027 & \textbf{-0.0266}  \\ \hline
      	H3LC\_RF\_EDM    & -0.031 & -0.030 & -0.016 & -0.032 & -0.025 & -0.0268  \\ \hline
      \end{tabular}
    % \end{scriptsize}
  % }
\end{table}

In this section we apply the mean-variance portfolio optimization (c.f.
Section~\ref{sec:sharpe-ratio}) on the test datasets. We compare the
portfolio \acp{ARR} and standard deviations obtained from the Markowitz
frontiers and the highest Sharpe ratios of the test datasets by using
the \ac{H3LC}\_\ac{RF}\_{EDM},\newline
\ac{H3LC}\_\ac{LOGIT}\_{EDM}, \ac{LOGIT}\_\ac{RBF}, and
\ac{RF}\_\ac{RBF} techniques.

Given the portfolio solution $x \in \mathbb{R}^{500}$, for each of the
five test datasets, with the highest Sharpe ratio, computed by
Equations~(\ref{eq:9}) and~(\ref{eq:10}), the \textit{predicted}
portfolio \ac{ARR} $\hat{R}_{port}$ and standard deviation
$\hat{\sigma}_{port}$ are calculated by Equations~(\ref{eq:11})
and~(\ref{eq:12}), respectively:

\begin{equation}
  \hat{R}_{port} = x^{T}\hat{\mu}, \:\: x,\hat{\mu} \in \mathbb{R}^{500},
  \label{eq:11}
\end{equation}

\begin{equation}
  \hat{\sigma}_{port} = x^{T}\hat{\sigma}, \:\: x,\hat{\sigma} \in \mathbb{R}^{500},
  \label{eq:12}
\end{equation}

\noindent
where $\hat{\mu}$ is the \textit{predicted} \ac{ARR} of the test
dataset, and $\hat{\sigma}$ is the \textit{predicted} standard deviation
(risk).

The \textit{error} between the predicted portfolio returns and real
portfolio returns is calculated by simply taking the difference between
the two. The error between the \textit{predicted} portfolio returns and
the real portfolio returns for test datasets is shown in
Table~\ref{fig:port_ARR}. A negative value indicates that the predicted
portfolio return is underestimated, while a positive value indicates
overestimates. Similar to the results shown in Table~\ref{fig:arrer},
the \textit{predicted} portfolio returns for \ac{H3LC} with \ac{EDM} is
closest to the real portfolio returns as compared to the 2-label
classification with \ac{RBF}. On average, the absolute error by using
2-label classification with \ac{RBF} (\ac{LOGIT}\_\ac{RBF} and
\ac{RF}\_\ac{RBF}) is about \textbf{double} that of using the proposed
\ac{H3LC} with \ac{EDM}. The closest portfolio \ac{ARR} is predicted by
the proposed technique: \ac{H3LC}\_\ac{LOGIT}\_{EDM} with an absolute
error of 0.0172. It can be observed that the absolute errors of
\ac{RF}\_\ac{RBF} is much higher than that of \ac{H3LC}\_\ac{RF}\_{EDM}.
% This is due to the observation that when predicting the \ac{ARR}, for a
% given loan, \ac{RF}\_\ac{RBF} usually predicts a higher \ac{ARR} than
% the one predicted by \ac{H3LC}\_\ac{RF}\_{EDM}.

The \textit{error} between the predicted standard deviation of a
portfolio and the real standard deviation of the same portfolio is
computed by taking their difference. Table~\ref{fig:port_std} displays
the error between the \textit{predicted} portfolio standard deviation
(risk) and the real portfolio standard deviation. The most significant
observation is that all prediction models \textit{underestimate} the
risk associated with investment portfolio. One can observe that the
smallest error with an absolute value of 0.0266 is achieved by using the
proposed technique \ac{H3LC}\_\ac{LOGIT}\_{EDM}. All four techniques
\textit{underestimate} the real portfolio standard deviation, because of
the misclassification of loans. Therefore, the predicted portfolio risk
is always smaller than the real portfolio risk. Especially, the
\ac{RF}\_\ac{RBF} technique underestimates the portfolio standard
deviation wildly (with absolute error of 0.0896)\footnote{Finally, in
  the supplementary material~\cite{Appendix}, we provide figures to show the
  Markowitz efficiency frontiers and the highest Sharpe ratios generated
  by each prediction model.} Overall, we can state that the proposed
technique outperforms the current-state-of-the-art in both predicting
the \ac{ARR} and the standard deviation of the constructed investment
portfolio.

% The highest Sharpe ratio is
% predicted by \ac{RF}\_\ac{RBF}. However, since \ac{RF}\_\ac{RBF}
% overestimates the portfolio returns and underestimates the portfolio
% standard deviation dramatically, this Sharpe ratio is meaningless. After
% the \ac{RF}\_\ac{RBF} technique, \ac{H3LC}\_\ac{RF}\_{EDM}(Mah), again
% this technique overestimates the portfolio returns and underestimates
% the portfolio standard deviation significantly, this Sharpe ratio is
% also meaningless. After these two techniques,
% \ac{H3LC}\_\ac{RF}\_{EDM}($L_{2}$) technique shows the highest Sharpe
% ratio amongst the rest. These results enforce that the proposed
% \ac{H3LC}\_\ac{EDM} technique is more accurate than the current state of
% the art 2-label classification techniques with \ac{RBF}.



%%% Local Variables:
%%% mode: latex
%%% TeX-master: "manualscript"
%%% End:
