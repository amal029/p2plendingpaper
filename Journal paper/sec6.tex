\section{Instance-based model for predicting the \acf{ARR} and standard
  deviation (risk) of investment}
\label{sec:instance-based-model}

Section~\ref{sec:class-meth} predicts the probability of a loan status,
but does not indicate how much should we invest in each loan. This
section and the next presents techniques to maximize returns, while
minimizing the risk of investment. In order to maximize returns, we
first need to predict the \ac{ARR} and the risk associated with
investing in each of the loans available for investment.

We first introduce some notation. Let $N$ and $M$ indicate the number of
loans in the historical (i.e., the training) dataset and the loans
available for investment (i.e., the testing dataset), respectively. Let
$\mu_{i}$ indicate the \ac{ARR} for the $i^{th}$ historical loan, where 
$i \in \{1,2,\dots N\}$. Similarly, $\sigma_{i}$ indicates the standard
deviation (risk) of the $i^{th}$ historical loan. Let $\hat{\mu}_{j}$
indicate the predicted \ac{ARR} for the $j^{th}$ loan, where
$j \in \{1,2,\dots M\}$. Similarly, let $\hat{\sigma}_{j}$ indicate the
predicted standard deviation of the $j^{th}$ loan. Finally, let
$\rho_{f,i}$, $\rho_{p,i}$, $\rho_{d,i}$, $\hat{\rho}_{f,j}$,
$\hat{\rho}_{p,j}$, and $\hat{\rho}_{d,j}$, indicate the probabilities of full
payment, prepayment, and default for the $i^{th}$ and $j^{th}$ loans,
respectively.

\subsection{Predicting the \acf{ARR} and the standard deviation for
loans available for investment}
\label{sec:predicting-acfarr}

The \ac{ARR} for the historical loans can be computed using
Equation~(\ref{eq:1}), where $\mathcal{E}_{i}$ is the total amount that
the borrower paid back on the date of closing (the closing date might be
before the original loan term specified, in case of prepayment), $A_{i}$
is the platform fee, $L_{i}$ stands for the original amount (the
principal) borrowed, and $T_{i}$ is the original date of maturity. The
predicted \ac{ARR} for the $j^{th}$ loan is then computed as shown in
Equation~(\ref{eq:2}), where $s_{j,i} \in \{[0, 1] \cap \mathbb{R}\}$ termed
the \textit{similarity metric} indicates how \textit{similar} is loan
$i$ with respect to $j$. The mathematical formulation of similarity
metric is given in the next section. We can also predict the standard
deviation of the $j^{th}$ loan as shown in Equation~(\ref{eq:3}).

\begin{align} \forall i \in \{1,2,\dots N\},\ \ & \mu_{i} =
[(\mathcal{E}_{i}-A_{i})/L_{i}]^{1/T_{i}}-1 \label{eq:1} \\ \forall j \in
\{1,2,\dots M\},\ \ & \hat{\mu}_{j} =
\sum\limits_{i=1}^{N}s_{j,i}\times\mu_{i} \label{eq:2} \\ \forall j \in \{1,2,\dots M\}, \
\ & \hat{\sigma}_{j}^{2} = \sum\limits_{i = 1}^{N}s_{j,i} \times
(\mu_{i}-\hat{\mu}_{j})^{2} \label{eq:3}
\end{align}


\subsection{Computing the similarity metric}
\label{sec:comp-simil-metric}

Given a $P$ dimensional vector space $\mathbb{R}^{P}$, we define
similarity metric as the function
$s_{j,i} : \mathbb{R}^{P} \times \mathbb{R}^{P} \rightarrow \mathbb{R}$. In our case,
$P = 3$, where the three constituents indicate the probability of full
payment, prepayment, and default, respectively. There are many functions that fit the criteria
for computing the similarity metric. Guo et al.~\cite{Guo2016417}
advocate the use of \ac{RBF}~\citep{alpaydin2014introduction} for
computing the similarity metric. We introduce a more efficient and
effective approach called \acf{EDM} and compare with the \ac{RBF}
approach. For the sake of completeness, we first describe Guo et
al.'s~\cite{Guo2016417} \ac{RBF} technique followed by \ac{EDM}.

\subsubsection{\acf{RBF} for computing the similarity metric}
\label{sec:acfrbf-comp-simil}

Guo et al.'s~\cite{Guo2016417} similarity metric function is given in
Equation~(\ref{eq:4}). The function $K(u)$ is the standard Gaussian as
shown in Equation~(\ref{eq:5}), where variable $h$, called the
bandwidth, is tuned using leave out one cross
validation~\citep{friedman2001elements} technique.

\begin{align} s_{j,i} =
\frac{K(\frac{|\rho_{d,i}-\hat{\rho}_{d,j}|}{h})}{\sum^{N}_{i=1}K(\frac{|\rho_{d,i}-\hat{\rho}_{d,j}|}{h})} \label{eq:4}\\
K(u) = \frac{1}{\sqrt{2\pi}}e^{-\frac{1}{2}u^{2}} \label{eq:5}
\end{align}

Similarity metric is a real number between 0 and 1, inclusive. If the
absolute \textit{distance} ($|\rho_{d,i} - \hat{\rho}_{d,j}|$) between the
probability of default of loans $i$ and $j$ is small, then they are more
similar, resulting in a larger value for $s_{j,i}$ and vice-versa.
Hence, similar loans in the training dataset contribute more when
predicting the \ac{ARR} and standard deviation of loan $j$ and
vice-versa. The bandwidth $h$ can control the contribution of more
distant loans. Tuning bandwidth $h$ using leave one out cross validation
with $N$ training samples requires, $N\times(N-1)$ iterations, which makes
\ac{RBF} algorithmically $O(N^{2})$.

The major difference compared to our model is that the distance between
loans $i$ and $j$ is computed \textit{only} as the difference between
their probabilities of default, i.e, in one dimensional vector space. We
identify each loan as a three dimensional vector ($\mathbb{R}^{3}$).

\subsubsection{\acf{EDM} for computing the similarity metric}
\label{sec:acfedm-comp-simil}

Given a historical loan $i$ and a loan available for investment $j$,
respectively, we compute the similarity metric using
Equation~(\ref{eq:6}), with three dimensional vectors $\mathbf{p}_{i}$
and $\mathbf{p}_{j}$ identifying the probabilities of full payment,
prepayment, and default for loans $i$ and $j$, respectively.

\begin{align}
s_{j,i} = \frac{e^{-\|\mathbf{p}_{i} - \mathbf{p}_{j}\|_{2}}}{\sum^{N}_{i=1}e^{-\|\mathbf{p}_{i} - \mathbf{p}_{j}\|_{2}}} \label{eq:6}\\
\mathbf{p}_{i} = [{p}_{f,i}, {p}_{p,i}, {p}_{d,i}] \in \mathbb{R}^{3} \nonumber \\
\mathbf{p}_{j} = [\hat{p}_{f,j}, \hat{p}_{p,j}, \hat{p}_{d,j}] \in \mathbb{R}^{3} \nonumber 
\end{align}


Like in the \ac{RBF} case, $s_{j,i}$ is a real number between 0 and 1,
inclusive. Unlike the \ac{RBF} technique, we use the $L^{2}$ norm (more
generally referred to as the Euclidean distance) of the difference
between the two probability vectors. The $L^{2}$ norm of the vector is
\textit{always} positive. Hence, we do not need the range of the
Gaussian that maps from the negative domain (the value of the Gaussian
function, when X-axis goes below zero). Therefore, an exponential
function suffices. Moreover,
$\|\mathbf{p}_{i} - \mathbf{p}_{j}\|_{2} \in [0, \sqrt2]$, hence there
is no need to tune for the bandwidth, which makes the \ac{EDM} method
algorithmically $O(1)$ and scalable for large datasets.

We also considered using Mahalanobis distance to compute the difference
between two probability vectors. The Mahalanobis distance of is always
non-negative. However, results show that using Mahalanobis distance
gives no better results than using simple Euclidean distance. Hence, for
sake of understanding we present all the same results as the
experimental section, but using Mahalanobis distance in~\cite{Appendix}.

% is not better than Euclidean distance and cost more computation time.
% Please refer to the Appendix~\ref{sec:appd} for detail results. Hence,
% we select the Euclidean distance. In
% Section~\ref{sec:comp-accur-pred}, we will show that the proposed
% \ac{EDM} approach scales to large data, unlike the \ac{RBF} approach,
% and moreover it is more effective in predicting the \ac{ARR}.

%\subsubsection{\acf{EDM} for computing the similarity metric}
%\label{sec:acfedm-comp-simil}
%
%Given a historical loan $i$ and a loan available for investment $j$,
%respectively, we compute the similarity metric using
%Equation~(\ref{eq:6}), with three dimensional vectors $\mathbf{p}_{i}$
%and $\mathbf{\hat{p}}_{j}$ identifying the probabilities of full
%payment, prepayment, and default for historical loan $i \in N$ and loan
%$j \in M$ in the testing dataset, respectively.
%
%\begin{align} s_{j,i} = \frac{e^{-D_{Mah}(\mathbf{p}_{i},
%\mathbf{\hat{p}}_{j})}}{\sum^{N}_{i=1}e^{-D_{Mah}(\mathbf{p}_{i},
%\mathbf{\hat{p}}_{j})}} \label{eq:6}\\ \mathbf{p}_{i} = [{\rho}_{f,i},
%{\rho}_{p,i}, {\rho}_{d,i}] \in \mathbb{R}^{3} \nonumber \\ \mathbf{\hat{p}}_{j}
%= [\hat{\rho}_{f,j}, \hat{\rho}_{p,j}, \hat{\rho}_{d,j}] \in \mathbb{R}^{3}
%\nonumber\\ \nonumber\\ D_{Mah}(\mathbf{p}_{i}, \mathbf{\hat{p}}_{j}) =
%\sqrt{(\mathbf{p}_{i}- \mathbf{\hat{p}}_{j})^{T}C^{-1}(\mathbf{p}_{i}-
%\mathbf{\hat{p}}_{j})} \nonumber
%\end{align}
%
%Where $D_{Mah}(\:)$ stands for the Mahalanobis
%distance~\cite{mahalanobis1936generalized}, and $C$ is the covariance
%matrix between $\mathbf{p}_{i}$ and $\mathbf{\hat{p}}_{j}$.
%
%Like in the \ac{RBF} case, $s_{j,i}$ is a real number between 0 and 1,
%inclusive. Unlike the \ac{RBF} technique, we use the Mahalanobis
%distance to compute the difference between two probability vectors. The
%Mahalanobis distance of the vector is \textit{always} non-negative.
%Hence, we do not need the range of the Gaussian that maps from the
%negative domain (the value of the Gaussian function, when X-axis goes
%below zero). Therefore, an exponential function suffices. However, if
%the covariance matrix $C$ is the identity matrix, i.e., there is no
%correlation between $\mathbf{p}_{i}$ and $\mathbf{\hat{p}}_{j}$, then
%the Mahalanobis distance reduces to the Euclidean distance. Since most
%borrowers on \ac{P2PL} platform only borrow once, observed from the
%Prosper dataset, correlation between two different loans should be low.
%Hence, Euclidean distance should suffice for computing the difference
%between $\mathbf{p}_{i}$ and $\mathbf{\hat{p}}_{j}$. Furthermore,
%in case of both Euclidean distance and Mahalanobis distance,
%  $\|\mathbf{p}_{i} - \mathbf{\hat{p}}_{j}\|_{2}\in \mathbb{R}_{\geq 0}$
%  and
%  $ D_{Mah}(\mathbf{p}_{i}, \mathbf{\hat{p}}_{j})\in \mathbb{R}_{\geq
%    0}$, respectively, and hence there is no need to tune for the
%bandwidth, which makes the \ac{EDM} method algorithmically $O(1)$ and
%scalable for large datasets. The experimental evaluation section
%(Section~\ref{sec:exper-eval}) uses both Mahalanobis and Euclidean
%distances when calculating the similarity metric, to discover, which one
%performs best. In Section~\ref{sec:exper-eval}, we also show that the
%proposed \ac{EDM} approach scales to large datasets, unlike the \ac{RBF}
%approach, and moreover it is more effective in predicting the \ac{ARR}.



%%% Local Variables:
%%% mode: latex
%%% TeX-master: "manualscript"
%%% End:
