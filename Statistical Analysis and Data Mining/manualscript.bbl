\begin{thebibliography}{33}
\providecommand{\natexlab}[1]{#1}
\providecommand{\url}[1]{{#1}}
\providecommand{\urlprefix}{URL }
\expandafter\ifx\csname urlstyle\endcsname\relax
  \providecommand{\doi}[1]{DOI~\discretionary{}{}{}#1}\else
  \providecommand{\doi}{DOI~\discretionary{}{}{}\begingroup
  \urlstyle{rm}\Url}\fi
\providecommand{\eprint}[2][]{\url{#2}}

\bibitem[{Alpaydin(2014)}]{alpaydin2014introduction}
Alpaydin E (2014) Introduction to machine learning. MIT press, {Cambridge}, MA

\bibitem[{Andreeva et~al(2007)Andreeva, Ansell, and
  Crook}]{andreeva2007modelling}
Andreeva G, Ansell J, Crook J (2007) Modelling profitability using survival
  combination scores. European Journal of Operational Research
  183(3):1537--1549

\bibitem[{Boyd and Vandenberghe(2004)}]{boyd2004convex}
Boyd S, Vandenberghe L (2004) {Convex Optimization}. Cambridge university
  press, {The} Edingburgh Building, Cambridge, 185-187

\bibitem[{Breiman(2001)}]{breiman2001random}
Breiman L (2001) Random forests. Machine learning 45(1):5--32

\bibitem[{Chatterjee and Barcun(1970)}]{chatterjee1970nonparametric}
Chatterjee S, Barcun S (1970) A nonparametric approach to credit screening.
  Journal of the American statistical Association 65(329):150--154

\bibitem[{Conlin(1999)}]{conlin1999249}
Conlin M (1999) Peer group micro-lending programs in canada and the united
  states. Journal of Development Economics 60(1):249 -- 269,
  \doi{10.1016/S0304-3878(99)00043-7}

\bibitem[{Duarte et~al(2012)Duarte, Siegel, and Young}]{Duarte01082012}
Duarte J, Siegel S, Young L (2012) Trust and credit: The role of appearance in
  peer-to-peer lending. Review of Financial Studies 25(8):2455--2484,
  \doi{10.1093/rfs/hhs071}

\bibitem[{Feldman and Gross(2005)}]{feldman2005mortgage}
Feldman D, Gross S (2005) Mortgage default: classification trees analysis. The
  Journal of Real Estate Finance and Economics 30(4):369--396

\bibitem[{Friedman et~al(2001)Friedman, Hastie, and
  Tibshirani}]{friedman2001elements}
Friedman J, Hastie T, Tibshirani R (2001) {The Elements of Statistical
  Learning}, vol~1. Springer series in statistics Springer, { Berlin}

\bibitem[{Frydman et~al(1985)Frydman, Kallberg, and Kao}]{frydman1985testing}
Frydman H, Kallberg JG, Kao DL (1985) Testing the adequacy of markov chain and
  mover-stayer models as representations of credit behavior. Operations
  Research 33(6):1203--1214

\bibitem[{Funk et~al(2015)Funk, Bachmann, Becker, Buerckner, Hilker, Kock,
  Lehmann, and Tiburtius}]{funk2015online}
Funk B, Bachmann A, Becker A, Buerckner D, Hilker M, Kock F, Lehmann M,
  Tiburtius P (2015) Online peer-to-peer lending? a literature review. The
  Journal of Internet Banking and Commerce 2011

\bibitem[{Guo et~al(2016)Guo, Zhou, Luo, Liu, and Xiong}]{Guo2016417}
Guo Y, Zhou W, Luo C, Liu C, Xiong H (2016) {Instance-based credit risk
  assessment for investment decisions in P2P lending}. {European Journal of
  Operational Research} 249(2):417 -- 426, \doi{10.1016/j.ejor.2015.05.050}

\bibitem[{Herzenstein et~al(2008)Herzenstein, Andrews, Dholakia, and
  Lyandres}]{Herzenstein2008}
Herzenstein M, Andrews RL, Dholakia UM, Lyandres E (2008) The democratization
  of personal consumer loans? determinants of success in online peer-to-peer
  lending communities. Boston University School of Management Research Paper
  14(6):1--45

\bibitem[{Herzenstein et~al(2011)Herzenstein, Sonenshein, and
  Dholakia}]{Herzenstein201111}
Herzenstein M, Sonenshein S, Dholakia UM (2011) Tell me a good story and i may
  lend you money: The role of narratives in peer-to-peer lending decisions.
  Journal of Marketing Research 48(SPL):S138--S149

\bibitem[{J.D.Roth(2012)}]{Roth20121115}
JDRoth (2012) {Taking a Peek at Peer-to-Peer Lending}.
  business.time.com/2012/11/15/taking-a-peek-at-peer-to-peer-lending/

\bibitem[{Khwaja et~al(2009)Khwaja, Iyer, Luttmer, and Shue}]{Lyer200908}
Khwaja AI, Iyer R, Luttmer EF, Shue K (2009) {Screening in New Credit Markets:
  Can Individual Lenders Infer Borrower Creditworthiness in Peer-to-Peer
  Lending?} Tech. rep., National Bureau of Economic Research, Cambridge, MA

\bibitem[{Kim and Sohn(2010)}]{kim2010support}
Kim HS, Sohn SY (2010) Support vector machines for default prediction of smes
  based on technology credit. European Journal of Operational Research
  201(3):838--846

\bibitem[{Klafft(2008)}]{Klafft20080714}
Klafft M (2008) Online peer-to-peer lending: A lenders' perspective.
  Proceedings of the International Conference on E-Learning, E-Business,
  Enterprise Information Systems, and E-Government pp pp. 371--375,
  \doi{10.2139/ssrn.1352352}

\bibitem[{LendingClub(2016)}]{LCW}
LendingClub (2016) Lending club statistics.
  https://www.lendingclub.com/info/download-data.action, {Last} accessed -
  15/9/2016

\bibitem[{Li et~al(2016)Li, Yao, Wen, and Yang}]{li2016prepayment}
Li Z, Yao X, Wen Q, Yang W (2016) {Prepayment and Default of Consumer Loans in
  Online Lending}. Tech. rep., School of financce, Southwestern University of
  Finance and Economics, Sichuan, \doi{10.2139/ssrn.2740858}

\bibitem[{Malekipirbazari and Aksakalli(2015)}]{malekipirbazari2015risk}
Malekipirbazari M, Aksakalli V (2015) Risk assessment in social lending via
  random forests. Expert Systems with Applications 42(10):4621--4631,
  \doi{10.1016/j.eswa.2015.02.001}

\bibitem[{Malhotra and Malhotra(2002)}]{malhotra2002differentiating}
Malhotra R, Malhotra DK (2002) Differentiating between good credits and bad
  credits using neuro-fuzzy systems. European Journal of Operational Research
  136(1):190--211

\bibitem[{Mangasarian(1965)}]{mangasarian1965linear}
Mangasarian OL (1965) Linear and nonlinear separation of patterns by linear
  programming. Operations research 13(3):444--452

\bibitem[{Markowitz(1991)}]{markowitz1991foundations}
Markowitz HM (1991) Foundations of portfolio theory. The journal of finance
  46(2):469--477

\bibitem[{Morduch(1999)}]{morduch1999microfinance}
Morduch J (1999) The microfinance promise. Journal of economic literature
  37(4):1569--1614

\bibitem[{Pedregosa et~al(2011)Pedregosa, Varoquaux, Gramfort, Michel, Thirion,
  Grisel, Blondel, Prettenhofer, Weiss, Dubourg et~al}]{pedregosa2011scikit}
Pedregosa F, Varoquaux G, Gramfort A, Michel V, Thirion B, Grisel O, Blondel M,
  Prettenhofer P, Weiss R, Dubourg V, et~al (2011) Scikit-learn: Machine
  learning in python. Journal of Machine Learning Research 12(Oct):2825--2830

\bibitem[{Rosenberg and Gleit(1994)}]{rosenberg1994quantitative}
Rosenberg E, Gleit A (1994) Quantitative methods in credit management: a
  survey. Operations research 42(4):589--613

\bibitem[{Sharpe(1966)}]{sharpe1966mutual}
Sharpe WF (1966) Mutual fund performance. The Journal of business
  39(1):119--138, \doi{10.1086/294846}

\bibitem[{So and Thomas(2011)}]{so2011modelling}
So MM, Thomas LC (2011) {Modelling the profitability of credit cards by Markov
  decision processes}. European Journal of Operational Research 212(1):123--130

\bibitem[{Stepanova and Thomas(2002)}]{stepanova2002survival}
Stepanova M, Thomas L (2002) Survival analysis methods for personal loan data.
  Operations Research 50(2):277--289

\bibitem[{Suykens and Vandewalle(1999)}]{suykens1999least}
Suykens JA, Vandewalle J (1999) Least squares support vector machine
  classifiers. Neural processing letters 9(3):293--300

\bibitem[{Udacity(2016)}]{Udacity}
Udacity (2016) Prosper loan data. https://github.com/joashxu/prosper-loan-data,
  {Last} accessed - 15/9/2016

\bibitem[{Wiginton(1980)}]{wiginton1980note}
Wiginton JC (1980) A note on the comparison of logit and discriminant models of
  consumer credit behavior. Journal of Financial and Quantitative Analysis
  15(03):757--770

\end{thebibliography}
