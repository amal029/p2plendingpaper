\section{Balancing the unbalanced dataset and feature reduction}
\label{sec:unbalced data}

In this section we investigate the best ratio to split the \ac{P2PL}
unbalanced Prosper dataset into defaulting and non-defaulting loans, for
training and testing purposes. Furthermore, once we have decided on the
best ratio, we perform feature selection, using different techniques, to
obtain the best recall rates for both; defaulting and non-defaulting
loans.

\subsection{Optimising the split of the unbalanced dataset}
\label{sec:way-split}

In order to determine the best ratio to balance the unbalanced dataset, we
start by selecting all 885 defaulting loans along with 885
non-defaulting loans, randomly sampled, from the Prosper dataset. Now we
have a balanced dataset with a ratio 1:1 of defaulting loans to 
non-defaulting loans. Next, we record the recall rates of defaulting and 
non-defaulting loans by applying \acf{LOGIT} and \acf{RF} classification 
techniques. Then, we increase the number of non-defaulting loans by 885 
each time and repeat the process until the ratio of defaulting loans to 
non-defaulting loans reaches 1:19, which is the unbalance present in the 
original dataset.

Figure~\ref{fig:ratio} shows the regression curves obtained by fitting
the results from \ac{LOGIT} and \ac{RF} with ratios of
defaulting:non-defaulting loans ranging from 1:1 to 1:19, respectively.
We observe that the recall rate of non-defaulting loans increases when
the number of non-defaulting loans increases for both \ac{RF} and
\ac{LOGIT}. Conversely, the recall rate of defaulting loans decreases at
the same time. There is a significant decrease, when the ratio of
defaulting loans to non-defaulting loans is increased from 1:1 to 1:2
for \ac{LOGIT}. Specifically, for \ac{LOGIT}, the recall rate of
defaulting loans drops from 70.8\% to 6.2\% and recall rate of
non-defaulting grows from 54.2\% to 98.1\%. This means \ac{LOGIT} is
less stable when performing two-way classification on unbalanced
datasets as compared to \ac{RF}.

\begin{figure}[tbh] \centering
  \includegraphics[width=5.5in]{d_nd_Total}
  \caption{\textbf{Recall rates of sample datasets with different
      ratios}~\label{fig:ratio}}
\end{figure}

We use the tuple $(A_{d}, A_{n})$ to identify the recall rates of
defaulting and non-defaulting loans in the test dataset, respectively.
We extend this notation to specify the \textit{best} recall rates of
defaulting and non-defaulting loans as the tuple:
$(A^{*}_{d},A^{*}_{n})$. The best recall rates are defined, for the
non-dominated frontier~\cite{deb2014multi}\footnote{Also sometimes
  referred to as the Pareto frontier.}, as follows:

\begin{enumerate}
\item Both $A^{*}_{d}$ and $A^{*}_{n} \in [0.5, 1]$, i.e., they must be
  greater than 50\% and less than or equal to 100\%.
\item There is no other pair ($A_{d}',A_{n}'$) such that
  $A_{d}' + A_{n}' > A^{*}_{d}+A^{*}_{n}$.
\end{enumerate}

\begin{figure}[h]
  \includegraphics[width=5.5in]{100}
  \caption{\textbf{100 tests by re-sampling the test
      datasets}~\label{fig:100}}
\end{figure}

Figure \ref{fig:ratio} illustrates the recall rates of sample datasets
with different ratios, we use Figure \ref{fig:ratio} as an example to
find the best recall rates by applying the above rules. From the Figure
\ref{fig:ratio}, we can easily observe that there are only three
feasible points where both $A_{d}$ and $A_{n} \in [0.5, 1]$. Among those
three points, $(0.78, 0.62)$, $(0.72, 0.61)$, and $(0.59, 0.74)$, we
compute and find out that $(0.78, 0.62)$ is the best recall rates, which
coincides with the condition that the ratio of the defaulting loans to
non-defaulting loans is 1:1 for \ac{RF} classification.

We generate the non-dominated frontier, by testing a 100 times, with
re-sampled test datasets for ratios ranging from 1:1 to
1:19. Figure~\ref{fig:100} illustrates the hundred tests of recall rates
on re-sampled test datasets. We can observe that the optimal solution
$(0.70, 0.65)$ happens in one of the \ac{RF}'s fitted lines with ratio
1:1, which is the point on the topmost line in Figure~\ref{fig:100}. The
top most line is the non-dominated frontier, which is generated by
equation: $A^{*}_{d}+A^{*}_{n}=1.35$. Any point on this frontier with
both $A_{d}$ and $A_{n} \in [0.5, 1]$ can be considered as a
non-dominated solution\footnote{also called Pareto solution}, and have
the same value of $A^{*}_{d}+A^{*}_{n}=1.35$ with $(0.70, 0.65)$. It can
be seen from Figure~\ref{fig:100} that beside the optimal point
$(0.70, 0.65)$, all other points are strictly below the green line,
which implies $A_{d}+A_{n}<1.35$ for all other points, and thereby
$(0.70, 0.65)$ is the only optimal point. Therefore, we can conclude
that based on the Prosper dataset, the ratio 1:1 of the defaulting loans
to non-defaulting loans is always the best way to balance the unbalanced
dataset, because the optimal solution ($A^{*}_{d},A^{*}_{n}$) always
appears for the ratio 1:1 with \ac{RF}.


\subsection{Feature reduction}
\label{sec:feature reduction}

% Recall that we still need to reduce the number of features which we
% filtered out in Section \ref{sec3:data descrip}, because there are
% uninformative features, which will reduce the recall rates of defaulting
% and non-defaulting loans. In addition, a large number of features will
% increase the execution time to train the models. However, in 

The recent work by Malekipirbazari and
Aksakalli~\cite{malekipirbazari2015risk}, Serrano-Cinca et.\
al.~\cite{serrano2015determinants}, and Section~\ref{sec:Data analysis},
suggest that the correlations and information gain between individual
feature and response variable are very small. Hence, it is hard to reduce
the number of features by only selection the ones with the highest
correlation or information gain, because there is no significant
difference between them. As a result, we apply forward
selection~\cite{guyon2003introduction}, backward
selection~\cite{guyon2003introduction}, and our proposed recursive
selection algorithms to reduce the number of features. In this section
we apply the above feature selection algorithms along
with the \ac{RF} classification technique on the sample dataset, which
has ratio of non-defaulting loans to defaulting loans of 1:1. Then, we
use statistical measures called ``t-test'' and McNemar's test to analyse
if the result of feature selection algorithms differ
\textit{significantly}.

\paragraph{Forward selection:}
\label{sec:forward-selection}

Forward selection~\cite{friedman2001elements} is a well studied
algorithm for feature reduction. Hence, we only give an overview of the
algorithm along with the obtained results. In forward selection, we
start with an empty feature set. We keep on adding features one after
another into this empty set until the sum of the recall rate of defaulting and
non-defaulting loans stop increasing. We use the \ac{RF}
classification technique to compute the recall rates.

\begin{enumerate}
\item Best recall rates obtained: $(A_{d},A_{n}) = (0.74, 0.54)$.
\item Selected features: `BorrowerRate' and `LenderYield' (2 features in
  total).
\end{enumerate}

\paragraph{Backward selection:}
\label{sec:backward-selection}

In backward selection, we start with a set with all the features. We
keep on dropping features one after another until the sum of the recall rates for
defaulting and non-defaulting loans stop increasing. Here again we
use the \ac{RF} classification model for computing the recall rates.

\begin{enumerate}
\item Best recall rates obtained: $(A_{d},A_{n}) = (0.74, 0.61)$.
\item Selected features: `OpenRevolvingAccounts', `LoanOriginalAmount',
  `LenderYield', `TradesOpenedLast6Months', `TotalTrades',
  `EstimatedEffectiveYield', `OpenCreditLines', `TotalInquiries',
  `MonthlyLoanPayment', `StatedMonthlyIncome', `TradesNeverDelinquent',
  `BorrowerState', `EstimatedLoss', `EmploymentStatusDuration',
  `AmountDelinquent', `CurrentCreditLines', `DelinquenciesLast7Years',
  `Term', `RevolvingCreditBalance', `ListingCategory', `BorrowerRate',
  `CurrentlyInGroup', `AvailableBankcardCredit',
  `TotalCreditLinespast7years', `CurrentDelinquencies',
  `IsBorrowerHomeowner', `EstimatedReturn', `BankcardUtilization',
  `ProsperScore', `CreditScoreRangeLower', `EmploymentStatus',
  `DebtToIncomeRatio', `OpenRevolvingMonthlyPayment',
  `PublicRecordsLast10Years', `Occupation', `InquiriesLast6Months', and
  `BorrowerAPR' (37 features in total).
\end{enumerate}

\paragraph{Recursive selection:}
\label{sec:recursive-selection}

In recursive selection, similar to the forward selection we start with an empty feature set. We keep on adding features one after another and compute the recall rates for each set until there is no features left. Then we select the feature set, which results in the highest sum of the recall rates of defaulting and non-defaulting loans, as the optimal feature set.

\begin{framed}
  \begin{Algorithm}\textbf{(Recursive selection)}\label{alg:rs}\\
    \textbf{Input}: A sample dataset with 38 features.\\
    \textbf{Output}:The score $T = A_{d} + A_{n}$ and a list of selected features.\\
    \textbf{Algorithm}:\\
    1. Set of selected features $S$ starts from null set, and score $T = 0$.\\
    2. Fit the models with all possibilities by adding a unique feature from the features not selected into the set $S$.\\
    3. Compute $T' = A_{d}+A_{n}$ for each model, and replace the set $S$ with the set that has the largest $T'$ if $T' > T$\\
    4. Repeat steps 2-3 until there is no feature left, and output the
    score $T$ and the list of selected features.
  \end{Algorithm}
\end{framed}

\begin{enumerate}
\item Best recall rates obtained: $(A_{d},A_{n}) = (0.73, 0.67)$.
\item Selected features: `LenderYield', `Term', `Occupation',
  `ListingCategory', `BorrowerState', `TotalTrades', `BorrowerAPR', and
  `EmploymentStatus' (8 features in total).
\end{enumerate}

From the results, it can be observed that the highest score
$A_{d} + A_{n} = 1.40$ is achieved by using the recursive selection
algorithm with \textit{only} 8 selected features. On the other hand, the
least number of selected features is achieved by using forward
selection, but the score, $A_{d} + A_{n} = 1.28$, is the lowest among
the three algorithms.

In order to guarantee that the results we obtain are robust, we test 100
times by re-sampling\footnote{We randomly select 30\% of the sample
  dataset as test dataset, and the remaining 70\% as training dataset
  each time.} the test dataset with all the feature selection algorithms
and measure the overall values of
$A_{d}+A_{n}$. Figures~\ref{fig:foward},~\ref{fig:back},
and~\ref{fig:rec} illustrate the distribution of 100 scores by using
forward selection, backward selection, and recursive selection
respectively. The mean values of these distributions are shown in the
Table~\ref{tab:msda}. It can be seen that the highest average score is
achieved by applying recursive selection.

\begin{figure}[h] \centering
  \subfigure[Forward selection~\label{fig:foward}]{
    \includegraphics[width=0.465\textwidth]{foward}
  }
  ~
  \subfigure[Backward selection~\label{fig:back}]{
    \includegraphics[width=0.465\textwidth]{back}
  }
  
  \subfigure[Recursive selection~\label{fig:rec}]{
    \includegraphics[width=0.5\textwidth]{recur}
  }
  \caption{Distribution diagrams of the three algorithms}
  \label{fig:d3}
\end{figure}

\begin{table}[h]
  \centering
  \caption{Average values of score and recall rates by applying
    different algorithms}
  \label{tab:msda}
  \begin{tabular}{|l|l|l|l|}
    \hline
    & Forward & Backward & Recursive \\ \hline
    Mean of $A_{d} + A_{n}$ & 1.29    & 1.34     & 1.39      \\ \hline
    Mean of $A_{d}$ & 0.73    & 0.74     & 0.75      \\ \hline
    Mean of $A_{n}$ & 0.56    & 0.60     & 0.64      \\ \hline
  \end{tabular}
\end{table}

In order to verify that these three distributions are statistically
different, we use ``t-test'' and McNemar's test. The ``t-test'' is a
statistical test, which is used to determine if two sets of data are
significantly different from each other. On the other hand, the
McNemar's test is used to determine if there are differences on a
dichotomous dependent variable between two related groups. Given any of
the two distributions, our null hypothesis is that the mean scores of
these two distributions are exactly the same.
\[H_{0}: \mu_{1} = \mu_{2}\]

\noindent
where $H_{0}$ is the null hypothesis, $\mu_{1}$ is the mean of the first
distribution, and $\mu_{2}$ is the mean of the other distribution. The
statistic-scores and the P-values for each pair of distributions are
shown in Table~\ref{tab:tm}. From Tables~\ref{tab:fb},~\ref{tab:fr},
and~\ref{tab:br}, we can observe that the P-values are extremely small
for both the t-test and McNemar's test for all pairs of
distributions. This implies that we can \textit{reject} the null
hypothesis. Specifically, it means that the scores of forward selection,
backward selection, and recursive selection are statistically different
to each other.

\begin{table}[h]
  \centering
  \caption{T-test and McNemar's test between each pair of distributions}
  \label{tab:tm}
  \subtable[Comparing forward selection and backward
  selection~\label{tab:fb}]{
    \begin{tabular}{|l|l|}
      \hline
      & P-value  \\ \hline
      T-test                 & 9.67e-25 \\ \hline
      McNemar's test          & 2.54e-16 \\ \hline
    \end{tabular}
  } ~ \subtable[Comparing forward selection and recursive
  selection~\label{tab:fr}]{
    \begin{tabular}{|l|l|}
      \hline
      & P-value  \\ \hline
      T-test                  & 9.15e-53 \\ \hline
      McNemar's test          & 1.59e-28 \\ \hline
    \end{tabular}
  } ~ \subtable[Comparing backward selection and recursive
  selection~\label{tab:br}]{
    \begin{tabular}{|l|l|}
      \hline
      & P-value  \\ \hline
      T-test                  & 1.42e-24 \\ \hline
      McNemar's test          & 2.54e-16 \\ \hline
    \end{tabular}
  }
\end{table}

Hence, we can conclude that based on the sample dataset with ratio 1:1
on defaulting loans to non-defaulting loans, the recursive selection
algorithm outperforms the other feature selection algorithms. The
\ac{RF} model built using the features obtained by applying the
recursive selection algorithm has the highest recall rates on both
defaulting and non-defaulting loans for our test dataset. In addition,
after applying the recursive selection algorithm, we only have 8
features in total. Compared to 38 features we started with.

\subsection{Feature set comparison}
\label{sec:comparison}

In this section, we compare the recall rates with features selected by
the proposed algorithm and features selected in Malekipirbazari and
Aksakalli's work~\cite{malekipirbazari2015risk}.  All our experiments in
this section are performed on the balanced Prosper dataset with the
ratio of non-defaulting to defaulting loans equal to 1:1 and use \ac{RF}
technique as the classifier. We split dataset in the ratio of 70\% to
30\% for training and testing the classification algorithms. We compare
the recall rates with the following sets of features:
\begin{enumerate}
\item The 8 features selected by recursive selection in Section 
\ref{sec:feature reduction}.
\item The top 8 correlated features selected\footnote{Prosper Score, 
Income to payment ratio, Stated Monthly Income, Credit Score Range Lower, 
Debt To Income Ratio, Term, Revolving to Income Ratio, and Total Trades.}
in the work of Malekipirbazari and Aksakalli 
~\cite{malekipirbazari2015risk}.
\end{enumerate}
The recall rates of both defaulting and non-defaulting loans with the 
two sets of features are shown in Table \ref{table:cp d vs nd}.
\begin{table}[tbh]
  \centering
  \caption{Recall rates of both defaulting and non-defaulting loans based 
  on the 2 sets of features}
  \label{table:cp d vs nd}
  \begin{tabular}{|l|p{2.9cm}|p{3cm}|}
    \hline
    Set of features & Recall rate of defaulting loans & Recall rate of non-defaulting loans \\ \hline
    1     & 73\%                & 67\%                  \\ \hline
    2     & 61\%                 & 46\%                   \\ \hline
  \end{tabular}
\end{table}

From Table \ref{table:cp d vs nd}, it is clear that based on the
balanced dataset, the recall rates with features selected by recursive
selection algorithm (set 1) outperforms the recall rates with features
selected in the work of Malekipirbazari and
Aksakalli~\cite{malekipirbazari2015risk} (set 2). Specifically, recall
rate of defaulting loans with feature set 1 is 12\% higher than that
with feature set 2, and the recall rate of non-defaulting loans with
feature set 1 is 21\% higher than that with feature set 2. This is as
expected, because the correlations between all features and the response
variable are lower than 0.2 as seen in Figure~\ref{fig:heat}, and the
features selected by Malekipirbazari and Aksakalli are only dependent on
the correlations. This result enforces that the proposed recursive
selection algorithm is more accurate than the current state of the art
method.

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "main"
%%% End:
