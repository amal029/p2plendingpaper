\section{Machine learning models for two-way classification}
\label{sec:machine learing}

In this paper, the problem we address is of the form:
\begin{align*}
  y = f(\mathbf{x})
\end{align*}
where $y$ is the output (i.e., target) variable that takes values
$\in \{0, 1\}$ to represent defaulting and non-defaulting loans
respectively, and $\mathbf{x}$ represents an input vector made up of the
values of features characterising the borrower. During training, we are
given many labeled examples of defaulting and non-defaulting loans,
i.e., input-output pairs of the form $\langle \mathbf{x}, y\rangle$, and
we build a model of the unknown function $f$. Once a model is trained
(i.e., learned), it is used to estimate the output value for previously
unseen input vectors. The objective is to maximise the accuracy of
classification. In this paper, \emph{recall} is used as the performance
measure. Because of the unbalanced dataset, \emph{precision} is not used
as the performance measure. Specifically, there are few defaulting
loans, hence precision rate can be very high by simply predicting all
loans as non-defaulting loans.

In this section, we use four popular classification algorithms, which
are \acf{LOGIT}, \acf{RF}, \acf{SVM}, and \acf{GP}, to train and test
the Prosper dataset and investigate the techniques, which has the
highest recall rates for both; defaulting and non-defaulting loans.
Among these machine learning models; \ac{LOGIT}, \ac{RF}, and \ac{SVM}
were used in the recent works of Malekipirbazari and
Aksakalli~\cite{malekipirbazari2015risk}, and Serrano-Cinca et.\
al.~\cite{serrano2015determinants}. Compared to the total recall
accuracy used in their work, we show the advantages of using the
recall rates of both defaulting and non-defaulting loans separately.

\subsection{\acf{LOGIT}}
\label{sec5:logit}

\acf{LOGIT}~\cite{wiginton1980note} is a regression model where the
response variable only has two possible outcomes, and it is very popular
for analyzing \ac{P2PL} datasets. Given $\hat{p}$ is the probability of
a loan defaulting, the logistic regression function can be written as:
\[
  f(\mathbf{x}) = logit(\hat{p}) = \ln (\frac{\hat{p}}{1 - \hat{p}}) = \beta_{0} +
  \sum\limits_{i = 1}^{N}\beta_{i} \times x_{i},
\]

\noindent
where $\beta_{0}$ is the intercept and $N$ represents the features that
correspond to $x_{1}, \dots, x_{N}$. The \textit{logit} function is
defined as the logged odds:

\[\text{odds} = \frac{\hat{p}}{1 - \hat{p}} = \frac{\text{probability of
      a loan defaulting}}{\text{probability of a loan
      not-defaulting}}.\]

Hence we have the following formulation:
\[y = \begin{cases}
0, & \text{if } f(\mathbf{x})> 0\\
1, & \text{otherwise}.
\end{cases} \]
\subsection{\acf{RF}}
\label{sec5:RF}

\acf{RF}~\cite{feldman2005mortgage} also known as random decision trees
is another popular model to analyze \ac{P2PL}, and is applicable to
classification tasks in which the underlying relationship between inputs
and output is inherently non-linear. The output variable $y$ in \acf{RF}
algorithms takes values $\in \{0, 1\}$ to represent defaulting and
non-defaulting loans, and the unknown function $f$ is built by the
processes below. Given a feature set $\mathbf{x} = x_{1}, \ldots, x_{n}$
with the response variable $y$, we repeatedly ($M$ times) choose random
samples (with replacement) and fit decision trees with the following
steps~\cite{friedman2001elements}:

\begin{enumerate}
\item For each sample, $k = 1,\ldots,M$, let the new training data and
  response variable be $\mathbf{x}_{k}$ and $y_{k}$,
  respectively.
\item Train a decision tree $\hat{f}_{k}$ on each sample $k$.
\item After training $M$ decision trees, the response for a new sample
  $\mathbf{x}'$ can then be predicted by averaging the predictions from each
  decision tree we trained, using the formula below:
  
  \[
    \hat{f} = \frac{1}{M}\sum\limits_{k=1}^{M}\hat{f}_{k}(\mathbf{x}')
  \]
  
\end{enumerate}

\ac{RF}s have several advantages. \ac{RF}s are easy to tune and
accurate. \ac{RF}s can handle nominal data and large dataset efficiently. 
With growing number of decision trees there is lower risk for 
overfitting. However, compared to \ac{LOGIT}, \ac{RF}s are slightly 
harder to interpret.

\subsection{\acf{SVM}}
\label{sec5:SVM}

\acf{SVM}~\cite{suykens1999least} is another popular algorithm for
classification, which can be used to capture linear or non-linear
relationships between input vector and output variable. Specifically,
\ac{SVM} constructs one or more hyperplanes to best separate data
corresponding to different values of the output variable.
Figure~\ref{fig:svm} provides an illustrative example of such
separating (decision) surfaces for data from two classes---here,
$H_{3}$ separates data with the maximum margin, i.e. it is the optimal
separating surface. In other word, the target variable $y$ is determined 
by the optimal separating surface $H = f(\mathbf{x})$. The maximum margin separating surface
is defined by using a kernel function $K(\mathbf{x},\mathbf{x}')$, which is the equivalent of projecting data to an infinite dimensional space using basis functions. 

\begin{figure}[tbh] \centering
  \includegraphics[width=3.0in]{svm}
  \caption{Optimal hyperplane for \ac{SVM} in the linear
    case~\label{fig:svm}. Reproduced from~\cite{WikipediaSVM}}
\end{figure}

\ac{SVM}s also have several advantages. They are effective in high
dimensional space with many features. They execute efficiently for
large datasets, and can handle diverse problems with different
kernel functions. However, \ac{SVM}s are weak in transparency of
results. In other word, \ac{SVM}s cannot estimate the probability or
score directly, because \ac{SVM}s always use hyperplanes to solve
classification problems.

\subsection{\acf{GP} Classification}
\label{sec5:GP}

\acf{GP} Classification is a generic supervised learning algorithm to
solve classification and regression problems. A Gaussian process can be
viewed as an infinite dimensional generalization of multivariate normal
distribution, and hence it supports the ability to explore the infinite
space of functions to figure out the mapping between input $\mathbf{x}$
and output $y$. A Gaussian process uses the kernel function to predict
the value for an unobserved point from the training dataset. For
Gaussian process regression, the prior mean generally is assumed to be
zero, i.e.  $f(\mathbf{x})\sim N(0,K(\mathbf{x},\mathbf{x}'))$, and the
target variable $y$ is determined by the function $f(\mathbf{x})$. Here
$K(\mathbf{x},\mathbf{x}')$ is the covariance matrix or kernel between
all possible pairs $(\mathbf{x},\mathbf{x}')$. Gaussian process can
solve different classification problems by selecting the desired
kernel. In this paper, we use the \acf{RBF} kernel with length parameter
$l=1.0$. The kernel can be written by:
\[K(\mathbf{x},\mathbf{x}')=\exp\big( -\frac{1}{2}\lVert \mathbf{x} - \mathbf{x}'\rVert^{2}\big). \] This kernel is infinitely differentiable, 
which means \ac{GP}s are very smooth because they have mean square 
derivatives of all orders.

There are several advantages by using \ac{GP}s. They can handle diverse 
problems with different kernel functions. The prediction made by using 
\ac{GP}s interpolates the observations. However, \ac{GP}s will be 
inefficient when the feature set is large.

\subsection{Comparison}
\label{sec5:comparison}


In this section, we apply the four algorithms mentioned above on the
Prosper dataset, and investigate the model that has the highest recall
rates for both; defaulting and non-defaulting loans. In all experiments,
we divide the sample dataset into a ratio of 7:3, indicating that 70\%
of the loans will be used to train diverse prediction models -- the
training dataset, and the remaining 30\% will be used for testing -- the
test dataset. The total accuracy/precision used for comparison purposes
can be computed as:

\[
\text{Total accuracy} = \frac{\text{True defaulting loans} +
  \text{True non-defaulting loans}}{\text{Total loans}}.
\]

Recall that in the sample dataset we have 18,572 completed loans with 38
features and 1 response variable --- the loan status. Among these 18,572
completed loans, there are only 885 defaulting loans, which results in
unbalanced dataset with the ratio of non-defaulting to defaulting loans
$\approx$ 1:19. Table~\ref{table:Auccracy will all} illustrates the
total accuracy, of applying each algorithm on the test dataset. It can
be seen that \ac{LOGIT}, \ac{RF}, and \ac{SVM} have a total accuracy
higher than 95\%, and there is only 4.6\% total accuracy by using
\ac{GP} on the test dataset. These results correlate well with the
results from Malekipirbazari and
Aksakalli~\cite{malekipirbazari2015risk}.  However, these high rates of
total accuracy are \textit{misleading}, because most algorithms
\textit{misclassified} defaulting loans as non-defaulting. In other
words, the recall rate of defaulting loans is very low, 0\% for
\ac{LOGIT} and \ac{SVM}, and 2\% for \ac{RF}. This low recall rate on
defaulting loans will mislead lenders to make investments in defaulting
loans, and thereby lose their investment. Hence, only recall rate is
used as the performance measure in further experiments.

\begin{table}[tbh]
  \centering
  \caption{Total accuracy on test data set with 38 features}
  \label{table:Auccracy will all}
  \begin{tabulary}{1.0\textwidth}{|l|l|p{2.9cm}|p{3cm}|}
    \hline
    Algorithm & Total accuracy & Recall rate of  defaulting loans & Recall rate of non-defaulting loans\\ \hline
    LOGIT     & 95.3\%    & 0\%    & 100\%    \\ \hline
    RF        & 95.5\%    & 1.5\%  & 100\%    \\ \hline
    SVM       & 95.3\%    & 0\%    & 100\%    \\ \hline
    GP        & 4.8\%     & 100\%  & 0\%      \\ \hline
  \end{tabulary}
\end{table} 

To overcome the problem of unbalanced dataset, we randomly select 885
non-defaulting loans together with the all the 885 defaulting loans as
our new sample dataset. Resulting in a sample dataset with the ratio of
non-defaulting to defaulting loans equal to 1:1. We again split this new
sample dataset in the ratio of 70\% to 30\% for training and testing the
classification algorithms. The results are shown in Table~\ref{table:d
  vs nd}.

\begin{table}[tbh]
  \centering
  \caption{Recall rates of both defaulting and non-defaulting loans with
    38 features on the balanced dataset}
  \label{table:d vs nd}
  \begin{tabular}{|l|l|p{2.9cm}|p{3cm}|}
    \hline
    Algorithm & Total accuracy & Recall rate of defaulting loans & Recall rate of non-defaulting loans \\ \hline
    LOGIT     & 70.0\% & 74.4\%              & 65.9\%                  \\ \hline
    RF        & 70.0\% & 77.9\%              & 61.0\%                  \\ \hline
    SVM       & 50.7\% & 0\%                 & 100\%                   \\ \hline
    GP        & 49.3\% & 100\%               & 0\%                     \\ \hline
  \end{tabular}
\end{table}

From Table~\ref{table:d vs nd}, we can observe that \ac{SVM}
misclassified all defaulting loans, and conversely \ac{GP} misclassified
all non-defaulting loans. Hence, we reject these two algorithms in later
experiments, as we aim to accurately classify both defaulting and
non-defaulting loans. \ac{RF} performs better in predicting defaulting
loans, and conversely \ac{LOGIT} performs better in predicting
non-defaulting loans.

However, it is hard to determine, which one of \ac{LOGIT} and \ac{RF} is
better if we consider the recall rates of both defaulting and
non-defaulting loans at the same time. For example, if \ac{LOGIT} has
recall rates of defaulting and non-defaulting loans of, say, 75\% and
50\% respectively, and \ac{RF} has a recall rate of 65\% on both
defaulting and non-defaulting loans, then deciding the better model is
equivalent to comparing two vectors, which are (75, 50) and (65, 65).
Therefore, it is necessary to develop a method to compare such vectors,
and thereby find the best model. Moreover, in order to test whether the
ratio 1:1 is the best way to split the unbalanced dataset, we also need
to split our original dataset into different ratios to find the best
split.



%%% Local Variables:
%%% mode: latex
%%% TeX-master: "main"
%%% End:
