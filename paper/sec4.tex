\section{A new encoding of categorical features for \ac{P2PL} datasets}
\label{sec:method categorical}

First we elaborate on the standard binary encoding
technique~\cite{lourenco2004binary} that encodes categorical features
into a set of binary features, which lead to an increase in the number
of total features. We then present the proposed encoding technique.

\subsection{The standard binary encoding technique}
\label{sec4:binary method}
To encode the categorical features into numerical values, a so called
binary encoding technique~\cite{lourenco2004binary} is the most popular
technique. For instance, suppose we have a feature called `fruit', which
can take three possible values: `apple', `banana', and `peach'. The
feature `fruit' can then be transferred into a set of three numerical
features:
\begin{itemize}
\item apple: \{0, 1\}, which takes the numerical value 1 if the fruit is
  apple, 0 otherwise.
\item banana: \{0, 1\}, which takes the numerical value 1 if the fruit
  is banana, 0 otherwise.
\item peach: \{0, 1\}, which takes the numerical value 1 if the fruit is
  peach, 0 otherwise.
\end{itemize}

However, this method is only feasible when the number of values in a
categorical feature is small. In case when a categorical feature has a
plethora of values, this encoding is not useful and will increase the
number of features in total.

If we consider the feature `BorrowerState' as an example, there are a
total of 48 different states (values) for this feature. Using the binary
encoding technique will transfer this single categorical feature into 48
numerical features, which dramatically increases the number of features
and consequently the duration to train and test any classification
model. More importantly, if we use the binary encoding technique to
encode the categorical features in Prosper dataset and compute the
information gain for each feature, the result shows that the information
gains of these newly introduced dummy features is extremely low, which
means that we have \textit{lost} information encoded into the
features. This result is validated by Malekipirbazari and
Aksakalli~\cite{malekipirbazari2015risk}. Hence, we need a new technique
for encoding categorical features.

\subsection{The proposed percentage encoding technique}
\label{sec4:percentage encoding}

In order to overcome the above problems, we develop a new method to
transfer categorical features which we call \textit{percentage encoding
  technique}. Based on the Prosper dataset, given a categorical feature,
it can be encoded in a \textit{single} numerical feature using the
following steps:

\begin{enumerate}
\item Compute the frequency of occurrence for each class of the
  categorical feature.
\item For each class, compute the percentage of either defaulting loans
  or non-defaulting loans.
\item Replace the categorical data with the percentage value.
\end{enumerate}

Again consider the feature `BorrowerState' as an example. There are
1,148 borrowers who live in New York, and 96.4\% of them did not
default. Hence we replace the `NY' class with `0.964' in
`BorrowerState'. We keep performing the above process for each state
(class in `Borrowerstate' feature) and end up with 48 numerical values
for the feature. To appreciate that the new encoding does not lead to
loss of information, we measure the correlations between the categorical
features, which are `BorrowerState' and `Occupation', and the response
variable `Loanstatus'. From Figure~\ref{fig:heat}, we observe that the
correlations between the response variable and the encoded categorical
features, `BorrowerState' and `Occupation', are higher than most of the
other features, which means our percentage encoding method maintain the
information of the categorical feature `BorrowerState'.


The advantages of our percentage encoding are: 
\begin{enumerate}
\item Easy to compute and interpret.
\item Does not increase the number of features in total.
\item Experimental studies do not indicate any significant information loss for the dataset under consideration.
\end{enumerate}

However, there are some limitations of the percentage encoding
technique. Firstly, the response variable must be binary, i.e.\ it only
works for a two-way classification problem. Secondly, categorical data
with sentences can not be encoded. For example, some \ac{P2PL} platforms
have the feature, which include the reason for borrowing money. These
reasons are written by borrowers themselves, as a result, people with
the same reason may use different words. Nevertheless, the percentage
encoding method is sufficient to transfer all the categorical features
of the Prosper dataset that we use in this work.



%%% Local Variables:
%%% mode: latex
%%% TeX-master: "main"
%%% End:
