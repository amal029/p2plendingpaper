\section{Introduction}
\label{sec:introduction}


\acf{P2PL} system is a micro financing platform, which is emerging as an
alternative to traditional financial lenders such as banks. In a
\ac{P2PL} system, individuals directly borrow and lend money to each
other. The lenders decide to invest in a particular loan by looking
through the information provided by the borrower. However, there are
usually over $100$ borrower characteristics (also called \emph{features}
or \emph{attributes}), which include both financial features such as
monthly income, and non-financial features such as the borrower's
occupation.  Since most lenders are not financial experts, it is
difficult for them to identify the features that might correlate
strongly to a particular borrower defaulting on any given loan. As a
result, lenders will find it hard to select the correct loan to invest
in. In order to help lenders be aware of the risk of default associated
with each individual loan, lending platforms like
Prosper\footnote{www.prosper.com} and Lending
Club\footnote{www.lendingclub.com} often provide a \emph{risk rating}.
The risk rating of a loan is denoted by letters grades: A, B, C, D, E
and F, where A stands for the loan with the lowest chance of default
(and consequently the lowest return rate), and F stands for the highest
chance of default (and consequently the highest return rate). In
addition, Prosper also provides a numerical risk rating system, called
the \emph{Prosper score}. Prosper scores range from 1 to 11, with each
Prosper score corresponding to a range of probability that the loan will
default. A score of 1 corresponds to the probability of default ranging
from $0\%\ \mathrm{to}\ 2.7\%$, and 11 corresponding to a probability
range greater than $13\%$. However, these risk ratings are not
sufficient, because even loans assigned a risk rating of A may default,
and not all loans assigned a high Prosper score will default. Thus, the
borrower features need to be investigated carefully before making an
investment.

\subsection{Challenges and contributions}
\label{sec:chall-contr}
As stated earlier, each borrower is characterised by more than $100$
features, not all of which are useful for identifying loans likely to
default. Thus, the first challenge is to figure out which features are
most informative for identifying the defaulting loans. In recent work,
Malekipirbazari and Aksakalli~\cite{malekipirbazari2015risk} computed
information gain and correlation of the borrower features with respect
to the loan's status, i.e., defaulting or non-defaulting.  It was
observed that the information gain from each features and the
correlation between individual features and the loan status were quite
small. In this work, we explore the reasons for such low correlations,
and explore techniques for selecting and combining the most relevant
features for classifying loans.

The second challenge is to numerically encode the categorical features
such as borrower's occupation and the reason for borrowing money. For
P2PL systems, the common approach, as demonstrated by Malekipirbazari
and Aksakalli~\cite{malekipirbazari2015risk}, is to use the binary
encoding technique. The technique encodes each categorical feature as a
set of binary features, with each binary feature corresponding to a
possible value of the categorical feature.  For example, if feature
``colour'' can take values ``red'', ``yellow'', or ``blue''.  Using the
binary encoding technique, colour is encoded as three separate binary
features: ``red'', ``yellow'', and ``blue'' with numerical values $1$
and $0$ representing the presence or absence of the corresponding
colour.  However, binary encoding increases the number of features in
the dataset, especially if the categorical feature can take one of a
large number of values.  In this paper, we explore a different encoding
of the categorical features that does not increase the number of
features in the dataset.
  
The final challenge is to deal with an unbalanced dataset that has far
fewer instances of defaulting loans than non-defaulting loans. Recent
work by Tsai et. al.~\cite{tsai2014peer}, and Berger and
Gleisner~\cite{berger2010emergence} indicated that an unbalanced data
will negatively impact classification accuracy in \ac{P2PL} systems.  In
this paper, we experimentally determine the best ratio of samples of
defaulting and non-defaulting loans for training and testing the models
based on machine learning algorithms for classifying loans.
  
In summary, this work seeks to help lenders correctly recognize
defaulting loans and reduce the loss on investments.  The three key
technical \textbf{contributions} are: (1) exploring a different encoding
of each categorical feature as a single numerical feature without any
significant loss of information; (2) experimentally determining the best
ratio of defaulting and non-defaulting loans in the training and test
datasets, to handle the unbalanced dataset problem; and (3) computing
the most informative features that maximize the accuracy of identifying
both defaulting and non-defaulting loans.

The remainder of this paper is organized as follows. First,
Section~\ref{sec:related work} reviews the state of the art in
classifying \ac{P2PL} datasets. Then, Section~\ref{sec:Data analysis}
analyses the \ac{P2PL} dataset to highlight the low correlation
between many features and the status of the loan, followed by
Section~\ref{sec:method categorical} that describes our approach to
encode categorical features. Next, Section~\ref{sec:machine learing}
compares the performance of four classification algorithms when they
are applied to the \ac{P2PL} dataset and Section~\ref{sec:unbalced
  data} experimentally explores the best ratio of defaulting and
non-defaulting loans to be used for training and testing the
classification models.  Finally, Section~\ref{sec:conclusion}
describes the conclusions and ideas for future work.


%%% Local Variables:
%%% mode: latex
%%% TeX-master: "main"
%%% End:
