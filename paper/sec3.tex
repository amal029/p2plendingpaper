\section{Data analysis}
\label{sec:Data analysis}

In this paper, we train and test based on dataset from a well
established \ac{P2PL} platform, Prosper. We choose Prosper, because
Prosper is America's first \ac{P2PL} online platform with over \$9
billion in funded loans since 2006.~\cite{Prop} In addition, Prosper has
been in operation for more than 10 years, and hence, can offer a
plethora of completed loans that are necessary for training and
testing. Other researchers who study \ac{P2PL} also use dataset from
Propser, which makes it easy for us to compare our work with the current
state of the art techniques.

\subsection{Data description}
\label{sec3:data descrip}

The Prosper dataset used in this paper is provided by
Udacity~\cite{Udacity}. This dataset contains 113,938 loans in total,
dating from 2005 to 2014. Since this dataset includes both completed and
current loans, we only select the completed loans without any missing
features. There are 81 features for each loan, including both financial
and borrower's personal characteristics. However, not all these 81
features are applicable to our study; we follow three simple rules to
remove any feature that is not applicable:

\begin{itemize}

\item Blank or zero valued features, because there is no information in
  such features.

\item Features that are not applicable to new loans, for example, `Loan
  current days delinquent'. Since we are aiming to predict the loan
  status of new loans, any feature that is recorded after the loan
  started should not be considered.

\item Features associated with loan IDs.\@ Since each loan already has
  unique ID, which is similar to an index.

\end{itemize}

After applying these rules, we end up with 18,572 completed loans with
38 features and 1 response variable, the status of the loan --- default
or non-default. The explanation of these 38 features and the response
variable are shown in Table~\ref*{table:fe}.

\begin{table}[th]
  \centering
  \caption{Features and response variable description}
  \label{table:fe}
  \resizebox{\columnwidth}{!}{%
    \begin{scriptsize}
      \begin{tabular}{|l|l|l|}
        \hline
        Feature   & Explanation    & Type                                                                                                                                                                              \\ \hline
        BorrowerRate                       & The Borrower's interest rate for this loan.                              & Numerical                                                                                                                    \\ \hline
        LenderYield                        & The Lender yield on the loan.                                & Numerical                                               \\ \hline
        OpenCreditLines                    & Number of open credit lines.                             & Numerical                                                                                                                                    \\ \hline
        EstimatedEffectiveYield            & Effective yield that estimated by Prosper.              & Numerical \\ \hline
        EstimatedLoss                      & Estimated principal loss on charge-offs.                    & Numerical                                                                                                           \\ \hline
        ProsperRating            & A custom rating score built by Prosper.                 & Numerical                                                                                                                  \\ \hline
        ProsperScore                       & A custom risk score built by Prosper.                            & Numerical                                                                                                         \\ \hline
        ListingCategory         & The category of the listing that the borrower.                      & Numerical                                                                                     \\ \hline
        CurrentCreditLines                 & Number of current credit lines.                               & Numerical                                                                                                                               \\ \hline
        TotalCreditLinespast7years         & Number of credit lines in the past seven years.                             & Numerical                                                                                                                 \\ \hline
        OpenRevolvingAccounts              & Number of open revolving accounts.                            & Numerical                                                                                                                               \\ \hline
        OpenRevolvingMonthlyPayment        & Monthly payment on revolving accounts.                             & Numerical                                                                                                                          \\ \hline
        TotalInquiries                     & Total number of inquiries.                            & Numerical                                                                                                                                       \\ \hline
        CurrentDelinquencies               & Number of accounts delinquent.                           & Numerical                                                                                                                                    \\ \hline
        AmountDelinquent                   & Dollars delinquent.                          & Numerical                                                                                                                                                \\ \hline
        Occupation                         & The Occupation selected by the Borrower.                           & Categorical                                                                                                                          \\ \hline
        PublicRecordsLast10Years           & Number of public records in the past 10 years.                               & Numerical                                                                                                                \\ \hline
        RevolvingCreditBalance             & Dollars of revolving credit.                             & Numerical                                                                                                                                    \\ \hline
        TradesNeverDelinquent & Trades that have never been delinquent.                          & Numerical                                                                                                              \\ \hline
        TotalTrades                        & Number of trade lines ever opened.                              & Numerical                                                                                                                             \\ \hline
        StatedMonthlyIncome                & The monthly income the borrower stated.                              & Numerical                                                                                                                        \\ \hline
        AvailableBankcardCredit            & The total available credit via bank card.                                 & Numerical                                                                                                                   \\ \hline
        MonthlyLoanPayment                 & The scheduled monthly loan payment.                             & Numerical                                                                                                                             \\ \hline
        TradesOpenedLast6Months            & Number of trades opened in the last 6 months.                              & Numerical                                                                                                                  \\ \hline
        BankcardUtilization                & The percentage of available credit that is utilized.                            & Numerical                                                                                                   \\ \hline
        IsBorrowerHomeowner                & Specifies if the borrower is a homeowner or not.                                & Binary                                                                                                        \\ \hline
        BorrowerAPR                        & The Borrower's Annual Percentage Rate.                                 & Numerical                                                                                                   \\ \hline
        DebtToIncomeRatio                  & The debt to income ratio of the borrower.                            & Numerical                                                                                                                        \\ \hline
        EstimatedReturn                    & Return that is estimated by Prosper.                         & Numerical                                                               \\ \hline
        InquiriesLast6Months               & Number of inquiries in the past six months.                              & Numerical                                                                                                                    \\ \hline
        LoanOriginalAmount                 & The origination amount of the loan.                                & Numerical                                                                                                                          \\ \hline
        CreditScoreRangeLower              & The lower range of the borrower's credit score.                                & Numerical                                                                                       \\ \hline
        EmploymentStatusDuration           & The length in months of the employment status.                             & Numerical                                                                                                                  \\ \hline
        DelinquenciesLast7Years            & Number of delinquencies in the past 7 years.                             & Numerical                                                                                                                    \\ \hline
        Term                               & The length of the loan expressed in months.                            & Numerical                                                                                                                      \\ \hline
        BorrowerState                      & The state of the address of the borrower.                           & Categorical                                                                                          \\ \hline
        EmploymentStatus                   & The employment status of the borrower.                          & Numerical                                                                                                                            \\ \hline
        CurrentlyInGroup                   & Specifies if or not the Borrower was in a group.                                & Binary                                                                                                       \\ \hline
        LoanStatus                         & The current status of the loan.                                & Binary                                                                                      \\ \hline 
      \end{tabular}%
    \end{scriptsize}
  }
\end{table}

\clearpage

\subsection{Data distribution}
\label{sec3:data distrib}

Information encoded in each feature with regards to loan status can be
gauged by looking at the correlation between individual borrower
features and the loan status. We show the frequency distribution
histograms for each feature with respect to different loan status ---
default and non-default. Specifically, we compare the probability
distribution of defaulting loans with that of non-defaulting loans for
each feature, and discover features, which have significant correlation
with the status of the loan.


\begin{figure}[h!]
  \centering
  \includegraphics[height=0.455\textheight, width=1.0\textwidth]{p_1} 
  \includegraphics[height=0.455\textheight, width=1.0\textwidth]{p_2}
  \caption{The probability distribution histograms~\label{fig:hist_1}}
\end{figure}

\begin{figure}[h!]
  \centering
  \includegraphics[height=0.455\textheight, width=1.0\textwidth]{p_3}
  \includegraphics[height=0.455\textheight, width=1.0\textwidth]{p_4}
  \caption{The probability distribution histograms cont.~\label{fig:hist_2}}
\end{figure}

From Figures~\ref{fig:hist_1} and~\ref{fig:hist_2}, it can be seen that
the probability distributions of defaulting and non-defaulting loans for
individual features is very similar to each other in most cases. If this
situation happens for all features, it would mean any loan has 50\%
chance of defaulting when we consider \textit{only} a single
feature. This is why the information gain and correlation with respect
to loan status is very low (<0.2) for all features which are selected in
the recent work of Malekipirbazari and
Aksakalli~\cite{malekipirbazari2015risk}, because there is no way to
distinguish between defaulting and non-defaulting loans by only looking
at individual features. Figure~\ref{fig:heat} makes the correlation more
prominent by building a heat map of correlations between the 38 features
and the response variable (the loan status). It can be observed that the
absolute values of correlations are all lower than 0.2. It is
interesting to note that even ProsperRating and ProsperScore, which
should identify the likelihood of default for a given loan, are
uncorrelated with the LoanStatus.

Fortunately, we observe that there are differences between defaulting
and non-defaulting loans in probability distribution of some features.
For instance, probability distribution histogram of the feature
`BorrowerRate', Figure~\ref{fig:hist_1}(i) shows that loans with
higher rates are more likely to default and vice-versa. However, these
differences are not significant enough to correctly identify the loan
as possibly defaulting or not on their own. In other words, features
that are selected simply by looking at the correlation and information
gain are not sufficient to correctly classify defaulting and
non-defaulting loans. As a result, it is necessary to further identify and combine the informative features from the 38 total features. 
On the one side, combinations of existing features can be treated as new
features and sometimes are helpful for prediction. Specifically,
combinations of  features can have higher values of correlations
between themselves and the loan status, even though the value
of correlation for unique feature is small. On the other side, compared
to features themselves, combinations of features sometimes are hard to
interpret. However, features like `BorrowerState' and `Occupation' are
categorical data, we first need to transform them into numerical data. 
In the next section we will propose a new technique to encode
categorical data.

\begin{figure}[h!]
  \centering
  \includegraphics[width=5.0in]{heat} 
  \caption{The heat map of correlations between 38 features and the loan
    status~\label{fig:heat}.}
\end{figure}


%%% Local Variables:
%%% mode: latex
%%% TeX-master: "main"
%%% End:
