\section{Experimental evaluation and discussion}
\label{sec:exper-eval}

In this section we perform a thorough comparison of the various
techniques described from Sections~\ref{sec:class-meth}
to~\ref{sec:portf-optim-model}. We divide this section into four
subsections, following the steps presented in Figure~\ref{fig:overall},
as follows:

\begin{itemize}
\item We describe the experimental setup in
  Section~\ref{sec:experimental-setup}.
\item Section~\ref{sec:comp-accur-class} then compares the accuracy of
  various classification techniques presented in
  Section~\ref{sec:class-meth}.
\item Section~\ref{sec:comp-accur-pred} compares the accuracy of
  predicting the \ac{ARR} and standard deviation between the best
  classification techniques from Section~\ref{sec:comp-accur-class}.
\item Finally, in Section~\ref{sec:mean-vari-front}, we present the
  Markowitz frontier generation and Sharpe ratio computation for the
  best technique from Section~\ref{sec:comp-accur-pred}, and compare the
  portfolio \ac{ARR} and standard deviation (risk) between them.
\end{itemize}

\subsection{Experimental setup}
\label{sec:experimental-setup}

All our experiments are performed on the Prosper and Lending Club
datasets (c.f. Section~\ref{sec:data-description}). We sample 10,000
completed loans from each dataset and divide these into a ratio of 95:5,
indicating the 9500 loans, which will be used to train various
prediction models and the remaining 500 loans used for testing. We use
the eight predictor variables presented in
Section~\ref{sec:feature-selection} for training our models. We give two
sets of results for both datasets: (1) 5-fold \ac{CV} results for the
training dataset, and (2) results for the test dataset. We randomly
sample 5 sets with 100 loans in each set out of the 500 loans in the
test dataset, and present the results for these 5 test datasets of 100
loans each. Where necessary, we also give the execution time for the
various prediction models. The models were built using the
Scikit-learn~\citep{pedregosa2011scikit} Python library. All experiments
were executed on Intel Core i7-4720HQ CPU, running at 2.6GHZ with 8.00
GB of RAM with Windows 10.

\subsection{Comparison of accuracy of classification}
\label{sec:comp-accur-class}

In this section we compare the accuracy of three-label classification
for the \ac{MLOGIT}, \ac{RF}, and the proposed \ac{H3LC} techniques. The
work in~\cite{li2016prepayment} proposes the \ac{MLOGIT} three-label
classification technique for classifying loans, the work
in~\cite{malekipirbazari2015risk} shows that \ac{RF} is the best
classification technique for \ac{P2PL} loans. Hence, in the presented
results we also compare the proposed \ac{H3LC} (c.f.
Section~\ref{sec:acfh3lc}) technique with \ac{RF} and \ac{MLOGIT}
techniques.

\subsubsection{Results of 5-fold CV on the training dataset}
\label{sec:5-fold-cv}

\begin{figure*}[t]
  \centering
  \subfigure[Prosper dataset accuracy results~\label{fig:5foldcvp}]{
    \begin{scriptsize}
      \begin{tabular}{|c|c|c|c|c|c|c|}
        \hline
        Technique & 1 & 2 & 3 & 4 & 5 & Average \\
        \hline
        \hline
        \ac{MLOGIT} & 68 & 70 & 69 & 71 & 65 & 68.6\\
        \hline
        \ac{RF} & 67 & 68 & 67 & 70 & 65 & 67.4\\
        \hline
        \ac{H3LC}\_\ac{RF} & 85 & 87 & 86 & 86 & 88 & \textbf{86.4}\\
        \hline
        \ac{H3LC}\_\ac{LOGIT} & 69 & 67 & 68 & 65 & 67 & 67.2\\
        \hline
      \end{tabular}
    \end{scriptsize}
    
  } \subfigure[Lending Club dataset accuracy
  results~\label{fig:5foldcvlc}]{
    \begin{scriptsize}
      \begin{tabular}{|c|c|c|c|c|c|c|}
        \hline
        Technique & 1 & 2 & 3 & 4 & 5 & Average \\
        \hline
        \hline
        \ac{MLOGIT} & 52 & 50 & 49 & 50 & 50 & 50.2\\
        \hline
        \ac{RF} & 47 & 48 & 49 & 49 & 47 & 48\\
        \hline
        \ac{H3LC}\_\ac{RF} & 76 & 75 & 76 & 74 & 77 & \textbf{75.6}\\
        \hline
        \ac{H3LC}\_\ac{LOGIT} & 45 & 44 & 47 & 47 & 46 & 45.8\\
        \hline
      \end{tabular}
    \end{scriptsize}
    
  }
  \subfigure[Prosper dataset average accuracy scores for each of the
  three labels~\label{fig:5foldacclabp}] {
    \begin{scriptsize}
      \begin{tabular}{|c|c|c|c|}
        \hline
        Technique & FP & PP & D\\
        \hline
        \hline
        \ac{MLOGIT} & 0.98 & 99 & 3.8 \\ 
        \hline
        \ac{RF} & 96 & 24 & 93 \\
        \hline
        \ac{H3LC}\_\ac{RF} & 97 & 99 & 95 \\
        \hline
        \ac{H3LC}\_\ac{LOGIT} & 75 & 25 & 3.4 \\
        \hline
      \end{tabular}
    \end{scriptsize}
  }\qquad
  \subfigure[Lending Club dataset average accuracy scores for each of the
  three labels~\label{fig:5foldacclablc}] {
    \begin{scriptsize}
      \begin{tabular}{|c|c|c|c|}
        \hline
        Technique & FP & Prepayment & Default \\
        \hline
        \hline
        \ac{MLOGIT} & 14 & 90 & 0 \\ 
        \hline
        \ac{RF} & 36 & 68 & 8.9 \\
        \hline
        \ac{H3LC}\_\ac{RF} & 85 & 93 & 2.7\\
        \hline
        \ac{H3LC}\_\ac{LOGIT} & 15 & 90.0 & 0 \\
        \hline
      \end{tabular}
    \end{scriptsize}
  } 
  \caption{Classification accuracy results (in \%) on 5-fold CV of
    training dataset. FP: Full payment, PP: Prepayment, D: Default}
  \label{fig:5foldcv}
\end{figure*}

The results for the 5-fold CV on the training dataset from Prosper and
Lending Club in shown in Figure~\ref{fig:5foldcv}. Recall from
Section~\ref{sec:random-forest}, that we build 100, fully unrolled,
decision trees in all the \ac{RF} techniques.

From Figures~\ref{fig:5foldcvp} and~\ref{fig:5foldcvlc}, it is clear
that the proposed \ac{H3LC} meta-classification technique out performs
both \ac{MLOGIT} and \ac{RF} based three-label classification. In fact,
\ac{H3LC} with \ac{RF} classification (termed \ac{H3LC}\_\ac{RF}) being
used for step-A and step-B (c.f. Section~\ref{sec:acfh3lc})
significantly outperforms all other techniques. We also get higher
accuracy scores for the Prosper dataset compared to the Lending Club
dataset.

The reasons for these results becomes clear from
Figures~\ref{fig:5foldacclabp} and~\ref{fig:5foldacclablc}, which show
the \textit{average} accuracy scores for each of the three possible
classes for the loan status response variable. From both these figures
it is clear that the accuracy of correctly predicting the loan as
prepaid is very high. This is as expected, because a large number of
borrowers prepay their loans as seen in Figures~\ref{fig:pls}
and~\ref{fig:lcls}. Hence, there are a large number of samples to
adequately influence the prediction model. Correctly classifying a loan
as full payment is the highest for \ac{H3LC}\_\ac{RF}, which contributes
to the increased overall accuracy of classification. Finally, the
accuracy of correctly classifying defaulted loans is quite low. Most
borrowers do \textit{not} default, hence there are a very small number
of samples to correctly influence the prediction model. We would also
like to point out that the execution time for \ac{H3LC}\_\ac{RF} and
\ac{RF} techniques is $25 \times$ larger (approximately 10 seconds) than
the execution time for \ac{H3LC}\_\ac{LOGIT} and \ac{MLOGIT} techniques
(approximately 0.4 seconds).

\subsubsection{Results for the five test dataset}
\label{sec:test-results}

The accuracy results for the 5 test datasets, containing 100 loans each,
is shown in Figure~\ref{fig:acccomp}. The results follow the same trend
as for 5-fold CV. The proposed \ac{H3LC}\_\ac{RF} technique outperforms
all other classification techniques. Like before, the accuracy of
correctly classifying a defaulting loan is the lowest (c.f.
Figures~\ref{fig:acclabp} and~\ref{fig:acclablc}). These test results
also confirm that there is no over fitting of the classification models.


\begin{figure*}[h!]
  \centering
  \subfigure[Prosper test dataset classification accuracy results\label{fig:acccompp}]
  {
    \begin{scriptsize}
      \begin{tabular}{|c|c|c|c|c|c|c|}
        \hline
        Technique & 1 & 2 & 3 & 4 & 5 & Average \\
        \hline
        \hline
        \ac{MLOGIT} & 67 & 73 & 68 & 64 & 65 & 67.5\\
        \hline
        \ac{RF} & 73 & 73 & 71 & 64 & 57 & 67.6\\
        \hline
        \ac{H3LC}\_\ac{RF} & 90 & 91 & 94 & 92 & 93 & \textbf{92}\\
        \hline
        \ac{H3LC}\_\ac{LOGIT} & 75 & 73 & 67 & 70 & 73 & 71.6\\
        \hline
      \end{tabular}
    \end{scriptsize}
  }
  \subfigure[Lending Club test dataset classification accuracy
  results\label{fig:acccomplc}] {
    \begin{scriptsize}
      \begin{tabular}{|c|c|c|c|c|c|c|}
        \hline
        Technique & 1 & 2 & 3 & 4 & 5 & Average \\
        \hline
        \hline
        \ac{MLOGIT} & 56 & 54 & 59 & 55 & 40 & 52.8\\
        \hline
        \ac{RF} & 53 & 44 & 52 & 48 & 48 & 49\\
        \hline
        \ac{H3LC}\_\ac{RF} & 88 & 80 & 80 & 79 & 81 & \textbf{81.6}\\
        \hline
        \ac{H3LC}\_\ac{LOGIT} & 51 & 51 & 51 & 43 & 45 & 48.2\\
        \hline
      \end{tabular}
    \end{scriptsize}
  }

  \subfigure[The average accuracy scores for each of the three
  labels for Prosper dataset\label{fig:acclabp}]
  {
    \begin{scriptsize}
      \begin{tabular}{|c|c|c|c|}
        \hline
        Technique & FP & PP & D\\
        \hline
        \hline
        \ac{MLOGIT} & 0 & 99.3 & 0 \\ 
        \hline
        \ac{RF} & 4.45 & 94.66 & 9.6 \\
        \hline
        \ac{H3LC}\_\ac{RF} & 99 & 98.5 & 2.8 \\
        \hline
        \ac{H3LC}\_\ac{LOGIT} & 0 & 99.5 & 4.8 \\
        \hline
      \end{tabular}
    \end{scriptsize}
  } \qquad
  \subfigure[The average accuracy scores for each of the three
  labels for Lending Club dataset\label{fig:acclablc}]
  {
    \begin{scriptsize}
      \begin{tabular}{|c|c|c|c|}
        \hline
        Technique & FP & PP & D \\
        \hline
        \hline
        \ac{MLOGIT} & 7.5 & 92.8 & 0 \\ 
        \hline
        \ac{RF} & 31.6 & 92.83 & 7.5 \\
        \hline
        \ac{H3LC}\_\ac{RF} & 81.3 & 97.5 & 2.2\\
        \hline
        \ac{H3LC}\_\ac{LOGIT} & 12.6 & 92.3 & 0 \\
        \hline
      \end{tabular}
    \end{scriptsize}
  } 
  \caption{Accuracy results for three-label loan status classification
    (in \%) on test datasets. FP: Full payment, PP: Prepayment, D:
    Default.}
  \label{fig:acccomp}
\end{figure*}

\begin{figure}[h!]
  \centering
  \subfigure[Prosper average accuracy scores for default/non-default
  two-label classification~\label{fig:twolabelaccdndp}]
  {
    \begin{scriptsize}
      \begin{tabular}{|c|c|c|}
        \hline
        Technique & Default & Non default \\
        \hline
        \hline
        \ac{LOGIT} & 4.88 & 100 \\
        \hline
        \ac{RF} & 2.8 & 97.6 \\
        \hline
      \end{tabular}
    \end{scriptsize}
  }
  \qquad
  \subfigure[Lending Club average accuracy scores for default/non-default
  two-label classification~\label{fig:twolabelaccdndlc}]
  {
    \begin{scriptsize}
      \begin{tabular}{|c|c|c|}
        \hline
        Technique & Default & Non default \\
        \hline
        \hline
        \ac{LOGIT} & 0 & 100 \\
        \hline
        \ac{RF} & 2.2 & 98\\
        \hline
      \end{tabular}
    \end{scriptsize}
  }
  \caption{Average accuracy scores (in \%) for two-label classification of
    loan status for the test dataset}
  \label{fig:twolabelacc}
\end{figure}

Finally, we present the two-label classification scores, in
Figure~\ref{fig:twolabelacc}, for the 5 test datasets. Recall that
\ac{H3LC} uses the classification shown in Figure~\ref{fig:twolabelacc}
in step-A. Moreover, ~\cite{Guo2016417} propose to use
\ac{LOGIT} based two label classification for predicting the \ac{ARR}
and standard deviation in step-2 (Figure~\ref{fig:overall}) of the
overall methodology. The results from Figure~\ref{fig:twolabelacc} will
be used for comparison in Section~\ref{sec:comp-accur-pred}.

\subsection{Comparison of accuracy of predicting \ac{ARR} and standard
  deviation}
\label{sec:comp-accur-pred}

In this section we compare the accuracy of predicting the \ac{ARR} and
the standard deviation of the loans being considered for investment
using the \ac{RBF} technique prescribed by ~\cite{Guo2016417}
and presented in the Section~\ref{sec:acfrbf-comp-simil} and the
proposed \ac{EDM} technique presented in
Section~\ref{sec:acfedm-comp-simil}.

The probability of default required for the \ac{RBF} technique is
computed using \ac{LOGIT} and \ac{RF} as shown in
Figure~\ref{fig:twolabelacc} and the three-vector indicating the
probability of full payment, prepayment, and default, required for
\ac{EDM}, is computed using the \ac{H3LC}\_\ac{LOGIT} and
\ac{H3LC}\_\ac{RF} as shown in Figure~\ref{fig:acccomp}. As before, we
give the results for the 5-fold CV and the test datasets.

\subsubsection{Results of 5-fold CV on training dataset}
\label{sec:results-5-fold}

\begin{figure}[h!]
  \centering
  \subfigure[5-fold CV real \ac{ARR} Vs predicted \ac{ARR} (in \%) for Prosper dataset~\label{fig:arrep}]
  {  \begin{scriptsize}
       \begin{tabular}{|l|c|c|c|c|c|c|}
         \hline
         Average ARR of loans & Real & Predicted & Real & Predicted & Real & Predicted \\ 
         \hline\hline
         Technique & \multicolumn{2}{c|}{1} & \multicolumn{2}{c|}{2} & \multicolumn{2}{c|}{3} \\ 
         \hline
         LOGIT\_RBF & 7.7 & 8.1 & 10 & 7 & 5.3 & 9.3 \\ 
         \hline
         RF\_RBF & 7.7 & 12 & 10 & 12 & 5.3 & 13 \\ 
         \hline
         H3LC\_RF\_EDM & 7.7 & 9.5 & 10 & 9 & 5.3 & 10 \\ 
         \hline
         H3LC\_LOGIT\_EDM & 7.7 & 7.7 & 10 & 6.9 & 5.3 & 8.8 \\  
         \hline\hline
         Average \ac{ARR} of loans & \multicolumn{2}{c|}{4} & \multicolumn{2}{c|}{5} & \multicolumn{2}{c|}{Average} \\ 
         \hline\hline
         LOGIT\_RBF & 3.8 & 8.1 & 6.8 & 8.4 & 6.72 & 8.18 \\ 
         \hline
         RF\_RBF & 3.8 & 12 & 6.8 & 13 & 6.72 & 12.4 \\ 
         \hline
         H3LC\_RF\_EDM & 3.8 & 9 & 6.8 & 9.9 & 6.72 & 9.48 \\ 
         \hline
         H3LC\_LOGIT\_EDM & 3.8 & 7.4 & 6.8 & 8.2 & 6.72 & 7.8 \\ 
         \hline
       \end{tabular}
    \end{scriptsize}
  }
  \subfigure[5-fold CV real \ac{ARR} Vs predicted \ac{ARR} (in \%) for Lending Club dataset~\label{fig:arrelc}]
  {  \begin{scriptsize}
       \begin{tabular}{|l|c|c|c|c|c|c|}
         \hline
         Average \ac{ARR} of loans & Real & Predicted & Real & Predicted & Real & Predicted \\ 
         \hline\hline
         Technique & \multicolumn{2}{c|}{1} & \multicolumn{2}{c|}{2} & \multicolumn{2}{c|}{3} \\ \hline
         LOGIT\_RBF & -2.7 & 2.3 & 3.4 & 0.9 & 1 & 1.4 \\ 
         \hline
         RF\_RBF & -2.7 & 6.1 & 3.4 & 5.6 & 1 & 4.1 \\ 
         \hline
         H3LC\_RF\_EDM & -2.7 & 3.2 & 3.4 & 2.8 & 1 & 1.5 \\ 
         \hline
         H3LC\_LOGIT\_EDM & -2.7 & 1.9 & 3.4 & -0.1 & 1 & 0.8 \\ 
         \hline\hline
         Average \ac{ARR} of loans & \multicolumn{2}{c|}{4} & \multicolumn{2}{c|}{5} & \multicolumn{2}{c|}{Average} \\ 
         \hline\hline
         LOGIT\_RBF & -1.9 & 0.72 & -1.2 & -1.7 & -0.28 & 0.724 \\ 
         \hline
         RF\_RBF & -1.9 & 6 & -1.2 & 6.6 & -0.28 & 5.68 \\ 
         \hline
         H3LC\_RF\_EDM & -1.9 & 2.1 & -1.2 & 0.46 & -0.28 & 2.012 \\ 
         \hline
         H3LC\_LOGIT\_EDM & -1.9 & 0.6 & -1.2 & -1.8 & -0.28 & 0.28 \\ 
         \hline
       \end{tabular}
    \end{scriptsize}
  }

  \caption{5-fold CV results for predicted \ac{ARR} for loans under
    consideration vis-a-vis real \ac{ARR}.}
  \label{fig:arrer}
\end{figure}

\begin{figure}[tb]
  \centering
  \subfigure[5-fold CV real standard deviation Vs predicted standard deviation for Prosper dataset~\label{fig:stddevp}]
  {  \begin{scriptsize}
       \begin{tabular}{|l|c|c|c|c|c|c|}
         \hline
         Standard deviation & Real & Predicted & Real & Predicted & Real & Predicted \\ 
         \hline\hline
         Technique & \multicolumn{2}{c|}{1} & \multicolumn{2}{c|}{2} & \multicolumn{2}{c|}{3} \\ 
         \hline
         LOGIT\_RBF & 0.2 & 0.017 & 0.18 & 0.034 & 0.25 & 0.013 \\ 
         \hline
         RF\_RBF & 0.2 & 0.081 & 0.18 & 0.078 & 0.25 & 0.054 \\  
         \hline
         H3LC\_RF\_EDM & 0.2 & 0.095 & 0.18 & 0.081 & 0.25 & 0.066 \\ 
         \hline
         H3LC\_LOGIT\_EDM & 0.2 & 0.024 & 0.18 & 0.027 & 0.25 & 0.014 \\ 
         \hline\hline
         Standard deviation & \multicolumn{2}{c|}{4} & \multicolumn{2}{c|}{5} & \multicolumn{2}{c|}{Average} \\ 
         \hline\hline
         LOGIT\_RBF & 0.24 & 0.0097 & 0.21 & 0.04 & 0.216 & 0.02274 \\ 
         \hline
         RF\_RBF & 0.24 & 0.035 & 0.21 & 0.083 & 0.216 & 0.0662 \\ 
         \hline
         H3LC\_RF\_EDM & 0.24 & 0.047 & 0.21 & 0.14 & 0.216 & 0.0858 \\ 
         \hline
         H3LC\_LOGIT\_EDM & 0.24 & 0.012 & 0.21 & 0.03 & 0.216 & 0.0214 \\ 
         \hline
       \end{tabular}
    \end{scriptsize}
  }
  \subfigure[5-fold CV real standard deviation Vs predicted standard deviation for Lending Club dataset~\label{fig:stddevlc}]
  {  \begin{scriptsize}
       \begin{tabular}{|l|c|c|c|c|c|c|}
         \hline
         Standard deviation & Real & Predicted & Real & Predicted & Real & Predicted \\ 
         \hline\hline
         Technique & \multicolumn{2}{c|}{1} & \multicolumn{2}{c|}{2} & \multicolumn{2}{c|}{3} \\ 
         \hline
         LOGIT\_RBF & 0.28 & 0.074 & 0.13 & 0.028 & 0.19 & 0.031 \\ 
         \hline
         RF\_RBF & 0.28 & 0.1 & 0.13 & 0.067 & 0.19 & 0.057 \\ 
         \hline
         H3LC\_RF\_EDM & 0.28 & 0.096 & 0.13 & 0.079 & 0.19 & 0.061 \\ 
         \hline
         H3LC\_LOGIT\_EDM & 0.28 & 0.075 & 0.13 & 0.031 & 0.19 & 0.029 \\ 
         \hline\hline
         Standard deviation & \multicolumn{2}{c|}{4} & \multicolumn{2}{c|}{5} & \multicolumn{2}{c|}{Average} \\ 
         \hline\hline
         LOGIT\_RBF & 0.24 & 0.042 & 0.23 & 0.05 & 0.214 & 0.045 \\ 
         \hline
         RF\_RBF & 0.24 & 0.11 & 0.23 & 0.058 & 0.214 & 0.0784 \\ 
         \hline
         H3LC\_RF\_EDM & 0.24 & 0.11 & 0.23 & 0.062 & 0.214 & 0.0816 \\ 
         \hline
         H3LC\_LOGIT\_EDM & 0.24 & 0.051 & 0.23 & 0.049 & 0.214 & 0.047 \\ 
         \hline
       \end{tabular}
    \end{scriptsize}
  }

  \caption{5-fold CV results for predicted standard deviation for
    loans under consideration vis-a-vis real
    standard deviation.}
  \label{fig:stddeve}
\end{figure}

Figure~\ref{fig:arrer} shows the \textit{predicted} \ac{ARR} (c.f.
Equation~(\ref{eq:2})) compared to real \ac{ARR}. First of all, the
two-label classification techniques along with \ac{RBF} (as proposed by
~\cite{Guo2016417}) predict a higher \ac{ARR} compared to three-label
classification (\ac{H3LC} technique) with the proposed \ac{EDM}
technique for the same underlying classification method. In particular,
compared to the real \ac{ARR}, the \textit{predicted} \ac{ARR} using
2-label classification with \ac{RBF} is grossly overestimated (almost
double for the Prosper dataset and more than double for the Lending Club
dataset). On the other hand, the \textit{predicted} \ac{ARR} of
\ac{H3LC} with \ac{EDM} is also overestimated, but it is closer to the
real \ac{ARR} compared to the 2-label classification with \ac{RBF}. As
the accuracy sores of correctly predicting the defaulting loans is low
for both techniques, many defaulted loans are classified as non-default,
which leads to this overestimation.

The comparison between the standard deviation of loans
for the two label classification with \ac{RBF}
as proposed by ~\cite{Guo2016417} and the proposed three
label classification with the \ac{EDM} technique is shown in
Figure~\ref{fig:stddeve}. On average, both techniques predict a lower
standard deviation compared to the real standard deviation. Similar to
the overestimation of \ac{ARR}, the smaller \textit{predicted} standard
deviation is due to the misclassification of the defaulted loans.

Finally, the \ac{RBF} technique is orders of magnitude slower compared
to the proposed \ac{EDM} technique, because one needs to perform leave
one out cross validation in order to tune the bandwidth, while \ac{EDM}
does not require any tuning at all. Figure~\ref{fig:exect} shows the
execution time comparison between the \ac{EDM} and \ac{RBF} techniques
with growing size of training dataset. It can be observed that, when the
number of training samples increases, the execution time of \ac{RBF}
grows quadratically. However, the executing time of the proposed
\ac{EDM} technique remains constant. Figure~\ref{fig:exect} validates
the algorithmic complexity ($O(1)$ for \ac{EDM} and $O(N^{2})$ for
\ac{RBF}) of each of these techniques.

\begin{figure}[h!]
\centering
\includegraphics[width=3.1in]{exec_t}
\caption{Execution time comparison of \ac{RBF} and \ac{EDM}
  techniques~\label{fig:exect}}
\end{figure}

\subsubsection{Results for the five test dataset}
\label{sec:results-test-dataset}

\begin{figure}[h!]
  \centering
  \subfigure[5-test real \ac{ARR} Vs predicted \ac{ARR}(in \%) for Prosper dataset~\label{fig:arrep_t}]
  {  \begin{scriptsize}
        \begin{tabular}{|l|c|c|c|c|c|c|}
          \hline
          Mean of ARR & Real & Predicted & Real & Predicted & Real & Predicted \\ 
          \hline\hline
          Technique & \multicolumn{2}{c|}{1} & \multicolumn{2}{c|}{2} & \multicolumn{2}{c|}{3} \\ 
          \hline
          LOGIT\_RBF & 8 & 8.6 & 6.2 & 11 & 7.3 & 7.6 \\ 
          \hline
          RF\_RBF & 8 & 10 & 6.2 & 14 & 7.3 & 11 \\ 
          \hline
          H3LC\_RF\_EDM & 8 & 8.9 & 6.2 & 10 & 7.3 & 8.5 \\ 
          \hline
          H3LC\_LOGIT\_EDM & 8 & 7.8 & 6.2 & 9.3 & 7.3 & 6.9 \\ 
          \hline\hline
          Mean of ARR & \multicolumn{2}{c|}{4} & \multicolumn{2}{c|}{5} & \multicolumn{2}{c|}{Average} \\ 
          \hline\hline
          LOGIT\_RBF & 8.5 & 6.4 & 10 & 9.2 & 8 & 8.56 \\ 
          \hline
          RF\_RBF & 8.5 & 10 & 10 & 12 & 8 & 11.4 \\ 
          \hline
          H3LC\_RF\_EDM & 8.5 & 8.1 & 10 & 10 & 8 & 9.1 \\ 
          \hline
          H3LC\_LOGIT\_EDM & 8.5 & 6.1 & 10 & 8.8 & 8 & 7.78 \\ \hline
        \end{tabular}
    \end{scriptsize}
  }
  \subfigure[5-test real \ac{ARR} Vs predicted \ac{ARR} (in \%) for Lending Club dataset~\label{fig:arrelc_t}]
  {  \begin{scriptsize}
       \begin{tabular}{|l|c|c|c|c|c|c|}
         \hline
         Mean of ARR & Real & Predicted & Real & Predicted & Real &  Predicted \\ 
         \hline\hline
         Technique & \multicolumn{2}{c|}{1} & \multicolumn{2}{c|}{2} & \multicolumn{2}{c|}{3} \\ 
         \hline
         LOGIT\_RBF & -7.8 & 1 & 1.2 & -0.15 & -0.75 & -1.1 \\ 
         \hline
         RF\_RBF & -7.8 & 7.3 & 1.2 & 3.2 & -0.75 & 1.4 \\ \hline
         H3LC\_RF\_EDM & -7.8 & 2.4 & 1.2 & 1.2 & -0.75 & -0.31 \\  
         \hline
         H3LC\_LOGIT\_EDM & -7.8 & 0.8 & 1.2 & 0.11 & -0.75 & -1.8 \\ 
         \hline\hline
         Mean of ARR & \multicolumn{2}{c|}{4} & \multicolumn{2}{c|}{5} & \multicolumn{2}{c|}{Average} \\ 
         \hline\hline
         LOGIT\_RBF & 1 & -1.6 & -3.1 & -1.6 & -1.89 & -0.69 \\ 
         \hline
         RF\_RBF & 1 & 4.3 & -3.1 & 5.6 & -1.89 & 4.36 \\ \hline
         H3LC\_RF\_EDM & 1 & -0.5 & -3.1 & 0.86 & -1.89 & 0.73 \\ 
         \hline
         H3LC\_LOGIT\_EDM & 1 & -1.9 & -3.1 & -2.3 & -1.89 & -1.018 \\ 
         \hline
       \end{tabular}
    \end{scriptsize}
  }
  \caption{Results for predicted \ac{ARR} for the five test datasets
    vis-a-vis real \ac{ARR}.}
  \label{fig:arrer_t}
\end{figure}

\begin{figure}[h!]
  \centering \subfigure[5-test real std-deviation Vs predicted standard
  deviation for Prosper dataset~\label{fig:stddevp_t}]
  { \begin{scriptsize}
       \begin{tabular}{|l|c|c|c|c|c|c|}
         \hline
         Standard deviation & Real & Predicted & Real & Predicted & Real & Predicted \\ 
         \hline\hline
         Technique & \multicolumn{2}{c|}{1} & \multicolumn{2}{c|}{2} & \multicolumn{2}{c|}{3} \\ 
         \hline
         LOGIT\_RBF & 0.22 & 0.014 & 0.24 & 0.003 & 0.17 & 0.041 \\ 
         \hline
         RF\_RBF & 0.22 & 0.17 & 0.24 & 0.07 & 0.17 & 0.072 \\ \hline
         H3LC\_RF\_EDM & 0.22 & 0.23 & 0.24 & 0.12 & 0.17 & 0.14 \\ 
         \hline
         H3LC\_LOGIT\_EDM & 0.22 & 0.011 & 0.24 & 0.01 & 0.17 & 0.031 \\ 
         \hline\hline
         Standard deviation & \multicolumn{2}{c|}{4} & \multicolumn{2}{c|}{5} & \multicolumn{2}{c|}{Average} \\ 
         \hline\hline
         LOGIT\_RBF & 0.18 & 0.04 & 0.21 & 0.04 & 0.204 & 0.0276 \\ 
         \hline
         RF\_RBF & 0.18 & 0.14 & 0.21 & 0.08 & 0.204 & 0.1064 \\ 
         \hline
         H3LC\_RF\_EDM & 0.18 & 0.16 & 0.21 & 0.1 & 0.204 & 0.15 \\ 
         \hline
         H3LC\_LOGIT\_EDM & 0.18 & 0.05 & 0.21 & 0.05 & 0.204 & 0.0304 \\ 
         \hline
       \end{tabular}
    \end{scriptsize}
  }
  \subfigure[5-test real standard deviation Vs predicted standard deviation for Lending Club dataset~\label{fig:stddevlc_t}]
  {  \begin{scriptsize}
       \begin{tabular}{|l|c|c|c|c|c|c|}
         \hline
         Standard deviation & Real & Predicted & Real & Predicted & Real & Predicted \\ 
         \hline\hline
         Technique & \multicolumn{2}{c|}{1} & \multicolumn{2}{c|}{2} & \multicolumn{2}{c|}{3} \\ 
         \hline
         LOGIT\_RBF & 0.31 & 0.006 & 0.24 & 0.003 & 0.25 & 0.005 \\ 
         \hline
         RF\_RBF & 0.31 & 0.01 & 0.24 & 0.07 & 0.25 & 0.15 \\ 
         \hline
         H3LC\_RF\_EDM & 0.31 & 0.014 & 0.24 & 0.083 & 0.25 & 0.15 \\ 
         \hline
         H3LC\_LOGIT\_EDM & 0.31 & 0.008 & 0.24 & 0.008 & 0.25 & 0.007 \\ 
         \hline\hline
         Standard deviation & \multicolumn{2}{c|}{4} & \multicolumn{2}{c|}{5} & \multicolumn{2}{c|}{Average} \\ 
         \hline\hline
         LOGIT\_RBF & 0.22 & 0.039 & 0.23 & 0.014 & 0.25 & 0.0134 \\ 
         \hline
         RF\_RBF & 0.22 & 0.076 & 0.23 & 0.072 & 0.25 & 0.0756 \\ 
         \hline
         H3LC\_RF\_EDM & 0.22 & 0.077 & 0.23 & 0.096 & 0.25 & 0.084 \\ 
         \hline
         H3LC\_LOGIT\_EDM & 0.22 & 0.038 & 0.23 & 0.017 & 0.25 & 0.0156 \\ 
         \hline
       \end{tabular}
    \end{scriptsize}
  }
  \caption{Test results for predicted standard deviation for loans
    vis-a-vis real standard deviation.}
  \label{fig:stddeve_t}
\end{figure}

% Now, that we have shown that
The \textit{predicted} \ac{ARR} compared to real \ac{ARR} for the 5 test
datasets, containing 100 loans each, is shown in
Figure~\ref{fig:arrer_t}. The results show the same trend as for 5-fold
CV. The proposed technique (\ac{H3LC}\_\ac{EDM}) outperforms the current
state of the art. Similar to the 5-fold CV, the \textit{predicted}
\ac{ARR} of \ac{H3LC} with \ac{EDM} is closer to the real \ac{ARR}
compared to the two-label classification with \ac{RBF} for the same
underlying classification method. Of particular concern is the grossly
overestimated \ac{ARR} by the \ac{RF}\_\ac{RBF} technique for the
Lending Club test dataset. As seen in Figure~\ref{fig:arrer_t}, the
\ac{RF}\_\ac{RBF} technique not only estimates the negative returns as
positive, but the overestimate is $330\%$ worse. Similarly,
Figure~\ref{fig:stddeve_t} shows the comparison between the
\textit{predicted} and real standard deviation for loans, which displays
the same trend as for the 5-fold CV. 

\subsection{Mean-variance frontier generation and portfolio selection}
\label{sec:mean-vari-front}

\begin{figure}[h!]
  \centering
  \subfigure[Real portfolio \ac{ARR} Vs predicted portfolio \ac{ARR} for Prosper dataset \label{fig:port_pros_ARR}]
  {
    \begin{scriptsize}
      \begin{tabular}{|l|c|c|c|c|c|c|}
        \hline
        Portfolio ARR & Real & Predicted & Real & Predicted & Real & Predicted \\ 
        \hline\hline
        Technique & \multicolumn{2}{c|}{1} & \multicolumn{2}{c|}{2} & \multicolumn{2}{c|}{3} \\ 
        \hline
        LOGIT\_RBF & 7 & 8.9 & 8.3 & 10 & 3.1 & 7 \\ 
        \hline
        RF\_RBF & 9 & 13 & 9.1 & 14 & 2.6 & 12 \\ 
        \hline
        H3LC\_RF\_EDM & 12 & 9.8 & 11 & 11 & 4.2 & 8.2 \\ 
        \hline
        H3LC\_LOGIT\_EDM & 10 & 8.4 & 9.8 & 9.4 & 3 & 6.2 \\ 
        \hline\hline
        Portfolio ARR & \multicolumn{2}{c|}{4} & \multicolumn{2}{c|}{5} & \multicolumn{2}{c|}{Average} \\ 
        \hline\hline
        LOGIT\_RBF & 5.6 & 7.1 & 8.8 & 9.3 & 6.56 & 8.46 \\ 
        \hline
        RF\_RBF & 4.7 & 13 & 8.4 & 14 & 6.76 & 13.2 \\ 
        \hline
        H3LC\_RF\_EDM & 6.6 & 8.5 & 9.1 & 11 & 8.58 & 9.7 \\ 
        \hline
        H3LC\_LOGIT\_EDM & 5.4 & 5.7 & 8.8 & 8.8 & 7.4 & 7.7 \\ 
        \hline
      \end{tabular}
    \end{scriptsize}
  }
  \qquad
  \subfigure[Real portfolio \ac{ARR} Vs predicted portfolio \ac{ARR} for Lending Club dataset\label{fig:port_lend_ARR}]
  {
    \begin{scriptsize}
      \begin{tabular}{|l|c|c|c|c|c|c|}
        \hline
        Portfolio ARR & Real & Predicted & Real & Predicted & Real & Predicted \\ 
        \hline\hline
        Technique & \multicolumn{2}{c|}{1} & \multicolumn{2}{c|}{2} & \multicolumn{2}{c|}{3} \\ 
        \hline
        LOGIT\_RBF & 1.9 & 1.3 & -0.65 & 0.71 & -1.9 & -0.68 \\ 
        \hline
        RF\_RBF & 3.1 & 7.7 & -0.9 & 6.6 & -1.7 & 6.1 \\ 
        \hline
        H3LC\_RF\_EDM & 3 & 2.8 & -0.3 & 2.1 & 0.5 & 1.1 \\ 
        \hline
        H3LC\_LOGIT\_EDM & 2.3 & 1.1 & -0.2 & 0.23 & -1.9 & -1.5 \\ 
        \hline\hline
        Portfolio ARR & \multicolumn{2}{c|}{4} & \multicolumn{2}{c|}{5} & \multicolumn{2}{c|}{Average} \\ 
        \hline\hline
        LOGIT\_RBF & -2.3 & 0.9 & 1.2 & 2.2 & -0.35 & 0.886 \\ 
        \hline
        RF\_RBF & -4.2 & 7.3 & 2.3 & 7.4 & -0.28 & 7.02 \\ 
        \hline
        H3LC\_RF\_EDM & -1 & 1 & 2.7 & 2.4 & 0.98 & 1.88 \\ 
        \hline
        H3LC\_LOGIT\_EDM & 1.1 & -1.5 & 2.4 & 1.9 & 0.74 & 0.046 \\ 
        \hline
      \end{tabular}
    \end{scriptsize}
  }
  \caption{5-test results for predicted portfolio \ac{ARR} vis-a-vis real portfolio \ac{ARR} }
  \label{fig:port_ARR}
\end{figure}

\begin{figure}[h!]
  \centering
  \subfigure[Real portfolio standard deviation Vs predicted portfolio standard deviation for Prosper dataset \label{fig:port_pros_std}]
  {
    \begin{scriptsize}
      \begin{tabular}{|l|c|c|c|c|c|c|}
        \hline
        Portfolio standard deviation & Real & Predicted & Real & Predicted & Real & Predicted \\ 
        \hline\hline
        Technique & \multicolumn{2}{c|}{1} & \multicolumn{2}{c|}{2} & \multicolumn{2}{c|}{3} \\ \hline
        LOGIT\_RBF & 0.18 & 0.14 & 0.2 & 0.15 & 0.2 & 0.18 \\ 
        \hline
        RF\_RBF & 0.17 & 0.087 & 0.19 & 0.1 & 0.18 & 0.12 \\ 
        \hline
        H3LC\_RF\_EDM & 0.17 & 0.14 & 0.19 & 0.17 & 0.2 & 0.19 \\ 
        \hline
        H3LC\_LOGIT\_EDM & 0.17 & 0.15 & 0.2 & 0.16 & 0.23 & 0.19 \\ 
        \hline\hline
        Portfolio standard deviation & \multicolumn{2}{c|}{4} & \multicolumn{2}{c|}{5} & \multicolumn{2}{c|}{Average} \\ 
        \hline\hline
        LOGIT\_RBF & 0.21 & 0.18 & 0.19 & 0.16 & 0.196 & 0.162 \\ 
        \hline
        RF\_RBF & 0.17 & 0.1 & 0.17 & 0.11 & 0.176 & 0.1034 \\ 
        \hline
        H3LC\_RF\_EDM & 0.19 & 0.18 & 0.18 & 0.16 & 0.186 & 0.168 \\ 
        \hline
        H3LC\_LOGIT\_EDM & 0.24 & 0.19 & 0.2 & 0.17 & 0.208 & 0.172 \\ 
        \hline
      \end{tabular}
    \end{scriptsize}
  }
  \qquad
  \subfigure[Real portfolio standard deviation Vs predicted portfolio standard deviation for Lending Club dataset\label{fig:port_lend_std}]
  {
    \begin{scriptsize}
      \begin{tabular}{|l|c|c|c|c|c|c|}
        \hline
        Portfolio standard deviation & Real & Predicted & Real & Predicted & Real & Predicted \\ 
        \hline\hline
        Technique & \multicolumn{2}{c|}{1} & \multicolumn{2}{c|}{2} & \multicolumn{2}{c|}{3} \\ 
        \hline
        LOGIT\_RBF & 0.23 & 0.17 & 0.21 & 0.18 & 0.31 & 0.3 \\ 
        \hline
        RF\_RBF & 0.28 & 0.088 & 0.21 & 0.089 & 0.21 & 0.11 \\ 
        \hline
        H3LC\_RF\_EDM & 0.23 & 0.16 & 0.24 & 0.18 & 0.23 & 0.22 \\ 
        \hline
        H3LC\_LOGIT\_EDM & 0.2 & 0.17 & 0.24 & 0.19 & 0.36 & 0.3 \\ 
        \hline\hline
        Portfolio standard deviation & \multicolumn{2}{c|}{4} & \multicolumn{2}{c|}{5} & \multicolumn{2}{c|}{Average} \\ 
        \hline\hline
        LOGIT\_RBF & 0.19 & 0.19 & 0.23 & 0.18 & 0.234 & 0.204 \\ 
        \hline
        RF\_RBF & 0.14 & 0.11 & 0.22 & 0.1 & 0.212 & 0.0994 \\ 
        \hline
        H3LC\_RF\_EDM & 0.25 & 0.24 & 0.21 & 0.19 & 0.232 & 0.198 \\ 
        \hline
        H3LC\_LOGIT\_EDM & 0.3 & 0.28 & 0.29 & 0.28 & 0.278 & 0.244 \\ 
        \hline
      \end{tabular}
    \end{scriptsize}
  }
  \caption{5-test results for predicted portfolio standard deviation vis-a-vis real portfolio standard deviation }
  \label{fig:port_std}
\end{figure}

In this section we apply the mean-variance portfolio optimization (c.f.
Section~\ref{sec:sharpe-ratio}) on the test dataset. We compare the
portfolio \acp{ARR} and standard deviations obtained from the Markowitz
frontiers and the highest Sharpe ratios of the test datasets by using
the \ac{H3LC}\_\ac{RF}\_{EDM}, \newline \ac{H3LC}\_\ac{LOGIT}\_{EDM},
\ac{LOGIT}\_\ac{RBF}, and \ac{RF}\_\ac{RBF} techniques.

Given the portfolio solution $x \in \mathbb{R}^{100}$, for each of the
five test datasets, with the highest Sharpe ratio, computed by
Equations~(\ref{eq:9}) and~(\ref{eq:10}), the \textit{predicted}
portfolio \ac{ARR} $\hat{R}_{port}$ and standard deviation
$\hat{\sigma}_{port}$ are calculated by Equations~(\ref{eq:11})
and~(\ref{eq:12}), respectively:

\begin{equation}
  \hat{R}_{port} = x^{T}\hat{\mu}, \:\: x,\hat{\mu} \in \mathbb{R}^{100},
  \label{eq:11}
\end{equation}

\begin{equation}
  \hat{\sigma}_{port} = x^{T}\hat{\sigma}, \:\: x,\hat{\sigma} \in \mathbb{R}^{100},
  \label{eq:12}
\end{equation}

\noindent
where $\hat{\mu}$ is the \textit{predicted} \ac{ARR} of the test
dataset, and $\hat{\sigma}$ is the \textit{predicted} standard deviation
(risk).

The \textit{predicted} portfolio returns compared to real portfolio
returns for test datasets is shown in Figure~\ref{fig:port_ARR}. Similar
to the results shown in Figure~\ref{fig:arrer_t}, the \textit{predicted}
portfolio returns for \ac{H3LC} with \ac{EDM} is closer to the real
portfolio returns as compared to the 2-label classification with
\ac{RBF}. On average, 2-label classification with \ac{RBF}
(\ac{LOGIT}\_\ac{RBF} and \ac{RF}\_\ac{RBF}) overestimate the portfolio
return by almost $7.7 \times$, compared to the proposed \ac{H3LC} with
\ac{EDM} overestimation of $1.03\times$. The closest portfolio \ac{ARR}
is predicted by technique \ac{H3LC}\_\ac{LOGIT}\_{EDM}. The real
portfolio \ac{ARR} generated by the proposed technique \ac{H3LC} with
\ac{EDM} is always higher than the current state of the art 2-label
classification with \ac{RBF}. Particularly, the technique
\ac{H3LC}\_\ac{RF}\_{EDM} generates the highest real portfolio \ac{ARR}
compared to others. This result is as expected, since \ac{H3LC}\_\ac{RF}
could correctly classify a loan as prepaid/full payment as indicated by
the accurate score in Figure~\ref{fig:acccomp}. In addition, it can be
observed from Figure~\ref{fig:port_lend_ARR} that all the techniques
generate very low real portfolio \ac{ARR}s on Lending Club data set.
This is mainly due to the fact that all techniques can not classify
defaulted loans correctly, and Lending Club dataset has more defaulted
loans compared to Prosper. Moreover, most of the defaulted loans in
Lending Club only repay 1/10 of the loan amount, and this leads to the
real \ac{ARR} of the defaulted loans to become very low (around -90\%),
which in turn, results in the low real portfolio \ac{ARR}.

Figure~\ref{fig:port_std} displays the comparison between the
\textit{predicted} portfolio standard deviation (risk) and the real
portfolio standard deviation. One can observe that all four techniques
\textit{underestimate} the real portfolio standard deviation, and this
is because of the misclassification of defaulted loans. Specifically,
many defaulted loans are classified as non-default, which then leads to
the predicted risk of each defaulted loan to become smaller. Therefore,
the predicted portfolio risk is always smaller than the real portfolio
risk. Especially, the \ac{RF}\_\ac{RBF} technique underestimates the
portfolio standard deviation wildly (only half of the real).

Finally, in Appendix, we supply figures and tables to
show the Markowitz efficiency frontiers and the highest Sharpe ratios
for each technique. The highest Shape ratio is predicted by
\ac{RF}\_\ac{RBF}. However, since \ac{RF}\_\ac{RBF} overestimates the
portfolio returns and underestimates the portfolio standard deviation
dramatically, this Sharpe ratio is meaningless. Besides the
\ac{RF}\_\ac{RBF} technique, \ac{H3LC}\_\ac{RF}\_{EDM} technique shows
the highest Sharpe ratio amongst the rest. These results enforce that
the proposed \ac{H3LC}\_\ac{EDM} technique is more accurate than the
current state of the art 2-label classification techniques with
\ac{RBF}.



%%% Local Variables:
%%% mode: latex
%%% TeX-master: "main"
%%% End:
