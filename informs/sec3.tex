\section{Data description and feature selection}
\label{sec:data-description}


Our work is based on datasets obtained from two well established
\ac{P2PL} platforms, Lending Club and Prosper. We chose these platforms,
because they have been in operation for more than 10 years, which
results in a number of matured loans, which are necessary for instance
based learning. Furthermore, research literature on \ac{P2PL} platforms
uses these datasets for evaluating their methodology. Choosing Prosper
and Lending Club datasets helps us perform a thorough comparison with
the current state of the art techniques.

\subsection{Feature selection}
\label{sec:feature-selection}

Historical loan data from \ac{P2PL} platforms has a large number of
features. For example, Prosper dataset includes 81
features~\citep{Udacity}. Many of these features are related to
financial transactions and have no bearing on the probability of loan
default. Hence, it is essential that we select only those features that
would help us in classifying the loans correctly. A number of studies
have been undertaken that perform two and three label classification for
\ac{P2PL}
loans~\citep{malekipirbazari2015risk,li2016prepayment,Lyer200908}. These
studies also indicate the primary features that help classify the
probability of loan default. The important predictors are listed below:

\begin{itemize}

\item \textbf{Loan term}: The length of the loan expressed in months.
\item \textbf{Employment status and duration}: The length in months of
borrower's employment status at the time the loan is created.
\item \textbf{Is borrower a home owner}: Binary (categorical) variable
indicating whether borrower is a home owner (indicated by 1) or not
(indicated by 0).
\item \textbf{\acs{FICO} credit score}: The borrower's credit score
provided by \ac{FICO}.
\item \textbf{Credit inquiries in last 6 months}: Number of inquiries in
the past six months of borrower.
\item \textbf{Delinquencies}: Number of borrower delinquencies (overdue
on payment) in the past two to seven years.
\item \textbf{Debt to income ratio}: The debt to income ratio of the
borrower.
\item \textbf{Loan original amount}: The total value of loan the
borrower wants to borrow.
\end{itemize}

The \textit{response variable} is \textbf{Loan status}. This is a
categorical variable. This variable takes the value 0, if the loan
defaulted. A value of 1 if it was fully paid on the date of maturity,
and 2 if it was prepaid completely before the date of maturity. The
effects of the aforementioned predictors on the loan status is provided
in the next sections for the Prosper and Lending Club datasets.

\subsection{Prosper dataset}
\label{sec:prosper-data}

\begin{figure}[tbh] \centering
  \includegraphics[scale=0.4]{figure_1}
  \caption{\textbf{The distributions of loans against the eight
predictor variables for the Prosper dataset}~\label{fig:pls}}
\end{figure}

The Prosper dataset used in our study is provided by~\cite{Udacity}.
This data set contains 113,938 loans, from 2005 to 2014 years with a
total of 81 financial and borrowers' personal features for each loan.
Since this data set contains both completed and current loans, we remove
all the current loans and loans with missing features. After performing
these cleaning operations we end up with 43,373 completed loans. The
frequency distribution of loan status against the eight predictors,
mentioned in Section~\ref{sec:feature-selection}, is plotted in
Figure~\ref{fig:pls}. Some important observations, which affect the
classification models are listed below:

\begin{enumerate}
\item Most borrowers on the Prosper platform prepay. This in turn means
  we would expect more \textit{misclassifications} when predicting full
  payment and defaults. Especially concerning is the correct
  classification of defaulting loans, because the absolute number of
  borrowers defaulting is low.
\item Home owner status seems to be a redundant feature, for loan
  classification, because it is equally likely for a borrower to default
  or not, irrespective of their home owner status. Yet, we keep this
  feature, for comparison purposes.
\item The likelihood of loan default varies as expected with other
borrower features. For example, we expect that borrowers' with low debt
and higher income would most likely pay their loans, same for credit
inquiries, delinquency, etc.
\end{enumerate}


\subsection{Lending Club dataset}
\label{sec:lending-club-data}

\begin{figure}[tbh] \centering
  \includegraphics[scale=0.4]{figure_2}
  \caption{\textbf{The distributions of loans against the eight
predictor variables for Lending Club dataset}~\label{fig:lcls}}
\end{figure}


This dataset was retrieved from the Lending Club website~\citep{LCW},
which contains complete loan data from the period 2007 to 2011. There
are 42,538 loans with a total 114 features for each loan. Similar to the
Prosper dataset, we had to clean the data, i.e., remove open loans and
remove loans with missing features. Moreover, because the feature set
provided by both platforms is not the same, we also had to manually
compute certain features. In particular, the \textbf{FICO credit score}
had to be calculated using the alphabet credit grade and sub-grade
features provided by Lending Club. After cleaning the dataset, we end up
with 33,951 completed loans with all the required features. The
frequency distribution of loans for the eight predictors for Lending
Club dataset is shown in Figure~\ref{fig:lcls}.

There are a number of differences in the Lending Club dataset compared
to Prosper:

\begin{enumerate}
\item Borrowers' default more often compared to the Prosper platform.
\item If the borrower does pay the loan back, it is only slightly more
  likely that the borrower would prepay compared to fully pay. Hence, we
  expect that predicting if a given loan would be prepaid or fully paid
  would be harder for the Lending Club dataset.
\item The home owner status in the Lending Club dataset, like Prosper,
  does not seem to matter.
\item Other factors affect likelihood of default similar to prosper, but
  in general we observe that accurately predicting loan status is harder
  in case of Lending Club dataset, because no predictor variable as
  strongly correlates as in the case of the Prosper dataset with a
  certain class of the response.
\end{enumerate}

%%% Local Variables: %%% mode: latex %%% TeX-master: "main" %%% End:
