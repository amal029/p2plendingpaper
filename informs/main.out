\BOOKMARK [1][-]{section.0.1}{Introduction}{}% 1
\BOOKMARK [1][-]{section.0.2}{Related work}{}% 2
\BOOKMARK [1][-]{section.0.3}{Data description and feature selection}{}% 3
\BOOKMARK [2][-]{subsection.0.3.1}{Feature selection}{section.0.3}% 4
\BOOKMARK [2][-]{subsection.0.3.2}{Prosper dataset}{section.0.3}% 5
\BOOKMARK [2][-]{subsection.0.3.3}{Lending Club dataset}{section.0.3}% 6
\BOOKMARK [1][-]{section.0.4}{Classification methods}{}% 7
\BOOKMARK [2][-]{subsection.0.4.1}{Logistic Regression \(LOGIT\)}{section.0.4}% 8
\BOOKMARK [2][-]{subsection.0.4.2}{Multi-nominal Logistic Regression \(MLOGIT\)}{section.0.4}% 9
\BOOKMARK [2][-]{subsection.0.4.3}{Random Forest \(RF\)}{section.0.4}% 10
\BOOKMARK [2][-]{subsection.0.4.4}{Hierarchical Three Label Classification \(H3LC\)}{section.0.4}% 11
\BOOKMARK [1][-]{section.0.5}{Instance-based model for predicting the Annualized Rate of Return \(ARR\) and standard deviation \(risk\) of investment}{}% 12
\BOOKMARK [2][-]{subsection.0.5.1}{Predicting the Annualized Rate of Return \(ARR\) and the standard deviation for loans available for investment}{section.0.5}% 13
\BOOKMARK [2][-]{subsection.0.5.2}{Computing the similarity metric}{section.0.5}% 14
\BOOKMARK [3][-]{subsubsection.0.5.2.1}{Gaussian Radial Basis Kernel Function \(RBF\) for computing the similarity metric}{subsection.0.5.2}% 15
\BOOKMARK [3][-]{subsubsection.0.5.2.2}{Exponential Distance Method \(EDM\) for computing the similarity metric}{subsection.0.5.2}% 16
\BOOKMARK [1][-]{section.0.6}{Portfolio optimization model}{}% 17
\BOOKMARK [2][-]{subsection.0.6.1}{Mean-variance optimization model}{section.0.6}% 18
\BOOKMARK [2][-]{subsection.0.6.2}{Sharpe ratio}{section.0.6}% 19
\BOOKMARK [1][-]{section.0.7}{Experimental evaluation and discussion}{}% 20
\BOOKMARK [2][-]{subsection.0.7.1}{Experimental setup}{section.0.7}% 21
\BOOKMARK [2][-]{subsection.0.7.2}{Comparison of accuracy of classification}{section.0.7}% 22
\BOOKMARK [3][-]{figure.0.4}{Results of 5-fold CV on the training dataset}{subsection.0.7.2}% 23
\BOOKMARK [3][-]{subsubsection.0.7.2.2}{Results for the five test dataset}{subsection.0.7.2}% 24
\BOOKMARK [2][-]{subsection.0.7.3}{Comparison of accuracy of predicting ARR and standard deviation}{section.0.7}% 25
\BOOKMARK [3][-]{figure.0.8}{Results of 5-fold CV on training dataset}{subsection.0.7.3}% 26
\BOOKMARK [3][-]{figure.0.11}{Results for the five test dataset}{subsection.0.7.3}% 27
\BOOKMARK [2][-]{subsection.0.7.4}{Mean-variance frontier generation and portfolio selection}{section.0.7}% 28
\BOOKMARK [1][-]{section.0.8}{Conclusion}{}% 29
