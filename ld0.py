# -*- coding: utf-8 -*-
"""
Created on Thu Oct 20 23:21:35 2016

@author: keith
"""

from __future__ import division, absolute_import
from __future__ import print_function
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt


def hist_g(p, f, d, bins, title):
    plt.hist((p, f, d), bins, color=('0.3', '0.6', '0.9'),
             label=('prepayment', 'fully payment', 'default'), align='left')
    # plt.legend(loc='upper right')
    plt.title(title)


def main():
    data = pd.read_csv('G:/p2p/LoanStats3a1.csv', encoding="ISO-8859-1")
    data = data[['id', 'loan_status', 'dti', 'delinq_2yrs',
                 'home_ownership', 'inq_last_6mths', 'length',
                 'loan_amnt', 'total_pymnt', 'grade',
                 'Years', 'Term']]

    data = data[(data['loan_status'] == 'Fully Paid') |
                (data['loan_status'] == 'Charged Off')]

    data = data[pd.notnull(data['dti'])]
    data = data[pd.notnull(data['inq_last_6mths'])]
    data = data[pd.notnull(data['delinq_2yrs'])]
    # Calculate the annualized return rate
    data['Return_rate'] = data['total_pymnt'] / data['loan_amnt']

    data['Return_rate'] = data['Return_rate'] ** (1/data['Years']) - 1

    # Change the home_ownership to 0,1
    data['home_ownership'][data['home_ownership'] != 'OWN'] = 0
    data['home_ownership'][data['home_ownership'] == 'OWN'] = 1

    data.grade = data.grade.astype(int)
    data.delinq_2yrs = data.delinq_2yrs.astype(int)
    data['loan_status'][data['loan_status'] == 'Charged Off'] = 0
    data['loan_status'][(data['Term'] > (data['Years'] * 12)) &
                        (data['loan_status'] == 'Fully Paid')] = 2
    data['loan_status'][data['loan_status'] == 'Fully Paid'] = 1
    data_p = data[data['loan_status'] == 2]
    data_f = data[data['loan_status'] == 1]
    data_d = data[data['loan_status'] == 0]
    plt.subplot(4, 2, 1)
    hist_g(data_p.Term, data_f.Term, data_d.Term, np.linspace(30, 60, 5),
           '\n Term')
    plt.legend(loc='upper right')
    plt.subplot(4, 2, 2)
    hist_g(data_p.length, data_f.length,
           data_d.length, np.linspace(0, 20, 10),
           '\n Employment length in month')

    plt.subplot(4, 2, 3)
    hist_g(data_p.home_ownership, data_f.home_ownership,
           data_d.home_ownership, [0, 1, 2],
           '\n Home status')

    plt.subplot(4, 2, 4)
    hist_g(data_p.grade, data_f.grade,
           data_d.grade, np.linspace(0, 900, 10),
           '\n Credit score')

    plt.subplot(4, 2, 5)
    hist_g(data_p.inq_last_6mths, data_f.inq_last_6mths,
           data_d.inq_last_6mths, np.linspace(0, 6, 10),
           '\n Inquiries last 6 months')

    plt.subplot(4, 2, 6)
    hist_g(data_p.delinq_2yrs, data_f.delinq_2yrs,
           data_d.delinq_2yrs, np.linspace(0, 4, 10),
           '\n Delinquencies')

    plt.subplot(4, 2, 7)
    hist_g(data_p.dti, data_f.dti,
           data_d.dti, np.linspace(0, 1.5, 10),
           '\n Debt to income ratio')

    plt.subplot(4, 2, 8)
    hist_g(data_p.loan_amnt, data_f.loan_amnt,
           data_d.loan_amnt, np.linspace(1000, 30000, 10),
           '\n Loan amount')

if __name__ == '__main__':
    main()
