\documentclass[acmlarge, screen, 12pt]{acmart}
\citestyle{acmauthoryear}
%\usepackage[round]{natbib}
%\usepackage{graphicx}
\usepackage{hyperref,amsmath}
\usepackage{acronym}
\usepackage{pgf}
\usepackage{pgfplots,pgfplotstable} %for plotting results
\usepackage{subfigure}
\usepackage{epstopdf}
\usepackage{booktabs,tabularx}
\usepackage{diagbox}
\usepackage[T1]{fontenc}

\definecolor{amethyst}{rgb}{0.6, 0.4, 0.8}


%% 
%% These commands are for a JOURNAL article.
% \acmJournal{JACM}
% \acmVolume{37}
% \acmNumber{4}
% \acmArticle{111}
% \acmMonth{8}

\begin{document}
	
	\input{00_acronyms.tex} 		%acronyms
	
	
\section{Efficacy of loan default likelihood predictive models for Lending Club dataset}\label{sec:lc}
In this section we compare the models that predict the likelihood of a
given loan defaulting. We first describe the experimental setup that
was used to compare the predictive models, followed by the comparison
itself.

\subsection{Experimental setup}
All our experiments in this section are performed on the Lending Club\footnote{www.lendingclub.com} dataset obtained from~\cite{LCW}. This dataset contains complete loan data from the period 2007 to 2011. There are 42,538 loans with a total 114 features for each loan. Similar to the Prosper dataset, we first cleaned/analysed as described in Section 3.1. After clean the dataset, we end up with 38,331 loans with 18 features (include response variable). The 17 selected features and the response variable are described in Table~\ref{table:fe}. 

Amongst these 38,331 loans, there are 5,517 defaulted loans. To avoid negative
impact of such an unbalanced dataset on the prediction models, we apply the
under-sampling technique~\cite{drummond2003c4}. Without loss of generality, we construct  5 sample datasets by randomly choosing 5,517 non-defaulting loans along with all 5,517 defaulting loans together as the sample dataset 5 times and show the average performance in this section. For training and testing the prediction algorithms,
we split each sample dataset into a ratio of 80:20, indicating 80\% of
the loans will be used to train the predictive models and the remaining
20\% used for testing. 

\begin{table}[th]
	\centering
	\caption{Features and response variable description}
	\label{table:fe}
	\begin{scriptsize}
		\resizebox{\textwidth}{!}{%
			\begin{tabular}{lll}\toprule
				Feature   & Explanation    & Type                                                                                                                                                                              \\ \midrule
				loan\_status                       & The current status of the loan.                              & Categorical                                                                        \\ 
				dti            & Debt to income ratio.              & Numerical \\ 
				delinq\_2yrs                     & The number of delinquency for the past two years.                   & Numerical                                                                                                           \\ 
				int\_rate            & The borrower's interest rate for this loan.                 & Numerical                                                                                                                  \\ 
				installment                     & The monthly payment owed by the borrower.  & Numerical                                                                                                         \\
				home\_ownership         & Specifies if the borrower is a homeowner or not.   & Categorical                                                                                  \\ 
		    	inq\_last\_6mths                & The number of inquiries in past 6 months.  & Numerical                                                                                                                               \\ 
				emp\_length                     & Employment length in years.            & Numerical                                                                                                                                       \\ 
		    	revol\_bal & Total credit revolving balance.                     & Numerical                                                                                                              \\ 
				annual\_inc                        & The borrower's annual income.       & Numerical                \\ 
				pub\_rec\_bankruptcies               & Number of public record bankruptcies.                              & Numerical       \\ 
				loan\_amnt                & The listed amount of the loan applied for by the borrower.    & Numerical         \\
				grade             & LC assigned loan grade.              & Numerical            \\ 
				open\_acc                       & The number of open credit lines.   & Numerical       \\ 
				addr\_state             & The state of the borrower.    & Categorical                                                                                     \\ 
				Term                               & The length of the loan expressed in months.                            & Numerical                                                                                                                      \\ 
				revol\_util                   & Revolving line utilization rate.               & Numerical                                                                                                       \\ 
				total\_acc                       & The total number of credit lines.                                & Numerical                                                                                     \\ \bottomrule 
			\end{tabular}%
		}
	\end{scriptsize}
\end{table}

\subsection{Comparison of models predicting the likelihood of loan default}
n this section, we compare \acf{SVM}, \acf{kNN}, \acf{LOGIT} and 
\acf{RF} models to predict
the likelihood of a given loan defaulting, and thereby find the best
predictive model. For comparison purposes, we select accuracy, True
Positive Rate (TPR) and True Negative Rate (TNR) as the criterion to
evaluate the goodness of fit of the model. The definition of these three
measures are described in Section 3.2. In particular, the
higher the accuracy, the better the model fits the dataset.

\begin{table}[th]
	\centering
	\caption{Results of SVM, kNN, LOGIT and RF for five sample datasets with the measure: accuracy.}
	\label{tab:acc5}
	\begin{tabular}{lllllll}\toprule
		Sample dataset  & 1    & 2    & 3    & 4    & 5    & Average \\ \midrule
		SVM & 0.49 & 0.49& 0.48 & 0.51 & 0.50 & 0.49   \\
		kNN      & 0.53 & 0.54 & 0.55 & 0.53 & 0.52 & 0.54    \\
		LOGIT & 0.60 & 0.62 & 0.63 & 0.61 & 0.62 & 0.62  \\
		RF      & 0.63 & 0.66 & 0.65& 0.62 & 0.64& 0.64    \\ \bottomrule
	\end{tabular}
\end{table}

\begin{table}[th]
	\centering
	\caption{Results of SVM, kNN, LOGIT and RF for five sample datasets with the measure: True Positive Rate (TPR).}
	\label{tab:tpr5}
	\begin{tabular}{lllllll}\toprule
		Sample dataset  & 1    & 2    & 3    & 4    & 5    & Average \\ \midrule
		SVM & 1.00 & 1.00& 1.00 & 0 & 0 & 0.60   \\
		kNN      & 0.52 & 0.56 & 0.53 & 0.54 & 0.53 & 0.54    \\
		LOGIT & 0.74 & 0.76 & 0.76 & 0.73 & 0.73 & 0.74  \\
		RF      & 0.56& 0.56& 0.57& 0.54& 0.54 & 0.55    \\ \bottomrule
	\end{tabular}
\end{table}

\begin{table}[th]
	\centering
	\caption{Results of SVM, kNN, LOGIT and RF for five sample datasets with the measure: True Negative Rate (TNR).}
	\label{tab:tnr5}
	\begin{tabular}{lllllll}\toprule
		Sample dataset  & 1    & 2    & 3    & 4    & 5    & Average \\ \midrule
		SVM & 0.00 & 0.00& 0.00 & 1.00 & 1.00 & 0.40   \\
		kNN      & 0.54 & 0.49 & 0.54 & 0.55 & 0.51 & 0.53    \\
		LOGIT & 0.47 & 0.49 & 0.50& 0.48 & 0.51 & 0.49  \\
		RF      & 0.69 & 0.71 & 0.71 & 0.69 & 0.72 & 0.70    \\ \bottomrule
	\end{tabular}
\end{table}

Tables~\ref{tab:acc5},~\ref{tab:tpr5} and~\ref{tab:tnr5} show the
results of accuracy, TPR and TNR for five sample datasets based on \ac{SVM}, 
\ac{kNN}, \ac{LOGIT} and \ac{RF} models, respectively. From 
Table~\ref{tab:acc5}, it can be observed that \ac{RF} achieves the highest 
average accuracy of 0.64 (64\%). \ac{LOGIT} also fits the sample datasets well 
with average accuracy of prediction of 0.62 (62\%). In 
addition, the average accuracy of \ac{SVM} and \ac{kNN} are 0.49 (49\%) and 0.54 (54\%), respectively. From Table~\ref{tab:tpr5}, it 
can be observed that \ac{LOGIT} achieves the highest TPR of 0.74 (74\%) amongst these five machine learning models. From Table~\ref{tab:tnr5}, it 
can be observed that \ac{RF} achieves the highest TNR of 0.70 (70\%) amongst these five machine learning models. Hence, \ac{RF} and \ac{LOGIT} are the two best models to predict the likelihood of a given loan defaulting. However, compared to \ac{LOGIT}, \ac{RF} may take longer to execute with a large dataset,
and one may select \ac{LOGIT} as the predictive model with a trade-off
of 0.02 (2\%) reduced accuracy of prediction.

\section{Result of portfolio optimisation comparison for lending club dataset}
In this section we compare the quality of the generated portfolio, by
comparing the average portfolio return rates and risk obtained after
solving the proposed technique,\textit{MV} model~(10) and \textit{IB} model~(11). We
test these models with 20 different scenarios and 3
lenders. For each scenario, we randomly choose 10 loans from the Lending Club dataset
as the loans available for investment and randomly generate the
constraints for each lender.

\subsection{Model-I comparison}
\begin{figure}[h]
	\centering
	\includegraphics[scale=0.50]{lcreturn}
	\caption{The average portfolio return rates achieved by solving the proposed \textit{WMPI} model~(4) and \textit{MV} model~(10) with three lenders}
	\label{fig:3ls}
\end{figure}

\begin{figure}[t]
	\centering
	\subfigure[The average portfolio risk comparison]{
		\label{fig:3risk}
		\includegraphics[width=0.45\linewidth,  height=2in]{lcrisk}}
	\hspace{0.1in}
	\subfigure[The average portfolio variance comparison]{
		\label{fig:3riskv}
		\includegraphics[width=0.45\linewidth, height=2in]{varil}}  
	\caption{The average portfolio risk and variance obtained by solving the 
		proposed \textit{WMPI} model~(4) and the \textit{MV}
		model~(10) with three lenders}
\end{figure}


Figure~\ref{fig:3ls} shows the average portfolio return rates with three
lenders obtained by solving \textit{WMPI} model~(4) and \textit{MV} 
model~(10). We can
observe from Figure~\ref{fig:3ls} that \textit{WMPI} model has better
results in 17 out of 20 scenarios. \textit{WMPI} model
achieves an average return rate of 0.17 (17\%) based on 20 scenarios.
Compared to the average return rate of 0.15 (15\%) achieved by \textit{MV} model~(10),
a difference of 0.02 (2\%). On
average, the proposed model's portfolio return is 2\% higher than \textit{MV}
model~(10). 

Figure~\ref{fig:3risk} illustrates the average portfolio risk with three
lenders obtained by solving the proposed technique and \textit{MV} model~(10).
We can observe from
Figure~\ref{fig:3risk} that the proposed technique has less or equal
risk in all scenarios. On average, the proposed model has an average
portfolio risk of 0.00022 (0.022\%) based on 20 scenarios. Compare to the
average portfolio risk of 0.032 (3.2\%) obtained by \textit{MV} model~(10), 
a difference of 0.0318 (3.18\%).
On average, \textit{WMPI} model's portfolio risk is about 1/145 of that
achieved by solving \textit{MV} model~(10). Hence, we
can conclude that the preferred portfolio achieved by applying our
proposed technique will have lower risk than that achieved by applying
\textit{MV} model~(10). This should be as
expected, because \textit{MV} model~(10) uses
covariance as risk, which is hard if not impossible to compute for
unrelated borrowers. Figure~\ref{fig:3riskv} illustrates the average portfolio variance with three
lenders obtained by solving the proposed technique and the \textit{MV}
model~(10). We can observe from
Figure~\ref{fig:3riskv} that the proposed technique has less or equal
variance in 18 out of 20 scenarios. On average, the proposed model has an 
average portfolio variance of 0.0016 based on 20 scenarios. Compare this to
the average portfolio variance of 0.021 obtained by the \textit{MV}
model~(10), a difference of 0.0005.

\begin{table}[th]
	\centering
	\caption{T-test between the results (the average portfolio return rate, portfolio risk and portfolio variance) of 20 scenarios by applying the \textit{WMPI}
		model and the \textit{MV} model with null hypothesis that they have the same mean value}
	\label{tab:ttestmv}
	\begin{tabular}{llll}\toprule
		
		& Average portfolio return rate   & Average portfolio risk &Average portfolio variance     \\ \midrule
		P-value & 6.6e-03 & 4.4e-04& 4.4e-04  \\
		Decision      & Reject & Reject & Reject \\ \bottomrule
	\end{tabular}
\end{table}

To validate the differences between the results (the average portfolio return rate, portfolio risk and portfolio variance) of 20 scenarios by applying the \textit{WMPI}
model~(4) and the \textit{MV} model~(10) are not negligible, T-test is applied with null hypothesis, that they have the same mean value. The results are shown in Table~\ref{tab:ttestmv}. We can observe from 
Table~\ref{tab:ttestmv} that all the P-values are smaller than 0.05 and we reject the null hypothesis. Hence, we can deduce that there is significant statistical difference between the average portfolio return rate, portfolio risk and portfolio variance of the investment portfolios gained by applying the \textit{WMPI}
model~(4) and the \textit{MV} model~(10).

Overall, the proposed technique, \textit{WMPI} model~(4),
\textit{approaches} a better quality solution than \textit{MV} model~(10). 

\subsection{Model-II comparison}
\begin{figure}[h]
	\centering
	\includegraphics[scale=0.60]{lcreturn1}
	\caption{The average portfolio return rates achieved by solving the proposed
		\textit{WMPI} model~(4) and \textit{IB} model~(11) 
		with three lenders}
	\label{fig:3rsm}
\end{figure}

\begin{figure}[t]
	\centering
	\subfigure[The average portfolio risk comparison]{
		\label{fig:3risk2}
		\includegraphics[width=0.45\linewidth,  height=2in]{lcrisk1}}
	\hspace{0.1in}
	\subfigure[The average portfolio variance comparison]{
		\label{fig:3riskv2}
		\includegraphics[width=0.45\linewidth, height=2in]{varil2}}  
	\caption{The average portfolio risk and variance obtained by solving the \textit{WMPI} model~(4) and the \textit{IB} model~(11) with three enders}
\end{figure}

Figure~\ref{fig:3rsm} shows the average portfolio return rates for the three
lenders achieved by solving the proposed technique and \textit{IB} model~(11)
with 1,000 training samples. We can
observe from Figure~\ref{fig:3rsm} that \textit{WMPI} model has better
results in all 20 scenarios. Specifically, \textit{WMPI} model achieves
an average portfolio return rate of 0.17 (17\%) for these 20 scenarios.
Compared to the average portfolio return rate of 0.13 (13\%) achieved by
solving \textit{IB} model~(11) --- the difference is
0.04 (4\%).

Figure~\ref{fig:3risk2} compares the average risk of the portfolios
obtained from \textit{WMPI} model~(4) and
\textit{IB} model~(11). Similar to the results shown in
Figure~\ref{fig:3rsm}, the proposed technique has better result in all
20 scenarios. Specifically, the proposed model obtained an average
portfolio risk of 0.0016 (0.16\%) for these 20 scenarios. Compared to
the average portfolio risk of 0.013 (1.3\%) obtained by solving \textit{IB}
model~(11) --- the difference is 0.011
(1.1\%). On average, On average, \textit{WMPI} model's portfolio risk is about 1/8 of that achieved by solving  \textit{IB} model~(11).
Again, this is as expected, because the
covariance matrix used as the risk indicator in \textit{IB}
model~(11), is misleading, since borrowers usually
have no relation to each other. The proposed model, fixes this problem,
by directly using the total loan default rate as the risk indicator.
Figure~\ref{fig:3riskv2} compares the
average variance of the portfolios obtained from the proposed \textit{WMPI}
model~(4) and the \textit{IB} model~(11).
The proposed technique has equal or better result in 19 out of 20 scenarios. 
Specifically, the proposed model obtained an average portfolio variance of 
0.0015 for these 20 scenarios. Compare this to the average 
portfolio variance of 0.0017 obtained by solving the \textit{IB}
model~(11). 

\begin{table}[th]
\centering
\caption{T-test between the results (the average portfolio return rate, portfolio risk and portfolio variance) of 20 scenarios by applying the \textit{WMPI}
	model and the \textit{IB} model with null hypothesis that they have the same mean value}
\label{tab:ttestib}
\begin{tabular}{llll}\toprule
	
	& Average portfolio return rate   & Average portfolio risk &Average portfolio variance     \\ \midrule
	P-value       & 5.0e-09 &0.062 & 0.44  \\
	Decision      & Reject & Not reject & Not reject \\ \bottomrule
\end{tabular}
\end{table}
To validate the differences between the results (the average portfolio return rate, portfolio risk and portfolio variance) of 20 scenarios by applying the \textit{WMPI}
model~(4) and the \textit{IB} model~(11) are not negligible, T-test is applied with null hypothesis, that they have the same mean value. The results are shown in Table~\ref{tab:ttestib}. We can observe from 
Table~\ref{tab:ttestib} that the P-value of the average portfolio return rate are smaller than 0.05 and we reject the null hypothesis. Hence, we can deduce that there is significant statistical difference between the average portfolio return rate of the investment portfolios gained by applying the \textit{WMPI}
model~(4) and the \textit{IB} model~(11). In addition, because the P-values of the average portfolio risk and the average portfolio variance are larger than 0.05, we do not reject the null hypothesis. In other words, there is no significant statistical difference between the average portfolio risk and the average portfolio variance of the investment portfolios gained by applying the \textit{WMPI} model~(4) and the \textit{IB} model~(11).

Overall, the proposed technique, \textit{WMPI} model~(4),
\textit{approaches} a better quality solution than \textit{IB} 
model~(11). 
\bibliographystyle{ACM-Reference-Format} % basic style, author-year citations
\bibliography{mybibfile} % name your BibTeX data base
\end{document}
\endinput